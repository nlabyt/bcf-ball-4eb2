<?php
/**
 * HsConfig Component Helper
 *
 * @license		GNU/GPL
 */

/**
 * HsConfig Component Helper
 */
class HsConfigHelper
{
	/**
	 * Determine if the id given represents the site configuration
	 *
	 * @param mixed $id
	 * @return true if id is for the site configuration, otherwise false
	 */
	public static function isSiteConfig( $id )
	{
		return ($id == -1);
	}

	public static function isPublished( $id )
	{
		jimport('joomla.filesystem.file');

		if (!JFile::exists( HsConfigHelper::createCssName($id)))
		{
			return false;
		}
		if (!JFile::exists( HsConfigHelper::createJsName($id)))
		{
			return false;
		}
		return true;
	}

	/**
	 * create a CSS stylesheet name based upon the id given
	 *
	 * @param mixed $id
	 * @return full path file name
	 */
	public static function createCssName( $id )
	{
		$fname = JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'highslide'.DS.'config'.DS.'css'.DS;

		if (HsConfigHelper::isSiteConfig($id) )
		{
			$fname .=  'highslide-sitestyles.css';
		}
		else
		{
			$fname .= 'highslide-article-' . $id . '-styles.css';
		}
		return $fname;
	}

	public static function createJsName( $id )
	{
		$fname = JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'highslide'.DS.'config'.DS.'js'.DS;

		if (HsConfigHelper::isSiteConfig($id) )
		{
			$fname .=  'highslide-sitesettings.js';
		}
		else
		{
			$fname .= 'highslide-article-' . $id . '-settings.js';
		}
		return $fname;
	}
}