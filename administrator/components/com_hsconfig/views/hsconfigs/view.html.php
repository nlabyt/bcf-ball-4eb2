<?php
/**
 * HsConfigs View for Highslide Configuration Component
 *
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

/**
 * HsConfigs View
 */
class HsConfigsViewHsConfigs extends JView
{
	protected $rows;
	protected $pagination;

	/**
	 * HsConfigs view display method
	 * @return void
	 **/
	function display($tpl = null)
	{
		// Get data from the model
		$this->rows = $this->get('List');
		$this->page = $this->get('Pagination');

		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helper/hsconfig.php';
		$canDo = HsConfigViewHelper::getActions();

		JToolBarHelper::title(   JText::_( 'COM_HSCONFIG_CONFIG_TITLE' ), 'config.png' );

		if ($canDo->get('core.create'))
		{
			JToolBarHelper::addNewX();
		}
		if ($canDo->get('core.edit'))
		{
			JToolBarHelper::editListX();
		}
		if ($canDo->get('core.create'))
		{
			JToolBarHelper::customX( 'copy', 'copy.png', 'copy_f2.png', 'Copy' );
		}
		if ($canDo->get('core.edit.state'))
		{
			JToolBarHelper::divider();
			JToolBarHelper::publishList();
			JToolBarHelper::unpublishList();
			JToolBarHelper::divider();
		}
		if ($canDo->get('core.delete'))
		{
			JToolBarHelper::deleteList('COM_HSCONFIG_ALERT_DELETEREALLY');
		}
	}
}