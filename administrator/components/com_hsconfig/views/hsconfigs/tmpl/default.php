<?php defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('script','system/multiselect.js',false,true);

$user	= JFactory::getUser();
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="clr"></div>
	<table class="adminlist">
	<thead>
		<tr>
			<th width="1%">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->rows ); ?>);" />
			</th>
			<th width="20%">
				<?php echo JText::_( 'COM_HSCONFIG_COLUMN_CONFIGURATION' ); ?>
			</th>
			<th width="2%" align="center">
				<?php echo JText::_( 'JPUBLISHED' ); ?>
			</th>
			<th width="2%">
				<?php echo JText::_( 'JGRID_HEADING_ID' ); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="4">
			<?php echo $this->page->getListFooter(); ?>
		</td>
	</tr>
	</tfoot>
	<tbody>
	<?php
	$n = count($this->rows);
	foreach ($this->rows as $i => $row) :
		$canCreate	= $user->authorise('core.create',		'com_content.category.'.$row->catid);
		$canEdit	= $user->authorise('core.edit',			'com_content.article.'.$row->id);
		$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $row->checked_out==$user->get('id') || $row->checked_out==0;
		$canChange	= $user->authorise('core.edit.state',	'com_contact.article.'.$row->catid) && $canCheckin;

		if ($row->id == -1)
		{
			$row->title = JText::_('COM_HSCONFIG_SITE_CONFIG');
		}
		?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center">
				<?php echo JHtml::_('grid.id', $i, $row->id); ?>
			</td>
			<td>
				<?php if ($canCreate || $canEdit) : ?>
				<a href="<?php echo JRoute::_( 'index.php?option=com_hsconfig&controller=hsconfig&task=edit&cid[]='. $row->id );?>">
					<?php echo $this->escape($row->title); ?></a>
				<?php else : ?>
					<?php echo $this->escape($row->title); ?>
				<?php endif; ?>
			</td>
			<td class="center">
				<?php echo JHtml::_('jgrid.published', $row->published, $i, '', $canChange); ?>
			</td>
			<td class="center">
			<?php echo $row->id; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	</table>

	<input type="hidden" name="option" value="com_hsconfig" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="controller" value="hsconfig" />
	<?php echo JHtml::_('form.token'); ?>
</form>