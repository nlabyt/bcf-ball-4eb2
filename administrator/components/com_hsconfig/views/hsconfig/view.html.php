<?php
/**
 * Highslide Configuration View for Highslide Configuration Component
 *
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport( 'joomla.application.component.view' );

/**
 * HsConfig View
 */
class HsConfigsViewHsConfig extends JView
{
	/**
	 * display method of HsConfig view
	 * @return void
	 **/
	public $form;

	public function display($tpl = null)
	{
		//get the hsconfig
		$hsconfig	= $this->get('Data');
		$form   	= $this->get('Form');
		$isNew		= ($hsconfig->id == 0);
		$params		= new JRegistry;
		$params->loadJSON( $hsconfig->params );
		$data = $params->toArray( $params );
		$data['css'] = $hsconfig->css;
		$data['overlayhtml'] = $hsconfig->overlayhtml;
		$data['script']= $hsconfig->script;
		$data['skincontrols']= $hsconfig->skincontrols;
		$data['skincontent']= $hsconfig->skincontent;
		$data['publishConfig'] = ($hsconfig->published ? "1" : "0");
		$isNew		= ($hsconfig->id == 0);

		// Check for model errors.
		if ($errors = $this->get('Errors')) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Bind the form to the data.
		if ($form && $data) {
			$form->bind($data);
		}

		$this->assignRef('form', $form);
		$this->assignRef('hsconfig', $hsconfig);
		$this->assignRef('params', $params);

		if ($isNew)
		{
			$articlelist = $this->get('ArticleList');
			$this->assignRef('articlelist', $articlelist);
		}
		$this->addToolbar( $isNew );
		parent::display($tpl);
	}

	protected function addToolbar( $isNew )
	{
		$text = $isNew ? JText::_( 'COM_HSCONFIG_ACTION_NEW' ) : JText::_( 'COM_HSCONFIG_ACTION_EDIT' );
		JToolBarHelper::title(   JText::_( 'COM_HSCONFIG_CONFIG_TITLE' ).': <small><small>[ ' . $text.' ]</small></small>', 'config.png' );

		if ( !$isNew)
		{
			// for existing items changes can be applied
			JToolBarHelper::apply('apply','JTOOLBAR_APPLY');
		}
		JToolBarHelper::save( 'save', 'JTOOLBAR_SAVE');
		JToolBarHelper::divider();
		JToolBarHelper::cancel();
//		JToolBarHelper::divider();
//		JToolBarHelper::help('JHELP_SITE_GLOBAL_CONFIGURATION');
	}

	protected function doFiller( $count )
	{
		if ($count > 0)
		{
			echo "<li>";
			for ($i = 0; $i < ($count+8); $i++ )
			{
				echo "<br/>";
			}
			echo "</li>\n";
		}

	}

	protected function renderOverlayExampleInfos()
	{
		$maxcount = 0;

		echo HsConfigsViewHsConfig::PresetInfo( 'com_hsconfig_op_none-selected', $maxcount );
		echo HsConfigsViewHsCOnfig::PresetInfo( 'com_hsconfig_op_no_info', $maxcount );

		$path		= JPATH_ADMINISTRATOR.DS."components".DS."com_hsconfig".DS."presets".DS."overlay";
		$filter		= ".html";
		$files		= JFolder::files($path, $filter);

		$options = "";

		if ( is_array($files) )
		{
			foreach ($files as $file)
			{
				$file = JFile::stripExt( $file );
				if ($file != 'index')
				{
					echo HSConfigsViewHsConfig::PresetInfo( "com_hsconfig_op_".$file, $maxcount );
				}
			}
		}
		HSConfigsViewHsConfig::doFiller( $maxcount );
	}

	protected function renderScreenshotInfos()
	{
		$maxcount = 0;

		echo HsConfigsViewHsConfig::PresetInfo( 'com_hsconfig_ss_none-selected', $maxcount );
		echo HsConfigsViewHsCOnfig::PresetInfo( 'com_hsconfig_ss_no_info', $maxcount );

		$path		= JPATH_ADMINISTRATOR.DS."components".DS."com_hsconfig".DS."presets".DS."slideshow";
		$filter		= ".js";
		$files		= JFolder::files($path, $filter);

		$options = "";

		if ( is_array($files) )
		{
			foreach ($files as $file)
			{
				$file = JFile::stripExt( $file );
				echo HSConfigsViewHsConfig::PresetInfo( "com_hsconfig_ss_".$file, $maxcount );
			}
		}
		HSConfigsViewHsConfig::doFiller( $maxcount );
	}

	protected function PresetInfo( $name, &$maxcount )
	{
		$text = JText::_($name);
		if ($text == $name)
		{
			return "";
		}
		$info  = "<li id=\"".$name."\" style=\"display: none;width: 40%;position: absolute\">\n";
		$info .= "<br/><h4>".$text."</h4>\n";
		$text = JText::_($name."_DESC");
		if ($text != $name."_DESC")
		{
			$info .= "<p>".$text."</p>\n";
		}
		$text = JText::_($name."_OVERRIDE0");
		$i = 0;
		$oinfo = "";
		while ($text != $name."_OVERRIDE".$i)
		{
			if ($oinfo == "")
			{
				$oinfo = "<br/><ul>\n";
			}
			$oinfo .= "<li>".$text."</li>\n";
			$i++;
			$text = JText::_($name."_OVERRIDE".$i);
		}
		if ($oinfo != "")
		{
			$oinfo .= "</ul>\n";
		}
		$text = JText::_($name."_NOTES0");
		$i = 0;
		$ninfo = "";
		while ($text != $name."_NOTES".$i)
		{
			if ($ninfo == "")
			{
				$ninfo = "<br />\n";
			}
			$ninfo .= $text."\n";
			$i++;
			$text = JText::_($name."_NOTES".$i);
		}
		if ($ninfo != "")
		{
			$ninfo .= "\n";
		}
		$info .= $oinfo.$ninfo."</li>\n";
		if (isset($maxcount))
		{
			$lines = preg_match_all( "/\n/", $info, $arr );
			$maxcount = max( $maxcount, $lines );
		}
		return $info;
	}

	protected function WarningIcon()
	{
		$tip = '<img src="'.JURI::root().'administrator/components/com_hsconfig/images/warning.png" border="0"  style="margin:0" alt="" />';

		return $tip;
	}
}