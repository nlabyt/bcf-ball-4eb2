<?php
/**
 * @version		$Id: default_cache.php 17109 2010-05-16 21:03:55Z severdia $
 * @package		Joomla.Administrator
 * @subpackage	com_hsconfig
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
?>
<div class="width-100">

<fieldset class="adminform">
	<legend><?php echo JText::_('COM_HSCONFIG_FIELDSET_SLIDESHOW_PRESET_EXAMPLE_LABEL'); ?></legend>
	<ul class="adminformlist">
		<li id="slideshowscreenshot">
			<img id="slideshowscreenshot_image" src="<?php echo JURI::root(true); ?>/administrator/components/com_hsconfig/presets/slideshow/none-selected.jpg" alt="Screenshot" style="visibility: visible;display:block" />
			<ul style="clear:left"><?php echo HsConfigsViewHsConfig::renderScreenshotInfos(); ?></ul>
		</li>
	</ul>
</fieldset>
</div>
