<?php
/**
 * @version		$Id: default_navigation.php 15735 2010-04-01 02:49:35Z infograf768 $
 * @package		Joomla.Administrator
 * @subpackage	com_hsconfig
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
?>
<div id="submenu-box">
	<div class="t">
		<div class="t">
			<div class="t"></div>
		</div>
	</div>
	<div class="m">
		<div class="submenu-box">
			<div class="submenu-pad">
				<ul id="submenu" class="com_hsconfig">
					<li><a href="#" onclick="return false;" id="general" class="active"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_GENERAL' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="popup" ><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_POPUP' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="overlays"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_OVERLAYS' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="slideshow"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_SLIDESHOW' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="csslink"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_CSS' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="script"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_SCRIPT' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="html"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_HTML' ); ?></a></li>
					<li><a href="#" onclick="return false;" id="language"><?php echo JText::_( 'COM_HSCONFIG_NAVIGATION_LANGUAGE' ); ?></a></li>
				</ul>
				<div class="clr"></div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="b">
		<div class="b">
			<div class="b"></div>
		</div>
	</div>
</div>