<?php
/**
 * @version		$Id: default_cache.php 17109 2010-05-16 21:03:55Z severdia $
 * @package		Joomla.Administrator
 * @subpackage	com_hsconfig
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
?>
<div class="width-100">

<fieldset class="adminform">
	<legend><?php echo JText::_('COM_HSCONFIG_FIELDSET_LANGUAGE_STRINGS_LABEL'); ?>
		<span class="hasTip" style="display: inline-block" title="<?php echo JText::_( 'COM_HSCONFIG_MSG_HEADER_WARNING' );?>::<?php echo JText::_( 'COM_HSCONFIG_MSG_WARNLANGUAGEOVERRIDE' ); ?>">
			<?php echo HsConfigsViewHsConfig::WarningIcon(); ?>
		</span>
	</legend>
	<ul class="adminformlist">
		<?php
		foreach ($this->form->getFieldset('languagestrings') as $field):
		?>
			<li><?php echo $field->label; ?>
			<?php echo $field->input; ?></li>
		<?php
		endforeach;
		?>
	</ul>
</fieldset>
</div>
