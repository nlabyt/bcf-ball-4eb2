<?php defined('_JEXEC') or die;

// Load tooltips behavior
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.switcher');
JHtml::_('behavior.tooltip');

// Load submenu template, using element id 'submenu' as needed by behavior.switcher
$this->document->setBuffer($this->loadTemplate('navigation'), 'modules', 'submenu');

JHtml::_('script', 'administrator/components/com_hsconfig/js/dotab.js' );
JHtml::_('script', 'administrator/components/com_hsconfig/js/ssscreenshot.js' );
JHtml::_('stylesheet', 'administrator/components/com_hsconfig/css/ssscreenshot.css' );


if ($this->hsconfig->id == 0)
{?>
<script language="javascript" type="text/javascript">
<!--
Joomla.submitbutton = function(pressbutton) {
	if (pressbutton == 'cancel') {
		submitform( pressbutton );
		return;
	}
	// do field validation
	if (getSelectedValue('adminForm','cid') < 1) {
		alert( "<?php echo JText::_( 'COM_HSCONFIG_ALERT_SELECT_AN_ARTICLE' ).'.'; ?>" , true);
	} else {
		submitform( pressbutton );
	}
}
<?php
}
else
{?>
<script language="javascript" type="text/javascript">
<!--
<?php
}
?>
var dotab = new doTab();
var ssscreenshot = new SsScreenshot();
var opexample = new SsScreenshot();
window.addEvent('domready', function() {
	dotab.init();
	ssscreenshot.init( { base: '<?php echo JURI::root(true); ?>' + '/administrator/components/com_hsconfig/presets/slideshow/',
	                     imgId: 'slideshowscreenshot_image',
	                     presetId: 'jform_ssPreset',
	                     prefix: 'ss'
	                   });
	opexample.init( { base: '<?php echo JURI::root(true); ?>' + '/administrator/components/com_hsconfig/presets/overlay/',
	                  imgId: 'overlayexample_image',
	                  presetId: 'jform_opPreset',
	                  prefix: 'op'
	                });
    });
-->
</script>

<form action="<?php echo JRoute::_('index.php?option=com_hsconfig');?>" id="adminForm" method="post" name="adminForm" class="form-validate">
    <div id="config-document">
		<div class="configuration">
		<?php if($this->hsconfig->id == 0)
		{
			$arraylist[]	= JHtml::_('select.option',  '0', JText::_( 'COM_HSCONFIG_OPTION_SELECT_ARTICLE' ), 'id', 'title' );
			$arraylist		= array_merge( $arraylist, $this->articlelist );
			echo JHtml::_('select.genericlist', $arraylist, 'cid', 'class="inputbox" size="1"','id', 'title' );
		}
		else
		{
			if ($this->hsconfig->id == -1)
			{
				echo JText::_('COM_HSCONFIG_SITE_CONFIG');
			}
			else
			{
				echo $this->hsconfig->title;
			}
		}
		?>
		</div>
        <div id="page-general" class="tab">
            <div class="noshow">
            	<div class="width-100 fltlft">
            	<?php echo $this->loadTemplate('general'); ?>
            	</div>
            </div>
        </div>
        <div id="page-popup" class="tab">
            <div class="noshow">
            	<div class="width-50 fltlft">
            	<?php echo $this->loadTemplate('popupcontent'); ?>
            	</div>
            	<div class="width-50 fltrt">
            	<?php echo $this->loadTemplate('popupbehavior'); ?>
            	</div>
            </div>
        </div>
        <div id="page-overlays" class="tab">
            <div class="noshow">
            	<div class="width-50 fltlft">
            	<?php echo $this->loadTemplate('captionoverlay'); ?>
            	<?php echo $this->loadTemplate('overlaypresets'); ?>
            	<?php echo $this->loadTemplate('customoverlay'); ?>
            	</div>
            	<div class="width-50 fltrt">
            	<?php echo $this->loadTemplate('headingoverlay'); ?>
            	<?php echo $this->loadTemplate('overlaypresetexample'); ?>
            	</div>
            	<div class="width-100 fltlft">
            	<?php echo $this->loadTemplate('overlayhtml'); ?>
            	</div>
            </div>
        </div>
        <div id="page-slideshow" class="tab">
            <div class="noshow">
            	<div class="width-50 fltlft">
            	<?php echo $this->loadTemplate('slideshowpresets'); ?>
            	<?php echo $this->loadTemplate('slideshowpresetexample'); ?>
            	</div>
            	<div class="width-50 fltrt">
            	<?php echo $this->loadTemplate('slideshow'); ?>
            	<?php echo $this->loadTemplate('thumbstrip'); ?>
            	</div>
            </div>
        </div>
        <div id="page-csslink" class="tab">
            <div class="noshow">
            	<div class="width-100">
           		<?php echo $this->loadTemplate('csstext'); ?>
             	</div>
            </div>
        </div>
        <div id="page-script" class="tab">
            <div class="noshow">
            	<div class="width-100">
           		<?php echo $this->loadTemplate('javascript'); ?>
            	</div>
            </div>
        </div>
        <div id="page-html" class="tab">
            <div class="noshow">
            	<div class="width-40 fltlft">
           		<?php echo $this->loadTemplate('htmlextensions'); ?>
            	</div>
            	<div class="width-60 fltrt">
           		<?php echo $this->loadTemplate('flash'); ?>
            	</div>
            	<div class="width-100 fltlft">
           		<?php echo $this->loadTemplate('htmlskin'); ?>
            	</div>
            </div>
        </div>
        <div id="page-language" class="tab">
            <div class="noshow">
            	<div class="width-100 fltlft">
           		<?php echo $this->loadTemplate('languagestrings'); ?>
            	</div>
            </div>
        </div>
    </div>
	<div class="clr"></div>

	<input type="hidden" name="option" value="com_hsconfig" />
	<input type="hidden" name="id" value="<?php echo $this->hsconfig->id; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="controller" value="hsconfig" />
	<?php echo JHtml::_('form.token'); ?>
</form>