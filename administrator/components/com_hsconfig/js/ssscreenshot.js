/**
* Highslide JS for Joomla
* @version $Id$
* @subpackage highslide.js
* @author Ken Lowther
* @license Limited  http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/
function SsScreenshot()
{

var thisObject = this;
this.sshotimg = null;
this.sspreset = null;
this.baseUrl = "";
this.imgId = "";
this.presetId = "";
this.prefix = "";

this.onchange = function(e)
{
	var sshotname = "";

	if (thisObject.sspreset.value == -1)
	{
		sshotname = "none-selected";
	}
	else
	{
		sshotname = thisObject.sspreset.value;
	}
	thisObject.sshotimg.src = thisObject.baseUrl + sshotname + ".jpg";
	thisObject.sshotimg.style.visibility = 'visible';
	var foundit = false;
	for (var i = 0; i < thisObject.sspreset.childNodes.length; i++)
	{
		var name = thisObject.sspreset.childNodes[i].value;
		if (name == -1)
		{
			name = "none-selected";
		}
		var ele = document.getElementById( 'com_hsconfig_' + thisObject.prefix + '_' + name );
		if (ele != null)
		{
			if (name == sshotname)
			{
				ele.style.display = "block";
				foundit = true;
			}
			else
			{
				ele.style.display = "none";
			}
		}
	}
	ele = document.getElementById( 'com_hsconfig_' + thisObject.prefix + '_no_info' );
	if (ele != null)
	{
		if (!foundit)
		{
			ele.style.display = 'block';
		}
		else
		{
			ele.style.display = 'none';
		}
	}
	return true;
}

this.init = function init( opts )
{
	this.baseUrl = opts.base;
	this.imgId = opts.imgId;
	this.presetId = opts.presetId;
	this.prefix = opts.prefix;
	this.sshotimg = document.getElementById( this.imgId );
	this.sspreset = document.getElementById( this.presetId );
	if (this.sspreset != null)
	{
		this.sspreset.onchange = this.onchange;
		this.onchange();
	}
}

}