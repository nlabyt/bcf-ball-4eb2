<?php
/**
 * Highslide Configuration table class
 *
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


/**
 * HsConfig Table class
 */
class HsconfigTableHsconfig extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $id = null;

	/**
	 * @var string
	 */
	var $css = null;

	/**
	 * @var string
	 */
	var $script = null;

	/**
	 * @var string
	 */
	var $overlayhtml = null;

	/**
	 * @var string
	 */
	var $skincontrols = null;

	/**
	 * @var string
	 */
	var $skincontent = null;

	/**
	 * @var text
	 */
	var $params = null;

	/**
	* @var published
	*/
	var $published = null;

	/**
	* @var publish_up
	*/
	var $publish_tmst = null;

	/**
	* @var modified
	*/
	var $modified = null;


	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(& $db) {
		parent::__construct('#__hsconfig', 'id', $db);
	}

	public function getDb()
	{
		return $this->_db;
	}

	public function getTbl()
	{
		return $this->_tbl;
	}

	public function getTblKey()
	{
		return $this->_tbl_key;
	}
}
?>