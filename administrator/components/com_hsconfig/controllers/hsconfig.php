<?php
/**
 * Highslide Configuration Controller for the component
 *
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

/**
 * HsConfigs Controller
 */
class HsConfigsControllerHsConfig extends HsConfigsController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask( 'add', 'edit' );
		$this->registerTask( 'copy', 'edit');
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'close','cancel' );
	}

	/**
	 * display the edit form
	 * @return void
	 */
	function edit()
	{
		JRequest::setVar( 'view', 'hsconfig' );

		parent::display();
	}

	/**
	 * save or apply changes to a record (and redirect to appropriate page)
	 * @return void
	 */
	function save()
	{
		// Check for request forgeries.
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$model = $this->getModel('hsconfig');
		$task 	= $this->getTask();
		$form	= $model->getForm();
		$data 	= JRequest::get( 'post' );
		if (isset( $data['jform'] ) && is_array($data['jform']))
		{
			$registry = new JRegistry();
			$registry->loadArray($data['jform']);
			$data['params'] = $registry->toString();
		}
		// Validate the posted data.
		$return = $model->validate($form, $data['params']);

		// Check for validation errors.
		if ($return === false)
		{
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if (JError::isError($errors[$i])) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'notice');
				} else {
					$app->enqueueMessage($errors[$i], 'notice');
				}
			}

			// Redirect back to the edit screen.
			$this->setRedirect(JRoute::_('index.php?option=com_hsconfig&view=hsconfig', false));
			return false;
		}

		//		$data = JRequest::get( 'post' );
		if ($data['id'] == 0)
		{
			$data['id'] = $data['cid'];
		}
		$id = $data['id'];
		$updated = ($model->store());

		if ($updated)
		{
			$msg = JText::_('COM_HSCONFIG_MSG_CONFIGURATION_SAVED') .'!';
		}
		else
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_ERROR_SAVING_CONFIGURATION') . ' - ' . $model->getError();
		}

		switch ( $task )
		{
			case 'apply':
				$link = 'index.php?option=com_hsconfig&controller=hsconfig&task=edit&cid[]='.$id;
				break;

			case 'save':
			default:
				$link = 'index.php?option=com_hsconfig';
				break;
		}
		$this->setRedirect($link, $msg);
	}

	/**
	 * remove record(s)
	 * @return void
	 */
	function remove()
	{
		$model = $this->getModel('hsconfig');
		if(!$model->delete())
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_ERROR_ONE_OR_MORE_CONFIGURATIONS_COULD_NOT_BE_DELETED' ).' - '.$model->getError();
		}
		else
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_CONFIGURATIONS_DELETED' );
		}

		$this->setRedirect( 'index.php?option=com_hsconfig', $msg );
	}

	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		// Check for request forgeries.
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$task 	= $this->getTask();

		$msg = JText::_( 'COM_HSCONFIG_MSG_OPERATION_CANCELLED' );
		$this->setRedirect( 'index.php?option=com_hsconfig', $msg );
	}

	function unpublish()
	{
		$model = $this->getModel('hsconfig');
		if(!$model->unpublish())
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_ERROR_ONE_OR_MORE_CONFIGURATIONS_COULD_NOT_BE_UNPUBLISHED' ).' - '.$model->getError();
		}
		else
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_CONFIGURATIONS_UNPUBLISHED' );
		}

		$this->setRedirect( 'index.php?option=com_hsconfig', $msg );
	}

	function publish()
	{
		$model = $this->getModel('hsconfig');
		if(!$model->publish())
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_ERROR_ONE_OR_MORE_CONFIGURATIONS_COULD_NOT_BE_PUBLISHED' ).' - '.$model->getError();
		}
		else
		{
			$msg = JText::_( 'COM_HSCONFIG_MSG_CONFIGURATIONS_PUBLISHED' );
		}

		$this->setRedirect( 'index.php?option=com_hsconfig', $msg );
	}
}
?>
