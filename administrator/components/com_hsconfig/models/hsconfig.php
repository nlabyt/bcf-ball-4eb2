<?php
/**
 * HsConfig Model for Highslide Configuration Component
 *
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');

class HsConfigsModelHsConfig extends JModelForm
{
	function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Hsconfig', $prefix = 'hsconfigTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_hsconfig.hsconfig', 'hsconfig', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to set the hsconfig identifier
	 *
	 * @access	public
	 * @param	int Hsconfig identifier
	 * @return	void
	 */
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}


	/**
	 * Method to get a hsconfig
	 * @return object with data
	 */
	function &getData()
	{
		jimport('joomla.filesystem.file');
		// Load the data
		if (empty( $this->_data )) {
			$query = ' SELECT a.*, b.title FROM #__hsconfig a '.
					 ' LEFT JOIN #__content b '.
					 ' ON a.id = b.id '.
					'  WHERE a.id = '.$this->_id;
			$this->_db->setQuery( $query );
			$this->_data = $this->_db->loadObject();
		}
		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->css = null;
			$this->_data->script = null;
			$this->_data->skincontrols = null;
			$this->_data->skincontent = null;
			$this->_data->overlayhtml = null;
			$this->_data->params = null;
			$this->_data->published = false;
			$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'css'.DS.'default.css';
			if (JFile::exists($filename))
			{
				$this->_data->css = JFile::read($filename);
			}
		}
		if ($this->state->task == 'copy')
		{
			$this->_data->id = 0;
		}
		return $this->_data;
	}

	function &getArticleList()
	{
		$query = ' SELECT a.id, a.title from #__content as a '
		        .' LEFT JOIN #__hsconfig as b on a.id = b.id '
		        .' WHERE b.modified is null';
		$this->_db->setQuery( $query );
		$this->_articleList = $this->_db->loadObjectList();
		return $this->_articleList;
	}

	/**
	 * Method to store a record
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function store()
	{
		$data = JRequest::get( 'post' );
		$rawdata = JRequest::getVar('jform', array(), 'post', 'array', JREQUEST_ALLOWRAW );
		$isnew = false;

		if ($data['id'] == 0)
		{
			$data['id'] = $data['cid'];
			$isnew = true;
		}
		$row = $this->getTable();

		$registry = new JRegistry;
		$registry->loadArray($data['jform']);
		$params = $registry->toArray();
		if (isset($rawdata['css']))
		{
			$data['css'] = $rawdata['css'];
			unset($params['css']);
		}
		if (isset($rawdata['script']))
		{
			$data['script'] = $rawdata['script'];
			unset($params['script']);
		}
		if (isset($rawdata['skincontrols']))
		{
			$data['skincontrols'] = $rawdata['skincontrols'];
			unset($params['skincontrols']);
		}
		if (isset($rawdata['skincontent']))
		{
			$data['skincontent'] = $rawdata['skincontent'];
			unset($params['skincontent']);
		}
		if (isset($rawdata['overlayhtml']))
		{
			$data['overlayhtml'] = $rawdata['overlayhtml'];
			unset($params['overlayhtml']);
		}
		if (isset($params['publishConfig']))
		{
			$data['published'] = $params['publishConfig'];
			unset($params['publishConfig']);
		}
		$reg = new JRegistry;
		$reg->loadArray( $params );
		$data['params'] = $reg->toString();

		// Bind the form fields to the hsconfig table
		if (!$row->bind($data)) {
			$this->setError($this->getDb()->getErrorMsg());
			return false;
		}

		// Make sure the hsconfig record is valid
		if (!$row->check()) {
			$this->setError($this->getDb()->getErrorMsg());
			return false;
		}

		$datenow = JFactory::getDate();
		$row->modified = $datenow->toMySQL();

		if ($isnew)
		{
			if (!$row->getDb()->insertObject( $row->getTbl(), $row, $row->getTblKey() ))
			{
				$row->setError(get_class( $row ).'::store failed - '.$row->getDb()->getErrorMsg());
				$this->setError( $this->getDb()->getErrorMsg());
				return false;
			}
		}
		else
		{
			// Store the web link table to the database
			if (!$row->store()) {
				$this->setError( $this->getDb()->getErrorMsg() );
				return false;
			}
		}


		if ($row->published)
		{
			$this->publishOne( $row->id );
		}
		else
		{
			$ispublished = HsConfigHelper::isPublished( $row->id );

			if ($ispublished)
			{
				$this->unpublishOne( $row->id );
			}
		}

		return true;
	}
	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row = $this->getTable();

		if (count( $cids ))
		{
			foreach($cids as $cid) {
				if (HsConfigHelper::isSiteConfig($cid))
				{
					$this->setError( JText::_('COM_HSCONFIG_MSG_SITE_CONFIGURATION_CANNOT_BE_DELETED').'.');
					return false;
				}
				if (HsConfigHelper::isPublished( $cid ))
				{
					$this->unpublishOne( $cid );
				}

				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function publish()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				$this->setId( $cid );
				$this->getData();
				$registry = new JRegistry();
				$registry->loadJSON($this->_data->params);
				$hs_base = JURI::root(true).'/plugins/content/highslide/';

				//	generate the configuration files
				if (!$this->_savecss( $registry, $cid, $hs_base, $this->_data ))
					return false;
				if (!$this->_savejs( $registry, $cid, $hs_base, $this->_data ))
					return false;
			}

			$datenow = JFactory::getDate();
			$row = $this->getTable();
			JArrayHelper::toInteger( $cid );
			$k			= $row->getTblKey();

			$cidx = $k . ' = ' . implode( ' OR ' . $k . '=', $cids );

			$query = 'UPDATE '. $row->getTbl()
			. ' SET published = ' . (int) 1
			. ', publish_tmst = '. "'". $datenow->toMySQL() ."'"
			. ' WHERE ('.$cidx.')'
			;

			$row->getDb()->setQuery( $query );
			if (!$row->getDb()->query())
			{
				$row->setError($row->getDb()->getErrorMsg());
				return false;
			}
		}
		return true;
	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function publishOne( $id )
	{
		$this->setId( $id );
		$this->getData();
		$registry = new JRegistry();
		$registry->loadJSON($this->_data->params);
		$hs_base = JURI::root(true).'/plugins/content/highslide/';

		//	generate the configuration files
		if (!$this->_savecss( $registry, $id, $hs_base, $this->_data ))
			return false;
		if (!$this->_savejs( $registry, $id, $hs_base, $this->_data ))
			return false;

		$datenow = JFactory::getDate();
		$row = $this->getTable();

		$query = 'UPDATE '. $row->getTbl()
		. ' SET published = ' . (int) 1
		. ', publish_tmst = '. "'". $datenow->toMySQL() ."'"
		. ' WHERE id = ' .$id.' '
		;

		$row->getDb()->setQuery( $query );
		if (!$row->getDb()->query())
		{
			$row->setError($row->getDb()->getErrorMsg());
			return false;
		}
		return true;
	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function unpublishOne( $id )
	{
		if (!$this->_delete( HsConfigHelper::createJsName( $id )))
			return false;
		if (!$this->_delete( HsConfigHelper::createCssName( $id )))
			return false;
		$row = $this->getTable();
		$row->publish( $id, false );
		return true;
	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function unpublish()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$this->_delete( HsConfigHelper::createJsName( $cid )))
					return false;
				if (!$this->_delete( HsConfigHelper::createCssName( $cid )))
					return false;
			}
			$row = $this->getTable();
			$row->publish( $cids, false );
		}
		return true;
	}

	function _getVer(){
		return ('2.5.0');
	}

	/**
	 * Save a CSS stylesheet for the given id's configuration
	 *
	 * @param mixed $registry	configuration parameters
	 * @param mixed $id			associated configuration id
	 * @param mixed $hs_base	base path of highslide installation
	 * @return 					none. errors will be thrown
	 */
	function _savecss( &$registry, $id, $hs_base, &$data )
	{

		$filelines = array();
		$filelines[] = '/* ';

		if (HsConfigHelper::isSiteConfig($id) )
		{
			$filelines[] = '*  Highslide site styles';
		}
		else
		{
			$filelines[] = '*  Highslide article specific styles';
		}

		$fname = HsConfigHelper::createCssName( $id );
		$jnow		= JFactory::getDate();

		$filelines[] = '*  DO NOT EDIT. Generated on ' . $jnow->toFormat() . ' (GMT) by the Highslide Configuration Component ' . $this->_getVer();
		$filelines[] = '*/';
		$filelines[] = '';

		//	Thumbnail visibility
		$filelines[] = '.highslide-active-anchor img {';
		$filelines[] = '	visibility: ' . $registry->getValue('thumbNail','hidden') . ';';
		$filelines[] = '}';

		// Opacity dimming
	   	$dimcolor = $registry->getValue('backgroundDimmingColor','black');
	   	$filelines[] = 	'.highslide-dimming {';
	   	$filelines[] = 	'	width: 100%;';
	   	$filelines[] = 	'	background: ' . $dimcolor . ';';
	   	$filelines[] = 	'}';

		if ($data->css != "")
		{
			$filelines[] = '/* Beginning of inserted CSS configuration parameters */';
			$newtext = preg_replace( "/url( )*\(( )*([^( ]+)/i", "url(" . JURI::root(true) . "\\3", $data->css );
			$filelines[] = $newtext;
			$filelines[] = '/* End of inserted CSS configuration parameters */';
		}

		if ($registry->getValue('ssPreset') != "")
		{
			$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'presets'.DS.'slideshow'.DS.$registry->getValue('ssPreset').'.css';
			if (JFile::exists($filename))
			{
				$presetdata = JFile::read($filename);
				$newtext = preg_replace( "/url( )*\(( )*([^( ]+)/i", "url(" . JURI::root(true) . "\\3", $presetdata );
				$filelines[] = $newtext;
			}
		}
		if ($registry->getValue('opPreset') != "")
		{
			$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'presets'.DS.'overlay'.DS.$registry->getValue('opPreset').'.css';
			if (JFile::exists($filename))
			{
				$presetdata = JFile::read($filename);
				$newtext = preg_replace( "/url( )*\(( )*([^( ]+)/i", "url(" . JURI::root(true) . "\\3", $presetdata );
				$filelines[] = $newtext;
			}
		}
		$filedata = implode( "\r\n", $filelines );

		return ($this->_write( $fname, $filedata ));
	}

	/**
	 * Save a Javascript file for the given id's configuration
	 *
	 * @param mixed $registry	configuration parameters
	 * @param mixed $id			associated configuration id
	 * @param mixed $hs_base	base path of highslide installation
	 * @return 					none. errors will be thrown
	 */
	function _savejs( &$registry, $id, $hs_base, &$data )
	{

		$filelines = array();
		$filelines[] = '/*';
		if (HsConfigHelper::isSiteConfig($id) )
		{
			$filelines[] = '*  Highslide site settings';
		}
		else
		{
			$filelines[] = '*  Highslide article specific settings';
		}

		$fname = HsConfigHelper::createJsName( $id );
		$jnow		= JFactory::getDate();

		$filelines[] = '*  DO NOT EDIT. Generated on ' . $jnow->toFormat() . ' (GMT) by the Highslide Configuration Component ' . $this->_getVer();
		$filelines[] = '*/';
		$filelines[] = '';

		// Credits display
		$filelines[] = "hs.showCredits = " . $registry->getValue('showCredits','tru') ."e;";
		if ($registry->getValue('crVPosition') != -1 || $registry->getValue('crHPosition') != -1)
		{
			$pos = "";
			$spc = "";
			if ($registry->getValue('crVPosition') != -1)
			{
				$pos .= $registry->getValue('crVPosition');
				$spc = " ";
			}
			if ($registry->getValue('crHPosition') != -1) {
				$pos .= $spc . $registry->getValue('crHPosition');
			}
			$filelines[] = "hs.creditsPosition = '" . $pos . "';";
		}

		$filelines[] = "hs.graphicsDir = 'plugins/content/highslide/graphics/';";

		// outlineType: outer-glow, rounded-white
		if ($registry->getValue('outlineType') != -1)
		{
			if ($registry->getValue('outlineType') == 'no-border' || $registry->getValue('outlineType') == "")
			{
				$filelines[] = "hs.outlineType = null;";
			}
			else
			{
				$filelines[] = "hs.outlineType = '" . $registry->getValue('outlineType') . "';";
			}
		}
		if (is_numeric($registry->getValue('outlineStartOffset')))
		{
	    	$filelines[] = "hs.outlineStartOffset = " . $registry->getValue('outlineStartOffset') . ";";
		}
		if ($registry->getValue('wrapperClassname') != '')
		{
			$filelines[] = "hs.wrapperClassName = '" . JText::_($registry->getValue('wrapperClassname')) . "';";
		}
		if ($registry->getValue('outlineWhileAnimating','2') == '2')
		{
			$filelines[] = "hs.outlineWhileAnimating = " . $registry->getValue('outlineWhileAnimating','2') . ";";
		}
		else
		{
			$filelines[] = "hs.outlineWhileAnimating = " . $registry->getValue('outlineWhileAnimating') . "e;";
		}
		if ($registry->getValue('loadingText') != '')
		{
			$filelines[] = "hs.lang.loadingText = '" . JText::_($registry->getValue('loadingText')) . "';";
		}
		if ($registry->getValue('loadingTitle') != '')
		{
			$filelines[] = "hs.lang.loadingTitle = '" . JText::_($registry->getValue('loadingTitle')) . "';";
		}
		if (is_numeric($registry->getValue('loadingOpacity')))
		{
	    	$filelines[] = "hs.loadingOpacity = " . $registry->getValue('loadingOpacity') . ";";
		}

		// Dimming background opacity
		if (is_numeric($registry->getValue('dimmingOpacity')))
		{
	    	$filelines[] = "hs.dimmingOpacity = " . $registry->getValue('dimmingOpacity') . ";";
		}
		if ($registry->getValue('captionText') != '')
		{
			$filelines[] = "hs.captionText = '" . JText::_($registry->getValue('captionText')) . "';";
		}
		if ($registry->getValue('captionEval') != '-1')
		{
			$filelines[] = "hs.captionEval = '" . $registry->getValue('captionEval') . "';";
		}
		if ($registry->getValue('headingText') != '')
		{
			$filelines[] = "hs.headingText = '" . JText::_($registry->getValue('headingText')) . "';";
		}
		if ($registry->getValue('headingEval') != '-1')
		{
			$filelines[] = "hs.headingEval = '" . $registry->getValue('headingEval') . "';";
		}
		if ($registry->getValue('maincontentText') != '')
		{
			$filelines[] = "hs.maincontentText = '" . JText::_($registry->getValue('maincontentText')) . "';";
		}
		if ($registry->getValue('maincontentEval') != '-1')
		{
			$filelines[] = "hs.maincontentEval = '" . $registry->getValue('maincontentEval') . "';";
		}
		$filelines[] = "hs.padToMinWidth = " . $registry->getValue('padToMinWidth','fals') . "e;";
		if ($registry->getValue('focusTitle') != '')
		{
			$filelines[] = "hs.lang.focusTitle = '" . JText::_($registry->getValue('focusTitle')) . "';";
		}
		if ($registry->getValue('cssDirection') != '-1')
		{
			$filelines[] = "hs.lang.cssDirection = '" . JText::_($registry->getValue('cssDirection')) . "';";
		}
		if ($registry->getValue('closeText') != '')
		{
			$filelines[] = "hs.lang.closeText = '" . JText::_($registry->getValue('closeText')) . "';";
		}
		if ($registry->getValue('closeTitle') != '')
		{
			$filelines[] = "hs.lang.closeTitle = '" . JText::_($registry->getValue('closeTitle')) . "';";
		}
		if ($registry->getValue('resizeTitle') != '')
		{
			$filelines[] = "hs.lang.resizeTitle = '" . JText::_($registry->getValue('resizeTitle')) . "';";
		}
		if ($registry->getValue('moveText') != '')
		{
			$filelines[] = "hs.lang.moveText = '" . JText::_($registry->getValue('moveText')) . "';";
		}
		if ($registry->getValue('moveTitle') != '')
		{
			$filelines[] = "hs.lang.moveTitle = '" . JText::_($registry->getValue('moveTitle')) . "';";
		}
		if ($registry->getValue('nextText') != '')
		{
			$filelines[] = "hs.lang.nextText = '" . JText::_($registry->getValue('nextText')) . "';";
		}
		if ($registry->getValue('nextTitle') != '')
		{
			$filelines[] = "hs.lang.nextTitle = '" . JText::_($registry->getValue('nextTitle')) . "';";
		}
		if ($registry->getValue('previousText') != '')
		{
			$filelines[] = "hs.lang.previousText = '" . JText::_($registry->getValue('previousText')) . "';";
		}
		if ($registry->getValue('previousTitle') != '')
		{
			$filelines[] = "hs.lang.previousTitle = '" . JText::_($registry->getValue('previousTitle')) . "';";
		}
		if ($registry->getValue('playText') != '')
		{
			$filelines[] = "hs.lang.playText = '" . JText::_($registry->getValue('playText')) . "';";
		}
		if ($registry->getValue('playTitle') != '')
		{
			$filelines[] = "hs.lang.playTitle = '" . JText::_($registry->getValue('playTitle')) . "';";
		}
		if ($registry->getValue('pauseText') != '')
		{
			$filelines[] = "hs.lang.pauseText = '" . JText::_($registry->getValue('pauseText')) . "';";
		}
		if ($registry->getValue('pauseTitle') != '')
		{
			$filelines[] = "hs.lang.pauseTitle = '" . JText::_($registry->getValue('pauseTitle')) . "';";
		}
		if (JString::strtolower($registry->getValue('expandCursor')) != 'null')
		{
			if ($registry->getValue('expandCursor') == '')
			{
				$filelines[] = "hs.expandCursor = 'zmin.cur';";
			}
			else
			{
				$filelines[] = "hs.expandCursor = '" . $registry->getValue('expandCursor') . "';";
			}
		}
		else
		{
			$filelines[] = "hs.expandCursor = null;";
		}
		if (JString::strtolower($registry->getValue('restoreCursor')) != 'null')
		{
			if ($registry->getValue('restoreCursor') == '')
			{
				$filelines[] = "hs.restoreCursor = 'zmout.cur';";
			}
			else
			{
				$filelines[] = "hs.restoreCursor = '" . $registry->getValue('restoreCursor') . "';";
			}
		}
		else
		{
			$filelines[] = "hs.restoreCursor = null;";
		}
		if ($registry->getValue('creditsHref') != '')
		{
			$filelines[] = "hs.creditsHref = '" . JText::_($registry->getValue('creditsHref')) . "';";
		}
		if ($registry->getValue('creditsTarget') != '-1')
		{
			$filelines[] = "hs.creditsTarget = '" . $registry->getValue('creditsTarget') . "';";
		}
		if ($registry->getValue('creditsText') != '')
		{
			$filelines[] = "hs.lang.creditsText = '" . JText::_($registry->getValue('creditsText')) . "';";
		}
		if ($registry->getValue('creditsTitle') != '')
		{
			$filelines[] = "hs.lang.creditsTitle = '" . JText::_($registry->getValue('creditsTitle')) . "';";
		}
		if ($registry->getValue('number') != '')
		{
			$filelines[] = "hs.lang.number = '" . JText::_($registry->getValue('number')) . "';";
		}
		if ($registry->getValue('easing') != '')
		{
			if($registry->getValue('easing') == 'null')
			{
				$filelines[] = "hs.easing = '';";
			}
			else
			{
				$filelines[] = "hs.easing = '" . $registry->getValue('easing') . "';";
			}
		}
		if ($registry->getValue('easingClose') != '')
		{
			$filelines[] = "hs.easingClose = '" . $registry->getValue('easingClose') . "';";
		}
		else
		{
			$filelines[] = "hs.easingClose = hs.easing;";
		}
		if ($registry->getValue('objectType',-1) != -1)
		{
			$filelines[] = "hs.objectType = '" . $registry->getValue('objectType') . "';";
		}
		if (is_numeric($registry->getValue('fullExpandOpacity')))
		{
	    	$filelines[] = "hs.fullExpandOpacity = " . $registry->getValue('fullExpandOpacity') . ";";
		}
		if ($registry->getValue('numberPosition') != '-1')
		{
			$filelines[] = "hs.numberPosition = '" . $registry->getValue('numberPosition') . "';";
		}
		if (is_numeric($registry->getValue('height')))
		{
	    	$filelines[] = "hs.height = " . $registry->getValue('height') . ";";
		}
		if (is_numeric($registry->getValue('width')))
		{
	    	$filelines[] = "hs.width = " . $registry->getValue('width') . ";";
		}
		if ($registry->getValue('fullExpandTitle') != '')
		{
			$filelines[] = "hs.lang.fullExpandTitle = '" . JText::_($registry->getValue('fullExpandTitle')) . "';";
		}
		if ($registry->getValue('fullExpandText') != '')
		{
			$filelines[] = "hs.lang.fullExpandText = '" . JText::_($registry->getValue('fullExpandText')) . "';";
		}
		if ($registry->getValue('targetX') != '')
		{
			$filelines[] = "hs.targetX = '" . JText::_($registry->getValue('targetX')) . "';";
		}
		if ($registry->getValue('targetY') != '')
		{
			$filelines[] = "hs.targetY = '" . JText::_($registry->getValue('targetY')) . "';";
		}
		if (is_numeric($registry->getValue('marginTop')))
		{
	    	$filelines[] = "hs.marginTop = " . $registry->getValue('marginTop') . ";";
		}
		if (is_numeric($registry->getValue('marginBottom')))
		{
	    	$filelines[] = "hs.marginBottom = " . $registry->getValue('marginBottom') . ";";
		}
		if (is_numeric($registry->getValue('marginLeft')))
		{
	    	$filelines[] = "hs.marginLeft = " . $registry->getValue('marginLeft') . ";";
		}
		if (is_numeric($registry->getValue('marginRight')))
		{
	    	$filelines[] = "hs.marginRight = " . $registry->getValue('marginRight') . ";";
		}
		if (is_numeric($registry->getValue('minHeight')))
		{
	    	$filelines[] = "hs.minHeight = " . $registry->getValue('minHeight') . ";";
		}
		if (is_numeric($registry->getValue('minWidth')))
		{
	    	$filelines[] = "hs.minWidth = " . $registry->getValue('minWidth') . ";";
		}
		if (is_numeric($registry->getValue('maxHeight')))
		{
			$filelines[] = "hs.maxHeight = " . $registry->getValue('maxHeight') . ";";
		}
		if (is_numeric($registry->getValue('maxWidth')))
		{
			$filelines[] = "hs.maxWidth = " . $registry->getValue('maxWidth') . ";";
		}
		if (is_numeric($registry->getValue('objectHeight')))
		{
	    	$filelines[] = "hs.objectHeight = " . $registry->getValue('objectHeight') . ";";
		}
		if (is_numeric($registry->getValue('objectWidth')))
		{
	    	$filelines[] = "hs.objectWidth = " . $registry->getValue('objectWidth') . ";";
		}
		if (is_numeric($registry->getValue('numberOfImagesToPreload')))
		{
	    	$filelines[] = "hs.numberOfImagesToPreload = " . $registry->getValue('numberOfImagesToPreload') . ";";
		}
		if ($registry->getValue('transitions') != '')
		{
			$filelines[] = "hs.transitions = [" . JText::_($registry->getValue('transitions')) . "];";
		}

		if ($registry->getValue('fullExpandVPosition') != -1 || $registry->getValue('fullExpandHPosition') != -1)
		{
			$pos = "";
			$spc = "";
			if ($registry->getValue('fullExpandVPosition') != -1)
			{
				$pos .= $registry->getValue('fullExpandVPosition');
				$spc = " ";
			}
			if ($registry->getValue('fullExpandHPosition') != -1) {
				$pos .= $spc . $registry->getValue('fullExpandHPosition');
			}
			$filelines[] = "hs.fullExpandPosition = '" . $pos . "';";
		}
		$filelines[] = "hs.objectLoadTime = '" . $registry->getValue('objectLoadTime','before') . "';";
		$filelines[] = "hs.align = '" . $registry->getValue('align') . "';";
		$filelines[] = "hs.anchor = '" . $registry->getValue('anchor') . "';";
		$filelines[] = "hs.allowSizeReduction = " . $registry->getValue('allowSizeReduction','tru') . "e;";
		$filelines[] = "hs.fadeInOut = " . $registry->getValue('fadeInOut','fals') . "e;";
		$filelines[] = "hs.allowMultipleInstances = " . $registry->getValue('allowMultipleInstances','tru') . "e;";
		$filelines[] = "hs.allowWidthReduction = " . $registry->getValue('allowWidthReduction','fals') . "e;";
		$filelines[] = "hs.allowHeightReduction = " . $registry->getValue('allowHeightReduction','tru') . "e;";
		$filelines[] = "hs.blockRightClick = " . $registry->getValue('blockRightClick','fals') . "e;";
		$filelines[] = "hs.enableKeyListener = " . $registry->getValue('enableKeyListener','tru') . "e;";
		$filelines[] = "hs.dynamicallyUpdateAnchors = ". $registry->getValue('dynamicallyUpdateAnchors','tru') . "e;";
		$filelines[] = "hs.forceAjaxReload = ". $registry->getValue('forceAjaxReload','fals') . "e;";
		$filelines[] = "hs.allowSimultaneousLoading = ". $registry->getValue('allowSimultaneousLoading','fals') . "e;";
		$filelines[] = "hs.useBox = ". $registry->getValue('useBox','fals') . "e;";
		$filelines[] = "hs.cacheAjax = " . $registry->getValue('cacheAjax','tru') . "e;";
		$filelines[] = "hs.preserveContent = " . $registry->getValue('preserveContent','tru') . "e;";
		$filelines[] = "hs.dragByHeading = " . $registry->getValue('dragbyheading','tru') . "e;";
		if ($registry->getValue('openerTagNames') != '')
		{
			$otn = explode( ',', $registry->getValue('openerTagNames'));
			$otntext = "";
			$sep = " ";
			foreach ($otn as $tn)
			{
				$otntext .= $sep."'".JString::trim($tn)."'";
				$sep = ", ";
			}
			$filelines[] = "hs.openerTagNames = [" . $otntext . " ];";
		}
		if (is_numeric($registry->getValue('dragSensitivity')))
		{
			$filelines[] = "hs.dragSensitivity = " . $registry->getValue('dragSensitivity') . ";";
		}
		if (is_numeric($registry->getValue('dimmingDuration')))
		{
			$filelines[] = "hs.dimmingDuration = " . $registry->getValue('dimmingDuration') . ";";
		}
		if (is_numeric($registry->getValue('expandDuration')))
		{
			$filelines[] = "hs.expandDuration = " . $registry->getValue('expandDuration') . ";";
		}
		if (is_numeric($registry->getValue('transitionDuration')))
		{
			$filelines[] = "hs.transitionDuration = " . $registry->getValue('transitionDuration') . ";";
		}
		if (is_numeric($registry->getValue('expandSteps')))
		{
			$filelines[] = "hs.expandSteps = " . $registry->getValue('expandSteps') . ";";
		}
		if (is_numeric($registry->getValue('restoreCursorDuration')))
		{
			$filelines[] = "hs.restoreCursorDuration = " . $registry->getValue('restoreCursorDuration') . ";";
		}
		if (is_numeric($registry->getValue('restoreCursorSteps')))
		{
			$filelines[] = "hs.restoreCursorSteps = " . $registry->getValue('restoreCursorSteps') . ";";
		}
		if (is_numeric($registry->getValue('zIndexCounter')))
		{
			$filelines[] = "hs.zIndexCounter = " . $registry->getValue('zIndexCounter') . ";";
		}
		if ($registry->getValue('restoreTitle') != '')
		{
			$filelines[] = "hs.lang.restoreTitle = '" . JText::_($registry->getValue('restoreTitle')) . "';";
		}
		if ($registry->getValue('caEnableCaptionOverlay',0) == 1)
		{
			$filelines[] = "hs.captionOverlay.fade = " . $registry->getValue('caFade',0) . ";";
			if ($registry->getValue('caOVVPosition') != -1 || $registry->getValue('caOVHPosition') != -1)
			{
				$pos = "";
				$spc = "";
				if ($registry->getValue('caOVVPosition') != -1)
				{
					$pos .= $registry->getValue('caOVVPosition');
					$spc = " ";
				}
				if ($registry->getValue('caOVHPosition') != -1) {
					$pos .= $spc . $registry->getValue('caOVHPosition');
				}
				$filelines[] = "hs.captionOverlay.position = '" . $pos . "';";
			}
			if (is_numeric($registry->getValue('caOVOffsetX')))
			{
				$filelines[] = "hs.captionOverlay.offsetX = " . $registry->getValue('caOVOffsetX') .";";
			}
			if (is_numeric($registry->getValue('caOVOffsetY')))
			{
				$filelines[] = "hs.captionOverlay.offsetY = " . $registry->getValue('caOVOffsetY') .";";
			}
			if ($registry->getValue('caOVRelativeTo') != -1)
			{
				$filelines[] = "hs.captionOverlay.relativeTo = '" . $registry->getValue('caOVRelativeTo') . "';";
			}
			$filelines[] = "hs.captionOverlay.hideOnMouseOut = " . $registry->getValue('caHideOnMouseOut','fals') . "e;";
			if (is_numeric($registry->getValue('caOpacity')))
			{
				$filelines[] = "hs.captionOverlay.opacity = " . $registry->getValue('caOpacity') .";";
			}
			if ($registry->getValue('caOVWidth') != '')
			{
				$filelines[] = "hs.captionOverlay.width = '" . $registry->getValue('caOVWidth') . "';";
			}
			if ($registry->getValue('caOVClassname') != '')
			{
				$filelines[] = "hs.captionOverlay.className = '" . $registry->getValue('caOVClassname') . "';";
			}
		}
		if ($registry->getValue('hdEnableHeadingOverlay',0) == 1)
		{
			$filelines[] = "hs.headingOverlay.fade = " . $registry->getValue('hdFade',0) . ";";

			if ($registry->getValue('hdOVVPosition') != -1 || $registry->getValue('hdOVHPosition') != -1)
			{
				$pos = "";
				$spc = "";
				if ($registry->getValue('hdOVVPosition') != -1)
				{
					$pos .= $registry->getValue('hdOVVPosition');
					$spc = " ";
				}
				if ($registry->getValue('hdOVHPosition') != -1) {
					$pos .= $spc . $registry->getValue('hdOVHPosition');
				}
				$filelines[] = "hs.headingOverlay.position = '" . $pos . "';";
			}
			if (is_numeric($registry->getValue('hdOVOffsetX')))
			{
				$filelines[] = "hs.headingOverlay.offsetX = " . $registry->getValue('hdOVOffsetX') .";";
			}
			if (is_numeric($registry->getValue('hdOVOffsetY')))
			{
				$filelines[] = "hs.headingOverlay.offsetY = " . $registry->getValue('hdOVOffsetY') .";";
			}
			if ($registry->getValue('hdOVRelativeTo') != -1)
			{
				$filelines[] = "hs.headingOverlay.relativeTo = '" . $registry->getValue('hdOVRelativeTo') . "';";
			}
			$filelines[] = "hs.headingOverlay.hideOnMouseOut = " . $registry->getValue('hdHideOnMouseOut','fals') . "e;";
			if (is_numeric($registry->getValue('hdOpacity')))
			{
				$filelines[] = "hs.headingOverlay.opacity = " . $registry->getValue('hdOpacity') .";";
			}
			if ($registry->getValue('hdOVWidth') != '')
			{
				$filelines[] = "hs.headingOverlay.width = '" . $registry->getValue('hdOVWidth') . "';";
			}
			if ($registry->getValue('hdOVClassname') != '')
			{
				$filelines[] = "hs.headingOverlay.className = '" . $registry->getValue('hdOVClassname') . "';";
			}
		}
		if($registry->getValue("mouseHoverAction") != "")
		{
			if ($registry->getValue("mouseHoverAction") == "focus")
			{
				$filelines[] = "hs.Expander.prototype.onMouseOver = function (sender) { ";
				$filelines[] = "   sender.focus();";
				$filelines[] = "};";
			}
			if ($registry->getValue("mouseHoverAction") == "close")
			{
				$filelines[] = "hs.Expander.prototype.onMouseOut = function (sender) { ";
				$filelines[] = "   sender.close();";
				$filelines[] = "};";
			}
		}
		if ($registry->getValue("modalWhenDimmed") == "yes")
		{
			$filelines[] = "hs.onDimmerClick = function () { ";
			$filelines[] = "   return false;";
			$filelines[] = "};";
		}

		$filelines[] = "hs.Expander.prototype.onBeforeGetCaption = function(sender)";
		$filelines[] = "{";
		$filelines[] = "	if (typeof sender.captionId != 'undefined' && sender.captionId != null)";
		$filelines[] = "	{";
		$filelines[] = "		if ((document.getElementById( sender.captionId ) || hs.clones[ sender.captionId ]) == null && sender.a.onclick != null)";
		$filelines[] = "		{";
		$filelines[] = "			var onclick = sender.a.onclick.toString();";
		$filelines[] = "			var onclickprop = onclick.match(/hsjcaption([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "			if (onclickprop != null)";
		$filelines[] = "			{";
		$filelines[] = "				var text = unescape( onclickprop[3] );";
		$filelines[] = "				var div = document.createElement('div');";
		$filelines[] = "				div['innerHTML'] = hs.replaceLang( text );";
		$filelines[] = "				div['id'] = sender.captionId;";
		$filelines[] = "				div['className'] = 'highslide-caption';";
		$filelines[] = "				var onclickstyle = onclick.match(/hsjcaptionstyle([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "				if (onclickstyle != null)";
		$filelines[] = "				{";
		$filelines[] = "					var styles = onclickstyle[3].match(/([^:; ])*:\s*([^,;}]*)/g);";
		$filelines[] = "					if (styles != null)";
		$filelines[] = "					{";
		$filelines[] = "						for (var i = 0; i < styles.length; i++)";
		$filelines[] = "						{";
		$filelines[] = "							var arr;";
		$filelines[] = "							arr = styles[i].split(\":\");";
		$filelines[] = "							div.style[arr[0]] = arr[1].replace( \" \", \"\");";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "				}";
		$filelines[] = "				if (sender.a.nodeName == \"A\")";
		$filelines[] = "				{";
		$filelines[] = "					sender.a.appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "				else";
		$filelines[] = "				{";
		$filelines[] = "					var x = document.getElementsByTagName('DIV');";
		$filelines[] = "					x[x.length-1].appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "			}";
		$filelines[] = "		}";
		$filelines[] = "	}";
		$filelines[] = "	return true;";
		$filelines[] = "}";
		$filelines[] = "hs.Expander.prototype.onBeforeGetHeading = function(sender)";
		$filelines[] = "{";
		$filelines[] = "	if (typeof sender.headingId != 'undefined' && sender.headingId != null)";
		$filelines[] = "	{";
		$filelines[] = "		if ((document.getElementById( sender.headingId ) || hs.clones[ sender.headingId ]) == null && sender.a.onclick != null)";
		$filelines[] = "		{";
		$filelines[] = "			var onclick = sender.a.onclick.toString();";
		$filelines[] = "			var onclickprop = onclick.match(/hsjheading([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "			if (onclickprop != null)";
		$filelines[] = "			{";
		$filelines[] = "				var text = unescape( onclickprop[3] );";
		$filelines[] = "				var div = document.createElement('div');";
		$filelines[] = "				div['innerHTML'] = hs.replaceLang( text );";
		$filelines[] = "				div['id'] = sender.headingId;";
		$filelines[] = "				div['className'] = 'highslide-heading';";
		$filelines[] = "				var onclickstyle = onclick.match(/hsjheadingstyle([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "				if (onclickstyle != null)";
		$filelines[] = "				{";
		$filelines[] = "					var styles = onclickstyle[3].match(/([^:; ])*:\s*([^,;}]*)/g);";
		$filelines[] = "					if (styles != null)";
		$filelines[] = "					{";
		$filelines[] = "						for (var i = 0; i < styles.length; i++)";
		$filelines[] = "						{";
		$filelines[] = "							var arr;";
		$filelines[] = "							arr = styles[i].split(\":\");";
		$filelines[] = "							div.style[arr[0]] = arr[1].replace( \" \", \"\");";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "				}";
		$filelines[] = "				if (sender.a.nodeName == \"A\")";
		$filelines[] = "				{";
		$filelines[] = "					sender.a.appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "				else";
		$filelines[] = "				{";
		$filelines[] = "					var x = document.getElementsByTagName('DIV');";
		$filelines[] = "					x[x.length-1].appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "			}";
		$filelines[] = "		}";
		$filelines[] = "	}";
		$filelines[] = "	return true;";
		$filelines[] = "}";
		$filelines[] = "hs.Expander.prototype.onBeforeGetContent = function(sender)";
		$filelines[] = "{";
		$filelines[] = "	if (typeof sender.contentId != 'undefined' && sender.contentId != null)";
		$filelines[] = "	{";
		$filelines[] = "		if ((document.getElementById( sender.contentId ) || hs.clones[ sender.contentId ]) == null && sender.a.onclick != null)";
		$filelines[] = "		{";
		$filelines[] = "			var onclick = sender.a.onclick.toString();";
		$filelines[] = "			var onclickprop = onclick.match(/hsjcontent([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "			if (onclickprop != null)";
		$filelines[] = "			{";
		$filelines[] = "				var text = unescape( onclickprop[3] );";
		$filelines[] = "				var div = document.createElement('div');";
		$filelines[] = "				div['innerHTML'] = hs.replaceLang( text );";
		$filelines[] = "				div['id'] = sender.contentId;";
		$filelines[] = "				div['className'] = 'highslide-html-content';";
		$filelines[] = "				var onclickstyle = onclick.match(/hsjcontentstyle([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "				if (onclickstyle != null)";
		$filelines[] = "				{";
		$filelines[] = "					var styles = onclickstyle[3].match(/([^:; ])*:\s*([^,;}]*)/g);";
		$filelines[] = "					if (styles != null)";
		$filelines[] = "					{";
		$filelines[] = "						for (var i = 0; i < styles.length; i++)";
		$filelines[] = "						{";
		$filelines[] = "							var arr;";
		$filelines[] = "							arr = styles[i].split(\":\");";
		$filelines[] = "							div.style[arr[0]] = arr[1].replace( \" \", \"\");";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "				}";
		$filelines[] = "				if (sender.a.nodeName == \"A\")";
		$filelines[] = "				{";
		$filelines[] = "					sender.a.appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "				else";
		$filelines[] = "				{";
		$filelines[] = "					var x = document.getElementsByTagName('DIV');";
		$filelines[] = "					x[x.length-1].appendChild( div );";
		$filelines[] = "				}";
		$filelines[] = "			}";
		$filelines[] = "		}";
		$filelines[] = "	}";
		$filelines[] = "	return true;";
		$filelines[] = "}";
		if ($registry->getValue('htEnableSkin','0') == '1')
		{
			if ($data->skincontrols != '')
			{
				$data->skincontrols = preg_replace("/[\n|\r|\t]/", "", $data->skincontrols );
				$data->skincontrols = addslashes( $data->skincontrols );
				$filelines[] = "hs.skin.controls = '" . $data->skincontrols . "';";
			}
			if ($data->skincontent != '')
			{
				$data->skincontent = preg_replace("/[\n|\r|\t]/", "", $data->skincontent );
				$data->skincontent = addslashes( $data->skincontent );
				$filelines[] = "hs.skin.contentWrapper = '" . $data->skincontent . "';";
			}
		}
		if ( $registry->getValue('flVersion') != ""
		   ||$registry->getValue('flExpressInstallURL') != ""
		   ||$registry->getValue('flFlashvars') != ""
		   ||$registry->getValue('flParams') != ""
		   ||$registry->getValue('flAttribs') != ""
		   )
		{
			$flversion = ($registry->getValue('flVersion') != "") ? $registry->getValue('flVersion') : "7";
			$filelines[] = "hs.swfOptions = { ";
			$filelines[] = "	version: '" . $flversion . "'";
			if ($registry->getValue('flExpressInstallURL') != "")
			{
				$filelines[] = "	,expressInstallSwfurl: '" . addslashes($registry->getValue('flExpressInstallURL')) . "'";
			}
			if ($registry->getValue('flFlashvars') != "")
			{
				$filelines[] = "	,flashvars: { " . $this->_filterVars( $registry->getValue('flFlashvars') ) . " }";
			}
			if ($registry->getValue('flParams') != "")
			{
				$filelines[] = "	,params: { " . $this->_filterVars( $registry->getValue('flParams') ) . " }";
			}
			if ($registry->getValue('flAttribs') != "")
			{
				$filelines[] = "	,attributes: { " . $this->_filterVars( $registry->getValue('flAttribs') ) . " }";
			}
			$filelines[] = "};";
		}
		if ($registry->getValue("ovOverlayId") != "")
		{
			if ($data->overlayhtml != "")
			{
				$filelines[] = "hs.Expander.prototype.onCreateOverlay = function(sender, e)";
				$filelines[] = "{";
				$filelines[] = "    if (e.overlay.innerHTML.indexOf( '{thumbalt}', 0 ) != -1) {";
				$filelines[] = "		e.overlay.innerHTML = e.overlay.innerHTML.replace('{thumbalt}', sender.thumb.alt);";
				$filelines[] = "	}";
				$filelines[] = "    if (e.overlay.innerHTML.indexOf( '{thumbtitle}', 0 ) != -1) {";
				$filelines[] = "		e.overlay.innerHTML = e.overlay.innerHTML.replace('{thumbtitle}', sender.thumb.title);";
				$filelines[] = "	}";
				$filelines[] = "    if (e.overlay.innerHTML.indexOf( '{popuptitle}', 0 ) != -1) {";
				$filelines[] = "		e.overlay.innerHTML = e.overlay.innerHTML.replace('{popuptitle}', sender.a.title);";
				$filelines[] = "	}";
				$filelines[] = "   return true;";
				$filelines[] = "}";
			}

			$filelines[] = "hs.registerOverlay(";
			$filelines[] = "{";
			if ($registry->getValue("ovThumbnailId") == "")
			{
				$filelines[] = "    thumbnailId: null,";
			}
			else
			{
				$filelines[] = "    thumbnailId: '" . $registry->getValue("ovThumbnailId") . "',";
			}
			if ($registry->getValue("ovSlideshowGroup") != "")
			{
				$filelines[] = "    slideshowGroup: '" . $registry->getValue("ovSlideshowGroup") . "',";
			}
			$filelines[] = "    fade: " . $registry->getValue("ovFade") . ",";
			if ($data->overlayhtml != "")
			{
				$data->overlayhtml = preg_replace("/[\n|\r|\t]/", "", $data->overlayhtml );
				$data->overlayhtml = addslashes( $data->overlayhtml );
				$filelines[] = "    html: hs.replaceLang( '".$data->overlayhtml."' ),";
			}
			else
			{
				$filelines[] = "    overlayId: '" . $registry->getValue("ovOverlayId") . "',";
			}
			if ($registry->getValue('ovVPosition') != -1 || $registry->getValue('ovHPosition') != -1)
			{
				$pos = "";
				$spc = "";
				if ($registry->getValue('ovVPosition') != -1)
				{
					$pos .= $registry->getValue('ovVPosition');
					$spc = " ";
				}
				if ($registry->getValue('ovHPosition') != -1) {
					$pos .= $spc . $registry->getValue('ovHPosition');
				}
				$filelines[] = "    position: '" . $pos . "',";
			}
			if (is_numeric($registry->getValue('ovOVOffsetX')))
			{
				$filelines[] = "    offsetX: " . $registry->getValue("ovOVOffsetX") . ",";
			}
			if (is_numeric($registry->getValue('ovOVOffsetY')))
			{
				$filelines[] = "    offsetY: " . $registry->getValue("ovOVOffsetY") . ",";
			}
			if ($registry->getValue('ovOVRelativeTo') != -1)
			{
				$filelines[] = "    relativeTo: '" . $registry->getValue("ovOVRelativeTo") . "',";
			}
			$filelines[] = "    hideOnMouseOut: " . $registry->getValue("ovHideOnMouseOut",'tru') . "e,";
			if (is_numeric($registry->getValue('ovOpacity')))
			{
				$filelines[] = "    opacity: " . $registry->getValue("ovOpacity") . ",";
			}
			if ($registry->getValue('ovWidth') != '')
			{
				$filelines[] = "    width: '" . $registry->getValue('ovWidth') . "',";
			}
			if ($registry->getValue('ovOVClassname') != '')
			{
				$filelines[] = "    className: '" . $registry->getValue('ovOVClassname') . "',";
			}
			$filelines[] = "    useOnHtml: " . $registry->getValue("ovUseOnHtml",'fals') . "e";
			$filelines[] = "});";
		}
		if ($registry->getValue('opPreset') != -1)
		{
			$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'presets'.DS.'overlay'.DS.$registry->getValue('opPreset').'.html';
			if (JFile::exists($filename))
			{
				$oppresetdata = JFile::read($filename);
				$filelines[] = "hs.registerOverlay(";
				$filelines[] = "{";
				if ($registry->getValue("opThumbnailId") == "")
				{
					$filelines[] = "    thumbnailId: null,";

				}
				else
				{
					$filelines[] = "    thumbnailId: '" . $registry->getValue("opThumbnailId") . "',";
				}
				if ($registry->getValue("opSlideshowGroup") != "")
				{
					$filelines[] = "    slideshowGroup: '" . $registry->getValue("opSlideshowGroup") . "',";
				}
				$filelines[] = "    fade: " . $registry->getValue("opFade",'1') . ",";
				$oppresetdata = preg_replace("/[\n|\r|\t]/", "", $oppresetdata );
				$oppresetdata = addslashes( $oppresetdata );
				$filelines[] = "    html: hs.replaceLang( '".$oppresetdata."' ),";
				if ($registry->getValue('opVPosition') != -1 || $registry->getValue('opHPosition') != -1)
				{
					$pos = "";
					$spc = "";
					if ($registry->getValue('opVPosition') != -1)
					{
						$pos .= $registry->getValue('opVPosition');
						$spc = " ";
					}
					if ($registry->getValue('opHPosition') != -1) {
						$pos .= $spc . $registry->getValue('opHPosition');
					}
					$filelines[] = "    position: '" . $pos . "',";
				}
				if (is_numeric($registry->getValue('opOVOffsetX')))
				{
					$filelines[] = "    offsetX: " . $registry->getValue("opOVOffsetX") . ",";
				}
				if (is_numeric($registry->getValue('opOVOffsetY')))
				{
					$filelines[] = "    offsetY: " . $registry->getValue("opOVOffsetY") . ",";
				}
				if ($registry->getValue('opOVRelativeTo',-1) != -1)
				{
					$filelines[] = "    relativeTo: '" . $registry->getValue("opOVRelativeTo") . "',";
				}
				$filelines[] = "    hideOnMouseOut: " . $registry->getValue("opHideOnMouseOut",'tru') . "e,";
				if (is_numeric($registry->getValue('opOpacity')))
				{
					$filelines[] = "    opacity: " . $registry->getValue("opOpacity") . ",";
				}
				if ($registry->getValue('opWidth') != '')
				{
					$filelines[] = "    width: '" . $registry->getValue('opWidth') . "',";
				}
				if ($registry->getValue('opOVClassname') != '')
				{
					$filelines[] = "    className: '" . $registry->getValue('opOVClassname') . "',";
				}
				$filelines[] = "    useOnHtml: " . $registry->getValue("opUseOnHtml",'fals') . "e";
				$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'presets'.DS.'overlay'.DS.$registry->getValue('opPreset').'.js';
				if (JFile::exists($filename))
				{
					$filelines[] = JFile::read($filename);
				}
				else
				{
					$filelines[] = "});";
				}
			}
		}

		$filelines[] = "hs.Expander.prototype.onBeforeExpand = function (sender) {";
		$filelines[] = "	if (this.custom != null";
		$filelines[] = "       &&this.custom['overlayId'] != null)";
		$filelines[] = "    {";
		$filelines[] = "		sender.createOverlay( this.custom );";
		$filelines[] = "	}";
		$filelines[] = "	return true;";
		$filelines[] = "}";

		$filelines[] = "hs.Expander.prototype.onInit = function (sender)";
		$filelines[] = "{";
		$filelines[] = "	if (sender.a.onclick != null)";
		$filelines[] = "	{";
		$filelines[] = "		var onclick = sender.a.onclick.toString();";
		$filelines[] = "		var onclickprop = onclick.match(/overlayId([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "		if (onclickprop != null)";
		$filelines[] = "		{";
		$filelines[] = "			var overlayId = onclickprop[3];";
		$filelines[] = "			if ((document.getElementById( overlayId ) || hs.clones[ overlayId ]) == null)";
		$filelines[] = "			{";
		$filelines[] = "				var onclickprop = onclick.match(/hsjcustomOverlay([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "				if (onclickprop != null)";
		$filelines[] = "				{";
		$filelines[] = "					var text = unescape( onclickprop[3] );";
		$filelines[] = "					var div = document.createElement('div');";
		$filelines[] = "					div['innerHTML'] = hs.replaceLang( text );";
		$filelines[] = "					div['id'] = overlayId;";
		$filelines[] = "					div['className'] = 'highslide-overlay';";
		$filelines[] = "					var onclickstyle = onclick.match(/hsjcustomOverlayStyle([\"']*):\s*('|\")([^'\"]*)/);";
		$filelines[] = "					if (onclickstyle != null)";
		$filelines[] = "					{";
		$filelines[] = "						var styles = onclickstyle[3].match(/([^:; ])*:\s*([^,;}]*)/g);";
		$filelines[] = "						if (styles != null)";
		$filelines[] = "						{";
		$filelines[] = "							for (var i = 0; i < styles.length; i++)";
		$filelines[] = "							{";
		$filelines[] = "								var arr;";
		$filelines[] = "								arr = styles[i].split(\":\");";
		$filelines[] = "								div.style[arr[0]] = arr[1].replace( \" \", \"\");";
		$filelines[] = "							}";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "					sender.a.appendChild( div );";
		$filelines[] = "					var overlayExists = false;";
		$filelines[] = "					for (var i = 0; i < hs.overlays.length; i++)";
		$filelines[] = "					{";
		$filelines[] = "						if (hs.overlays[i].overlayId == overlayId)";
		$filelines[] = "						{";
		$filelines[] = "							overlayExists = true;";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "					if (! overlayExists)";
		$filelines[] = "					{";
		$filelines[] = "						onclickprop = onclick.match(/customOverlay([\"']*):\s*{\s*([^}]*)}/);";
		$filelines[] = "						if (onclickprop != null)";
		$filelines[] = "						{";
		$filelines[] = "							try";
		$filelines[] = "							{";
		$filelines[] = "								eval( \"var opts = {\" + onclickprop[2] + \"}\" );";
		$filelines[] = "								opts.overlayId = overlayId;";
		$filelines[] = "								if (typeof sender.thumb.id != \"undefined\" && sender.thumb.id != \"\")";
		$filelines[] = "								{";
		$filelines[] = "									opts.thumbnailId = sender.thumb.id;";
		$filelines[] = "									hs.registerOverlay( opts );";
		$filelines[] = "								}";
		$filelines[] = "								else";
		$filelines[] = "								if (typeof sender.a.id != \"undefined\" && sender.a.id != \"\")";
		$filelines[] = "								{";
		$filelines[] = "									opts.thumbnailId = sender.a.id;";
		$filelines[] = "									hs.registerOverlay( opts );";
		$filelines[] = "								}";
		$filelines[] = "							}";
		$filelines[] = "							catch(e)";
		$filelines[] = "							{";
		$filelines[] = "								//	ignore";
		$filelines[] = "							}";
		$filelines[] = "						}";
		$filelines[] = "					}";
		$filelines[] = "				}";
		$filelines[] = "			}";
		$filelines[] = "		}";
		$filelines[] = "	}";

		$filelines[] = "	return true;";
		$filelines[] = "}";
		if ($registry->getValue('ssEnableSlideshow',0) == 1)
		{
			$filelines[] = "hs.addSlideshow( {";
			if (is_numeric($registry->getValue('ssInterval')))
			{
				$filelines[] = "	interval: " . $registry->getValue('ssInterval');
			}
			if ($registry->getValue('ssSlideshowGroup') != '')
			{
				$ssg = explode( ',', $registry->getValue('ssSlideshowGroup'));
				$ssgtext = "";
				$sep = " ";
				foreach ($ssg as $ss)
				{
					$ssgtext .= $sep."'".JString::trim($ss)."'";
					$sep = ", ";
				}
				$filelines[] = "	,slideshowGroup: [" . $ssgtext . " ]";
			}
			$filelines[] = "	,repeat: " . $registry->getValue('ssRepeat','fals') . "e";
			$filelines[] = "	,useControls: " . $registry->getValue('ssUseControls','tru') . "e";
			if ($registry->getValue('ssFixedControls','tru') == 'fit')
			{
				$filelines[] = "	,fixedControls: 'fit'";
			}
			else
			{
				$filelines[] = "	,fixedControls: " . $registry->getValue('ssFixedControls') . "e";
			}
			if ($registry->getValue('ssUseControls','tru') == 'tru')
			{
				$filelines[] = "	,overlayOptions: {";
				$filelines[] = "		fade: " . $registry->getValue('ssFade',1);
				if ($registry->getValue('ssOVVPosition') != -1 || $registry->getValue('ssOVHPosition') != -1)
				{
					$pos = "";
					$spc = "";
					if ($registry->getValue('ssOVVPosition') != -1)
					{
						$pos .= $registry->getValue('ssOVVPosition');
						$spc = " ";
					}
					if ($registry->getValue('ssOVHPosition') != -1) {
						$pos .= $spc . $registry->getValue('ssOVHPosition');
					}
					$filelines[] = "		,position: '" . $pos . "'";
				}
				if (is_numeric($registry->getValue('ssOVOffsetX')))
				{
					$filelines[] = "    	,offsetX: " . $registry->getValue("ssOVOffsetX");
				}
				if (is_numeric($registry->getValue('ssOVOffsetY')))
				{
					$filelines[] = "    	,offsetY: " . $registry->getValue("ssOVOffsetY");
				}
				if ($registry->getValue('ssOVRelativeTo',-1) != -1)
				{
					$filelines[] = "    	,relativeTo: '" . $registry->getValue("ssOVRelativeTo") . "'";
				}
				$filelines[] = "    	,hideOnMouseOut: " . $registry->getValue('ssHideOnMouseOut','tru') . "e";
				if (is_numeric($registry->getValue('ssOpacity')))
				{
					$filelines[] = "    	,opacity: " . $registry->getValue('ssOpacity');
				}
				if ($registry->getValue('ssOVWidth') != '')
				{
					$filelines[] = "		,width: '" . $registry->getValue('ssOVWidth') . "'";
				}
				if ($registry->getValue('ssOVClassname') != '')
				{
					$filelines[] = "    	,className: '" . $registry->getValue('ssOVClassname') . "'";
				}
				$filelines[] = "	}";
			}
			if ($registry->getValue('tsEnableThumbstrip',0) == 1)
			{
				$filelines[] = "	,thumbstrip: {";
				$filelines[] = "		fade: " . $registry->getValue('tsFade',1);
				if ($registry->getValue('tsOVVPosition') != -1 || $registry->getValue('tsOVHPosition') != -1)
				{
					$pos = "";
					$spc = "";
					if ($registry->getValue('tsOVVPosition') != -1)
					{
						$pos .= $registry->getValue('tsOVVPosition');
						$spc = " ";
					}
					if ($registry->getValue('tsOVHPosition') != -1) {
						$pos .= $spc . $registry->getValue('tsOVHPosition');
					}
					$filelines[] = "		,position: '" . $pos . "'";
				}
				if (is_numeric($registry->getValue('tsOVOffsetX')))
				{
					$filelines[] = "    	,offsetX: " . $registry->getValue("tsOVOffsetX");
				}
				if (is_numeric($registry->getValue('tsOVOffsetY')))
				{
					$filelines[] = "    	,offsetY: " . $registry->getValue("tsOVOffsetY");
				}
				if ($registry->getValue('tsOVMode',-1) != -1)
				{
					$filelines[] = "    	,mode: '" . $registry->getValue("tsOVMode") . "'";
				}
				if ($registry->getValue('tsOVRelativeTo',-1) != -1)
				{
					$filelines[] = "    	,relativeTo: '" . $registry->getValue("tsOVRelativeTo") . "'";
				}
				$filelines[] = "    	,hideOnMouseOut: " . $registry->getValue('tsHideOnMouseOut','tru') . "e";
				if (is_numeric($registry->getValue('tsOpacity')))
				{
					$filelines[] = "    	,opacity: " . $registry->getValue('tsOpacity');
				}
				if ($registry->getValue('tsOVWidth') != '')
				{
					$filelines[] = "		,width: '" . $registry->getValue('tsOVWidth') . "'";
				}
				if ($registry->getValue('tsOVClassname') != '')
				{
					$filelines[] = "    	,className: '" . $registry->getValue('tsOVClassname') . "'";
				}
				$filelines[] = "	}";
			}
			$filelines[] = "});";
			$filelines[] = "hs.autoplay = " . $registry->getValue('ssAutoplay','fals') . "e;";
		}

		if ($registry->getValue('ssPreset') != -1)
		{
			$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'presets'.DS.'slideshow'.DS.$registry->getValue('ssPreset').'.js';
			if (JFile::exists($filename))
			{
				$presetdata = JFile::read($filename);
				$filelines[] = $presetdata;
				if (is_numeric($registry->getValue('sspInterval')))
				{
					$filelines[] = "	,interval: " . $registry->getValue('sspInterval');
				}
				$filelines[] = "	,repeat: " . $registry->getValue('sspRepeat','fals') . "e";
				if ($registry->getValue('sspSlideshowGroup') != '')
				{
					$ssg = explode( ',', $registry->getValue('sspSlideshowGroup'));
					$ssgtext = "";
					$sep = " ";
					foreach ($ssg as $ss)
					{
						$ssgtext .= $sep."'".JString::trim($ss)."'";
						$sep = ", ";
					}
					$filelines[] = "	,slideshowGroup: [" . $ssgtext . " ]";
				}
				$filelines[] = "});";
				$filelines[] = "hs.autoplay = " . $registry->getValue('sspAutoplay','fals') . "e;";
			}
		}

		if ($data->script != "")
		{
			$filelines[] = '/* Beginning of inserted user defined script */';
			$filelines[] = $data->script;
			$filelines[] = '/* End of inserted user defined script */';
		}

		$filedata = implode( "\r\n", $filelines );

		return ($this->_write( $fname, $filedata ) );
	}

	function _filterVars( $vars )
	{
		if (preg_match_all( "/([^, :]*):\s*'([^']*)'/", $vars, $arr ))
		{
			$c = "";
			$str = "";
			foreach ($arr[0] as $v)
			{
				$str .= $c.$v;
				$c = ", ";
			}
		}

		return $str;
	}

	/**
	 * Delete the specified file if it exists
	 *
	 * @param mixed $fname		full path name of file to be deleted
	 * @return 					none, errors will be thrown
	 */
	function _delete( $fname )
	{
		jimport('joomla.filesystem.file');
		$result = false;

		if (JFile::exists( $fname))
		{
			$result = JFile::delete( $fname );
		}
		return $result;
	}

	/**
	 * Create the specified file.
	 *
	 * @param mixed $fname		full path name of file to be written
	 * @param mixed $data		data to be written to the file
	 * @return 					none, errors will be thrown
	 */
	function _write( &$fname, &$data )
	{
		jimport('joomla.filesystem.file');

		return JFile::write( $fname, $data );
	}
}
?>