/*
 * gallery preset: caption-overlay-small-thumbstrip
*/
hs.outlineType = 'round-red-stripe';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.fadeInOut = true;
hs.wrapperClassName = 'sspreset small-black';
hs.captionEval = 'this.a.title';
hs.numberPosition = 'caption';
hs.captionOverlay.position = 'bottom left';
hs.captionOverlay.width = '100%';
hs.captionOverlay.hideOnMouseOut = true;
hs.captionOverlay.opacity = .7;
hs.dimmingOpacity = .8;

hs.addSlideshow( {
	repeat: false
	,useControls: true
	,fixedControls: true
	,overlayOptions: {
		position: 'top center'
   		,hideOnMouseOut: true
		,offsetY: -20
   		,opacity: .7
	}
	,thumbstrip: {
		className: 'sspreset-thumbstrip'
		,position: 'below'
    	,mode: 'horizontal'
		,relativeTo: 'expander'
		,hideOnMouseOut: false
	}