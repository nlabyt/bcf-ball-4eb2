<?php
/**
 * Highslide JS Configuration database install
 *
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

class HsInstallHelper
{
	function getParams( $element )
	{
		if (!is_a($element, 'JSimpleXMLElement') || !count($element->children())) {
			// Either the tag does not exist or has no children therefore we return zero files processed.
			return null;
		}

		// Get the array of parameter nodes to process
		$params = $element->children();
		if (count($params) == 0) {
			// No params to process
			return null;
		}

		// Process each parameter in the $params array.
		$ini = null;
		foreach ($params as $param) {
			if (!$name = $param->attributes('name')) {
				continue;
			}

			if (!$value = $param->attributes('default')) {
				continue;
			}

			$ini .= $name."=".$value."\n";
		}
		return $ini;
	}
}

$app	= JFactory::getApplication();
$db = JFactory::getDBO();
$lang = JFactory::getLanguage();
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

$lang->load( 'com_hsconfig', JPATH_ADMINISTRATOR.'/', null, false, false);

$query = "CREATE TABLE IF NOT EXISTS `#__hsconfig` (
	`id` int(11) NOT NULL,
	`css` text NOT NULL default '',
	`script` text NOT NULL default '',
	`overlayhtml` text NOT NULL default '',
	`skincontrols` text NOT NULL default '',
	`skincontent` text NOT NULL default '',
	`params` text NOT NULL,
	`published` bool NOT NULL default 0,
	`modified` datetime NOT NULL default '0000-00-00 00:00:00',
	`publish_tmst` datetime NOT NULL default '0000-00-00 00:00:00',
		PRIMARY KEY  (`id`)
		) ENGINE=MyISAM CHARACTER SET `utf8`";
$db->setQuery( $query );

if( !$db->query() )
{
 	$app->enqueueMessage( JText::_('COM_HSCONFIG_MSG_UNABLE_TO_CREATE_TABLE'), 'alert' );
 	$app->enqueueMessage( $db->getErrorMsg(), 'alert' );
}
else
{
	$fields = $db->getTableFields( "#__hsconfig", false );
	if (!isset($fields['#__hsconfig']['skincontrols']))
	{
		$query = "ALTER TABLE `#__hsconfig` ADD COLUMN `skincontrols` text NOT NULL default '' AFTER `overlayhtml`";
		$db->setQuery( $query );
		$db->query();
	}
	if (!isset($fields['#__hsconfig']['skincontent']))
	{
		$query = "ALTER TABLE `#__hsconfig` ADD COLUMN `skincontent` text NOT NULL default '' AFTER `skincontrols`";
		$db->setQuery( $query );
		$db->query();
	}
	if (!isset($fields['#__hsconfig']['script'])) {
		$query = "ALTER TABLE `#__hsconfig` ADD COLUMN `script` text NOT NULL default '' AFTER `css`";
		$db->setQuery( $query );
		$db->query();
	}
	$query = "SELECT id FROM `#__hsconfig` WHERE id = -1";
	$db->setQuery( $query );
	$id = $db->loadResult();
	if ($id === null)
	{
		$datenow = JFactory::getDate();
		$mySqlDate = $datenow->toFormat();
		$filename = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hsconfig'.DS.'css'.DS.'default.css';
		if (JFile::exists($filename))
		{
			$css = JFile::read($filename);
		}
		$installer = JInstaller::getInstance();
		$manifest = $installer->getManifest();
		$root =& $manifest->document;
		$elements = $root->children();
		$params = "";
		foreach ($elements as $element )
		{
			if ($element->_name == 'params')
			{
				$params .= HsInstallHelper::getParams( $element );
			}
		}
//		$params = $installer->getParams();
		$query = "INSERT INTO `#__hsconfig` ( `id`, `css`, `script`, `overlayhtml`, `skincontrols`, `skincontent`, `params`, `published`, `modified`, `publish_tmst`) VALUES
			(-1, '". $db->getEscaped($css) ."', '', '', '', '', '". $db->getEscaped($params) ."', 0, '" . $db->getEscaped($mySqlDate) . "', '0000-00-00 00:00:00')";
		$db->setQuery( $query );
		if (!$db->query())
		{
			$app->enqueueMessage( JText::_('COM_HSCONFIG_MSG_UNABLE_TO_UPDATE_TABLE'), 'alert' );
			$app->enqueueMessage( $db->getErrorMsg(), 'alert' );
		}
		else
		{
			$app->enqueueMessage( JText::_('COM_HSCONFIG_MSG_DEFAULT_SITE_CONFIGURATION_ADDED'));
		}
	}
	//
	//	move config files if necessary
	//
	$hs_root = JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'highslide'.DS;
	$oldconfigfolder = JPATH_ROOT.DS.'hsconfig';
	$newconfigfolder = $hs_root.'config';
	$oldconfigfile = $oldconfigfolder.DS.'js'.DS.'highslide-sitesettings.js';
	$newconfigfile = $newconfigfolder.DS.'js'.DS.'highslide-sitesettings.js';
	if (JFile::exists($oldconfigfile) && !JFile::exists($newconfigfile))
	{
			JFolder::copy( $oldconfigfolder.DS.'js', $newconfigfolder.DS.'js', '', true );
			JFolder::copy( $oldconfigfolder.DS.'css', $newconfigfolder.DS.'css', '', true );
			JFolder::delete( $oldconfigfolder );
	}
}
?>