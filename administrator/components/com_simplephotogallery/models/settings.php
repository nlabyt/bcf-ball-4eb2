<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: settings.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Settings model for album and image settings.
 */
/* No direct acesss */
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );

class SimplephotogalleryModelsettings extends JModel 
{
	//Function to get settings details from the database.
 	function getsettings()
    {
        global $option, $mainframe;
        $db =& JFactory::getDBO();

        $query = "SELECT count(*) FROM #__simplephotogallery_settings";
        $db->setQuery( $query);
        $total = $db->loadResult();

        $query = "SELECT * FROM #__simplephotogallery_settings  ";
        $db->setQuery( $query );
        $rows = $db->loadObjectList();

        if ($db->getErrorNum()) 
        {
            echo $db->stderr();
            return false;
        }
        $settings = array('option'=>$option,'row'=>$rows);
        return $settings;
    }
    
    //Function to save the settings details to the database.
	function savesettings($settings)
    {
       $settingsTableRow =& $this->getTable('settings');
       if (!$settingsTableRow->bind($settings)) 
       {
            JError::raiseError(500, 'Error binding data');
        }
        if (!$settingsTableRow->check()) 
        {
            JError::raiseError(500, 'Invalid data');
        }
        if (!$settingsTableRow->store()) 
        {
            $errorMessage = $settingsTableRow->getError();
            JError::raiseError(500, 'Error binding data: '.$errorMessage);
        }
    }
    
}