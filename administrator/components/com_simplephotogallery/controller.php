<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: controller.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    This file contains our base controller.
 */

// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
/* Submenu view */
$view = JRequest::getVar( 'view', '', '', 'string', JREQUEST_ALLOWRAW );

if ($view == '' || $view == 'cpanel') {
    if ($view == '') $view = 'cpanel';
	
    JSubMenuHelper::addEntry(JText::_('Cpanel'), JRoute::_('index.php?option=com_simplephotogallery'),true);
    JSubMenuHelper::addEntry(JText::_('Album'), JRoute::_('index.php?option=com_simplephotogallery&view=album'));
    JSubMenuHelper::addEntry(JText::_('Images'), JRoute::_('index.php?option=com_simplephotogallery&view=images'));
    JSubMenuHelper::addEntry(JText::_('Settings'), JRoute::_('index.php?option=com_simplephotogallery&view=settings'));
}

 else if($view == 'album')
{   
	JSubMenuHelper::addEntry(JText::_('Cpanel'), JRoute::_('index.php?option=com_simplephotogallery'));
	JSubMenuHelper::addEntry(JText::_('Albums'), JRoute::_('index.php?option=com_simplephotogallery&view=album'),true);
	JSubMenuHelper::addEntry(JText::_('Images'), JRoute::_('index.php?option=com_simplephotogallery&view=images'));
   	JSubMenuHelper::addEntry(JText::_('Settings'), JRoute::_('index.php?option=com_simplephotogallery&view=settings'));

}

else if($view == 'images')
{   
	JSubMenuHelper::addEntry(JText::_('Cpanel'), JRoute::_('index.php?option=com_simplephotogallery'));
	JSubMenuHelper::addEntry(JText::_('Albums'), JRoute::_('index.php?option=com_simplephotogallery&view=album'));
   	JSubMenuHelper::addEntry(JText::_('Images'), JRoute::_('index.php?option=com_simplephotogallery&view=images'),true);
	JSubMenuHelper::addEntry(JText::_('Settings'), JRoute::_('index.php?option=com_simplephotogallery&view=settings'));
}
else if($view == 'settings')
{   
	JSubMenuHelper::addEntry(JText::_('Cpanel'), JRoute::_('index.php?option=com_simplephotogallery'));
	JSubMenuHelper::addEntry(JText::_('Albums'), JRoute::_('index.php?option=com_simplephotogallery&view=album'));
   	JSubMenuHelper::addEntry(JText::_('Images'), JRoute::_('index.php?option=com_simplephotogallery&view=images'));
	JSubMenuHelper::addEntry(JText::_('Settings'), JRoute::_('index.php?option=com_simplephotogallery&view=settings'),true);
}
class simplephotogalleryController extends JController
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
        parent::display();
    }
    

}
