<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Settings view to retrieves the data to be displayed and pushes it into the template.
 */

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */

class SimplephotogalleryViewSettings extends JView
{
    function display($tpl = null)
    {
    	if(JRequest::getVar('task') == 'edit' )
        {
    		JToolBarHelper::save();
            JToolBarHelper::apply();
            JToolBarHelper::cancel();
        
        	$model = $this->getModel();
            $settings = $model->getsettings();
            $this->assignRef('settings', $settings);
            
    	parent::display();
        }
        else if(JRequest::getVar('task') == '' )
        {
        	JToolBarHelper::custom("regenrate", "move.png","",JText::_('Re-Generate Image'),false);
    	JToolBarHelper::editListX();
        
        $model = $this->getModel();
        $settings = $model->getsettings();
        $this->assignRef('settings', $settings);
            
    	parent::display();
        }
        
        JToolBarHelper::title(JText::_('Simple Photo Gallery').': [ <small> '.JText::_('Settings').' </small> ]');
    }
}