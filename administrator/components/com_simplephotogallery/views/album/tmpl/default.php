<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: default.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Default page to list and create new albums.
 */
?>
<script language="JavaScript" type="text/javascript">
	function submitbutton(pressbutton)
	{
        var form=document.adminForm;
        if(pressbutton=="save" || pressbutton=="apply")
        {
            if (form.album_name.value == '')
            {
                alert("<?php  echo JText::_('Please enter album name') ?>");
                document.getElementById('album_name').focus();
                return false;
            }
            if (form.album_name.value != "")
            {
            	if(!isAliasName(form.album_name.value)){
                	alert("<?php  echo JText::_('Special characters are not allowed in album name') ?>");
                    document.getElementById('album_name').focus();
                    return false;
            }
            }
            if (form.alias_name.value != "")
            {
            
            if(!isAliasName(form.alias_name.value)){
            	alert("<?php  echo JText::_('Special characters are not allowed in alias name') ?>");
                document.getElementById('alias_name').focus();
                return false;
        }
            }
         }
        submitform( pressbutton );
        return;
    }

	
    

	Joomla.submitbutton = function(pressbutton)
	{
        var form=document.adminForm;
        if(pressbutton=="save" || pressbutton=="apply")
        {
            if (form.album_name.value == '')
            {
                alert("<?php  echo JText::_('Please enter album name') ?>");
                document.getElementById('album_name').focus();
                return false;
            }
            if (form.album_name.value != "")
            {
            	if(!isAliasName(form.album_name.value)){
                	alert("<?php  echo JText::_('Special characters are not allowed in album name') ?>");
                    document.getElementById('album_name').focus();
                    return false;
            }
            }
            if (form.alias_name.value != "")
            {
            
            if(!isAliasName(form.alias_name.value)){
            	alert("<?php  echo JText::_('Special characters are not allowed in alias name') ?>");
                document.getElementById('alias_name').focus();
                return false;
        }
            }
        }
        submitform( pressbutton );
        return;
    }

	function isAliasName(aliasName) {
	 	var check = /^([a-zA-Z0-9\-\_\ ]+)$/;
	 	return check.test(aliasName);
	}

</script>
<style type="text/css">
    .button2-left{
        margin-top: 10px !important;
        margin-left: 0px !important;
     }
</style>
<?php
if(JRequest::getVar('task') == 'edit' || JRequest::getVar('task') == '')
{
    $rows = $this->album['row'];
    $lists = $this->album['lists'];
    
}
if(JRequest::getVar('task') == 'edit' || JRequest::getVar('task') == 'add')
{
    $taskName = JRequest::getVar('task');
?>
    <!-- Display this form when task is edit or add -->

    <form action="<?php echo JRoute::_('index.php?option=com_simplephotogallery&view=album'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" >
        <fieldset class="adminform" style="background-color: white">
            <legend><?php  echo JText::_('Albums') ?></legend>
            <input type="hidden" value="<?php echo $taskName?>" id="hdntask"/>
            <table class="admintable"  style="width: 65%;" cellspacing="2" >
                 <tr>
                    <td width="150" class="key" >
                       <span class="editlinktip hasTip" title="<?php  echo JText::_('Album Name') ?>::<?php  echo JText::_('Album Name') ?>"><?php  echo JText::_('Album Name') ?><font color="red">*</font></span>
                    </td>
                    <td>
                        <input  type="text" style="width:380px" name="album_name" id="album_name" value="<?php if($taskName=='edit'){echo $rows->album_name;}?>">
                    </td>
                </tr>
                <tr>
                    <td width="150" class="key" >
                       <span class="editlinktip hasTip" title="<?php  echo JText::_('Alias Name') ?>::<?php  echo JText::_('Alias Name') ?>"><?php  echo JText::_('Alias Name') ?></span>
                    </td>
                    <td>
                        <input  type="text" style="width:380px" name="alias_name" id="alias_name" value="<?php if($taskName=='edit'){echo $rows->alias_name;}?>">
                    </td>
                </tr>
                <tr>
                    <td width="150" class="key" >
                        <span class="editlinktip hasTip" title="<?php  echo JText::_('Album Description') ?>::<?php  echo JText::_('Album Description') ?>"><?php  echo JText::_('Album Description') ?></span>
                    </td>
                    <td>
                    
                    <?php $editor = & JFactory::getEditor(); 
	                    
	                    $imageDesc = "";
						if (isset($rows->description))
                                $imageDesc = $rows->description;
	                    
	                    echo $editor->display('description', $imageDesc, '350', '200', '60', '20', false);
	                    ?>	  
                    
                    </td>
                </tr>
                <tr>
                    <td width="100" class="key" >
                       <span class="editlinktip hasTip" title="<?php  echo JText::_('published') ?>::<?php  echo JText::_('published') ?>"> <?php  echo JText::_('published') ?></span>
                    </td>
                    <td >
                        <input type="radio" style="float:left" name="published"
                            <?php if($taskName=='edit'){
                                        if($this->album['row']->published==1){
                                            echo 'checked="checked" ';
                                        }
                                   } else {
                                    echo "checked='checked'";
                                    }?> value="1"   />



                        <span style="float:left;margin-top: 4px;"><?php  echo JText::_('Yes') ?>&nbsp;&nbsp;</span>
                        


                       <input type="radio" style="float:left" name="published"
                            <?php if($taskName=='edit'){
                                    if($this->album['row']->published==0){
                                        echo 'checked="checked" ';
                                    }
                                }?>value="0" />
                                
                                <span style="float:left;margin-top: 4px;"><?php  echo JText::_('No') ?></span>
                    </td>
                </tr>
            </table>
        </fieldset>
        
        <input type="hidden" name="id" value="<?php if($taskName=='edit'){echo $rows->id;} ?>" />
        <input type="hidden" name="option" value="com_simplephotogallery" />
        <input type="hidden" name="controller" value="album" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="1" />
    </form>
<?php
} else { 
	$pagesna = $this->album;
	$mainframe = JFactory::getApplication();
	if(!isset($option))
            $option = '';
        $search = $mainframe->getUserStateFromRequest( $option.'search','search','','string' );
?>
<form action="<?php echo JRoute::_('index.php?option=com_simplephotogallery&view=album'); ?>" method="post" name="adminForm" id="adminForm" >
            <table>
                <tr>
                    <td align="left" width="100%">
                        <?php echo JText::_( 'Search' ); ?>:
                        <input type="text" name="search" id="search" value="<?php if (isset($search)) echo $search;?>" class="text_area"  onchange="document.adminForm.submit();" />
                        <button onClick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
                        <button onClick="document.getElementById('search').value='';"><?php  echo JText::_( 'Reset' ); ?></button>
                    </td>
                </tr>
            </table>
            <table class="adminlist">
                <thead>
                    <tr>
                        
                        <?php if(JRequest::getCmd("tmpl")==""){ ?><th width="60">
                            <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
                        </th><?php } ?>
                        <th width="70"><?php echo JHTML::_('grid.sort',  JText::_( 'Id') , 'id', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                        <th  style="color:#3366CC">
                            <?php
                            echo JHTML::_('grid.sort',  'Album Name', 'album_name', @$lists['order_Dir'], @$lists['order'] );
                            ?>
                        </th>
                        <th width="5%" nowrap="nowrap" style="color:#3366CC"><?php echo JHTML::_('grid.sort',  JText::_( 'Published' ), 'published', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                    </tr>
                </thead>
                <?php
                    jimport('joomla.filter.output');
                    $j = 0;
                    for ($i = 0, $n = count( $rows ); $i < $n; $i++) {
                        $row = &$rows[$i];
                        $checked = JHTML::_('grid.id', $i, $row->id );
                        $published = JHTML::_('grid.published', $row, $i );
                        $link = JRoute::_('index.php?option=com_simplephotogallery&view=album&task=edit&cid[]='. $row->id );?>
                        <tr>
                        	<?php if(JRequest::getCmd("tmpl")==""){ ?><td align="center"><?php echo $checked; ?></td><?php }?>
                            <td align="center"><?php echo $row->id; ?></td>
	                            <?php if(JRequest::getCmd("tmpl")){ ?>
	                            	<td align="center"><a class="pointer" onclick="if (window.parent) window.parent.jSelectArticle('<?php echo $row->id;?>', '<?php echo $row->album_name;?>');"  ><?php echo $row->album_name;?></a></td>
	                            <?php } else{ ?>
									<td align="center"><a href="<?php echo $link; ?>"><?php echo $row->album_name;?></a></td>
								<?php } ?>
                            <td align="center"><?php echo $published;?></td>
                        </tr>
                            <?php
                        $j++;
                    }?>
                <tr><td colspan="6"><?php if(count($rows))  echo $pagesna['pageNav']->getListFooter(); ?></td></tr>
            </table>
            
            <input type="hidden" name="option" value="com_simplephotogallery" />
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="boxchecked" value="0" />
            <input type="hidden" name="controller" value="album" />
            <input type="hidden" name="filter_order" value="<?php echo @$lists['order']; ?>" />
            <input type="hidden" name="filter_order_Dir" value="<?php echo @$lists['order_Dir']; ?>" />
        </form>
        <?php }?>