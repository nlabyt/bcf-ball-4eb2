<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Album view to retrieves the data to be displayed and pushes it into the template.
 */

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */

class SimplephotogalleryViewAlbum extends JView
{
    function display($tpl = null)
    {
        	$this->toolbar();
        	$model = &$this->getModel("album");
            $album = $model->getAlbum();
            
            if(JRequest::getVar('task') == 'edit')
            {
	            $id=JRequest::getVar('cid');
	            $album = $model->getAlbums($id[0]);
	            $this->assignRef('album', $album);
            
            }
            else 
            {
            $this->assignRef('album', $album);
            }
            parent::display($tpl);
    }
    
    function toolbar()
    {
    	if(JRequest::getVar('task') == 'add' || JRequest::getVar('task') == 'edit' ){
    		JToolBarHelper::save();
            JToolBarHelper::cancel();
    	}
    	else
    	{
    	 	
            JToolBarHelper::publishList();
            JToolBarHelper::unpublishList();
            JToolBarHelper::deleteList();
            JToolBarHelper::editListX();
            JToolBarHelper::addNewX();
    	}
    	JToolBarHelper::title(JText::_('Simple Photo Gallery').': [ <small> '.JText::_('Album').' </small> ]');
    }

}