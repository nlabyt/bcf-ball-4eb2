<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Images view to retrieves the data to be displayed and pushes it into the template.
 */

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */

class SimplephotogalleryViewImages extends JView
{
    function display($tpl = null)
    {
    if(JRequest::getVar('task') == 'edit' )
        {
            
            JToolBarHelper::save("saveclose");
            JToolBarHelper::apply();
            JToolBarHelper::cancel();
            $model = $this->getModel();
            $id=JRequest::getVar('cid');
            $images = $model->getimages($id[0]);
            $this->assignRef('images', $images);
            parent::display();
        }
        else if(JRequest::getVar('task') == 'add' )
        {
            
            JToolBarHelper::save();
            JToolBarHelper::save("savenew","Save &amp; new");
            JToolBarHelper::cancel();
            
            $model = &$this->getModel();
            //$images = $model->getNewimages();
            //$this->assignRef('images', $images);
            $settings = $model->settings();
            $images = $model->getNewimages();
            $this->assignRef('images', $images);
            $this->assignRef('settings', $settings);
            
            parent::display();
        }
        else 
        {
        	JToolBarHelper::publishList('featuredpublish','Featured');
        	JToolBarHelper::unpublishList('featuredunpublish','Unfeatured');
    		JToolBarHelper::publishList();
            JToolBarHelper::unpublishList();
            JToolBarHelper::deleteList();
            JToolBarHelper::editListX();
            JToolBarHelper::addNewX();
    	$model = $this->getModel();
        if(JRequest::getVar('set')!= '')
            {
            	
                $set = $model->setImage();
                $this->assignRef('set', $set);
            }
    	$images = $model->getimage();
        $this->assignRef('images', $images);
        parent::display();
        }
        
        JToolBarHelper::title(JText::_('Simple Photo Gallery').': [ <small> '.JText::_('Images').' </small> ]');
    }
    
	function published( $row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix='download')
	{

        $app = JFactory::getApplication();
        $templateName = $app->getTemplate();
		if(version_compare(JVERSION,'1.6.0','ge')) 
        {
        	$route = JURI::base().'templates/'.$templateName.'/images/admin/';
        }
        else
        {
        	$route = JURI::base().'images/';
        }
        $img    = $row ? $imgY : $imgX;
        $task   = $row ? 'unpublish' : 'publish';
        $alt    = $row ? JText::_( 'Published' ) : JText::_( 'Unpublished' );
        $action = $row ? JText::_( 'Disable product download' ) : JText::_( 'Enable product download' );

        $href = '
        <a href="#" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
        <img src="'.$route.$img.'" border="0" alt="'. $alt .'" /></a>'
        ;

        return $href;
	}
}