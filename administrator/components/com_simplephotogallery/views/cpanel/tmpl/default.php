<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: default.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Default page to display icons for quick view of photogallery menu's.
 */
/* No direct acesss */

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.html.pane' );
?>
<?php 
	function quickiconButton( $link, $image, $text )
	  	{
	           	global $mainframe;
	          	$lang        =& JFactory::getLanguage();
	         	?>
	         	<div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
	            <div class="icon">
	            <a href="<?php echo $link; ?>">
	            <?php echo JHTML::_('image.site',  $image, 'components/com_simplephotogallery/images/', NULL, NULL, $text ); ?>
	            <span><?php echo $text; ?></span></a>
	            </div>
	          	</div>
	  	<?php
		}
      	?>
<div class="adminform">
<div class="cpanel-left">
        
            <div id="cpanel">
                <?php
                $link = 'index.php?option=com_simplephotogallery&view=album';
                quickIconButton( $link, 'albums.png', JText::_( 'Albums' ) );
                
                $link = 'index.php?option=com_simplephotogallery&view=images';
                quickIconButton( $link, 'images.png', JText::_( 'Images' ) );

                $link = 'index.php?option=com_simplephotogallery&view=settings';
                quickIconButton( $link, 'settings-icon.png', JText::_( 'Settings' ) );
                ?>

                <div style="clear:both">&nbsp;</div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
	    </div>
        </div>

	<input type="hidden" name="option" value="com_simplephotogallery" />
	<input type="hidden" name="view" value="cpanel" />
</div>
