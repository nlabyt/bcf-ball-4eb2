<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Cpanel view to retrieves the data to be displayed and pushes it into the template.
 */
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */

class SimplephotogalleryViewcpanel extends JView
{
	
    function display()
    {
    	JToolBarHelper::title(JText::_('Simple Photo Gallery').': [ <small> '.JText::_('Cpanel').' </small> ]');
        parent::display();
    }
}