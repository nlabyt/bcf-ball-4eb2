<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: install.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Custom install script for simplephotogallery component.
 */
// no direct access
defined('_JEXEC') or die('Restricted Access');
error_reporting(0);
// import joomla's filesystem classes
jimport('joomla.filesystem.folder');
jimport('joomla.installer.installer');
// delete a folder inside your images folder
//JFolder::delete(JPATH_ROOT.DS.'images'.DS.'qrcode');
   
$installer = new JInstaller();

		$query = 'UPDATE  #__components '.
                 'SET name = "Images" '.
                 'WHERE name = "COM_PHOTOGALLERY_IMAGES"';
	        $db->setQuery($query);
	        $db->query();
	        
	        $query = 'UPDATE  #__components '.
	                 'SET name = "Settings" '.
	                 'WHERE name = "COM_PHOTOGALLERY_SETTINGS"';
	        $db->setQuery($query);
	        $db->query();
	        
	        $query = 'UPDATE  #__components '.
	                 'SET name = "Simple Photo Gallery" '.
	                 'WHERE name = "COM_PHOTOGALLERY"';
	        $db->setQuery($query);
	        $db->query();
	        
	         $query = 'UPDATE  #__components '.
	                 'SET name = "Albums" '.
	                 'WHERE name = "COM_PHOTOGALLERY_ALBUMS"';
	        $db->setQuery($query);
	        $db->query();
        
?>