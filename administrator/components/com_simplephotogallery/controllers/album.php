<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: album.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Album controller for listing and create new albums.
 */
/* no direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

/* Album Administrator Controller  */
class SimplephotogalleryControlleralbum extends JController
{   
	//defult function for the View.
    function display()
    {
    	parent::display();
    }
    
    // This function to save the new album and redirect to list album page.
	function save()
    {
        $album = JRequest::get('POST');
        //print_r($total);
        //die;
        $model = $this->getModel('album');
        $model->saveAlbum($album);
        $this->setRedirect('index.php?view=album&option='.JRequest::getVar('option'), 'Saved!');
    }
    
    //function to Publish the item from the listing.
	function publish()
    {
        $album = JRequest::get('POST');
        $model =& $this->getModel('album');
        $model->publish($album);
        $this->setRedirect('index.php?view=album&option='.JRequest::getVar('option'));
    }

    // function to Unpublish the item from the listing.
    function unpublish()
    {
        $album = JRequest::get('POST');
        $model =& $this->getModel('album');
        $model->publish($album);
        $this->setRedirect('index.php?view=album&option='.JRequest::getVar('option'));
    }
    
    // function to cancel the current operation.
 	function cancel()
    {
        $this->setRedirect('index.php?view=album&option='.JRequest::getVar('option'), 'Cancelled...');
    }
    
    // function is to delete the album's.
 	function remove()
    {
        //Reads cid as an array
        $arrayIDs = JRequest::getVar('cid', null, 'default', 'array' );
        if($arrayIDs === null)
        { 
           //Make sure the cid parameter was in the request
           JError::raiseError(500, 'cid parameter missing from the request');
        }
        $model =& $this->getModel('album');
        $model->deleteAlbum($arrayIDs);
        $this->setRedirect('index.php?view=album&option='.JRequest::getVar('option'), 'Deleted...');

    }
    
}