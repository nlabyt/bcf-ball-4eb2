<?php 
/**
 * @name        Simple Photo Gallery
 * @version	1.0: cpanel.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Cpanel controller for displaying quick view incons.
 */
/* no direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class SimplephotogalleryControllercpanel extends JController
{   
    function display()
    {
    	if(JRequest::getVar('view') == '')
    	{
    		JRequest::setVar('view','cpanel');
    	}
    	
    	parent::display();
    }
}