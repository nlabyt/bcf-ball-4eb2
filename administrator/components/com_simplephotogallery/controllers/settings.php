<?php 
/**
 * @name        Simple Photo Gallery
 * @version	1.0: settings.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Settings controller for image's and album's settings. 
 */
/* no direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

/* Settings Administrator Controller  */
class SimplephotogalleryControllersettings extends JController
{   
	//defult function for the View.
    function display()
    {
    	parent::display();
    }
    
    // This function to save image and album settings and redirect to settings display page.
    function save()
    {
        $detail = JRequest::get( 'POST' );
        $model =& $this->getModel('settings');
        $model->savesettings($detail);
        $this->setRedirect('index.php?view=settings&option='.JRequest::getVar('option'), JText::_('Settings saved.').$msg);
    }
    
    // This function to save image and album settings and remains in edit setting page.
	function apply()
    {
        $detail = JRequest::get( 'POST' );
        $model =& $this->getModel('settings');
        $model->savesettings($detail);
        
        $link = 'index.php?option=com_simplephotogallery&view=settings&task=edit&cid[]='.$settings['id'];
        $this->setRedirect($link, JText::_('Settings saved..').$msg);
    }
    
    // function to cancel the current operation.
 	function cancel()
    {
        $this->setRedirect('index.php?view=settings&option='.JRequest::getVar('option'), JText::_('Cancelled..'));
    }
    
    //Function to regenerate the image size in folder as per the settings value.
	function regenrate() 
	{
	        // open the directory
	        $pathToImages = JPATH_ROOT.DS."images".DS."photogallery".DS;
	        
	        ini_set("memory_limit", "1000M");
	        $array = array("full_image","featured_image","album_image","thumb_image");
	        
	        $model = $this->getModel('settings');
    		$result = $model->getsettings();
    		$row = $result["row"][0];
	        
	        for($f=0;$f<count($array);$f++)
	        {
	        
	        	$pathToThumbs = JPATH_ROOT.DS."images".DS."photogallery".DS . $array[$f] . DS;
	        	$dir = opendir($pathToImages);
	        
	        	// loop through it, looking for any/all JPG files:
	        	while (false !== ($fname = readdir($dir))) {
	            // parse path for the extension
	            $info = pathinfo($pathToImages . $fname);            
	            	if($fname !="." && $fname !=".." ):
		            if (strtolower($info['extension']) == 'jpg') 
		            {
		                // load image and get image size
		                $img = imagecreatefromjpeg("{$pathToImages}{$fname}");
		            } else if (strtolower($info['extension']) == 'png') 
		            {
		                // load image and get image size
		                $img = imagecreatefrompng("{$pathToImages}{$fname}");
		            } else if (strtolower($info['extension']) == 'gif') 
		            {
		                // load image and get image size
		                $img = imagecreatefromgif("{$pathToImages}{$fname}");
		            }
	        		else if (strtolower($info['extension']) == 'bmp') 
	        		{
		                // load image and get image size
		                $img = imagecreatefromwbmp("{$pathToImages}{$fname}");
		            }
		                $width = imagesx($img);
		                $height = imagesy($img);
		
		                // calculate thumbnail size
		                //$new_width = $thumbWidth;
		                //$new_height = floor($height * ( $thumbWidth / $width ));
		                
		                if($array[$f] == "full_image")
		                {
							$new_width = $row->fullimg_width;
		                	$new_height = $row->fullimg_height;
		                }
		                else if($array[$f] == "featured_image")
		                {
		                	$new_width = $row->feat_photo_width;
		                	$new_height = $row->feat_photo_height;
		                }
	        			else if($array[$f] == "album_image")
	        			{
		                	$new_width = $row->alb_photo_width;
		                	$new_height = $row->alb_photo_height;
		                }
		                else if($array[$f] == "thumb_image")
		                {
		                	$new_width = $row->thumbimg_width; ;
		                	$new_height = $row->thumbimg_height;;
		                }
		
		                // create a new temporary image
		                $tmp_img = imagecreatetruecolor($new_width, $new_height);
		
		                // copy and resize old image into new image
		                imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		
		                if (strtolower($info['extension']) == 'jpg') 
		                {
			                // save thumbnail into a file
			                imagejpeg($tmp_img, "{$pathToThumbs}{$fname}");
			            } else if (strtolower($info['extension']) == 'png') 
			            {
			                // save thumbnail into a file
			                imagepng($tmp_img, "{$pathToThumbs}{$fname}");
			            } else if (strtolower($info['extension']) == 'gif') 
			            {
			                // save thumbnail into a file
			                imagegif($tmp_img, "{$pathToThumbs}{$fname}");
			            }
	        			else if (strtolower($info['extension']) == 'gif') 
	        			{
			                // save thumbnail into a file
			                image2wbmp($tmp_img, "{$pathToThumbs}{$fname}");
			            }
		            endif;
	            }            
		        // close the directory
		        closedir($dir);
	        }
			$this->setRedirect('index.php?view=settings&option='.JRequest::getVar('option'), JText::_('Images regenerated successfully') );
	    }
}