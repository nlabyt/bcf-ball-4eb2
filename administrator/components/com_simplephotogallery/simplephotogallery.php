<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: simplephotogallery.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    This file is the entry point to our component.
 */
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT.DS.'controller.php' );
 
/* get value of view from url */
$controller = JRequest::getWord('view');

/*if no view set controller to cpanel */
if($controller == '') $controller='cpanel';
if($controller) { 
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) 
    {
    	require_once $path;
    } 
    else 
    {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'SimplephotogalleryController'.$controller;
$controller   = new $classname( );
 
// Perform the Request task
$controller->execute( JRequest::getVar( 'task' ) );
 
// Redirect if set by the controller
$controller->redirect();