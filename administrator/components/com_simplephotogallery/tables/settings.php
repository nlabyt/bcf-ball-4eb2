<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: settings.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Settings table for storing image and album setting details
 */
/* No direct acesss */
defined( '_JEXEC' ) or die( 'restricted access' );
class Tablesettings extends JTable
{
    var $id = null;
    var $feat_cols=null;
    var $feat_rows = null;
    var $feat_photo_width = null;
    var $feat_photo_height = null;
    var $feat_vspace = null;
    var $feat_hspace = null;
    var $alb_photo_width = null;
    var $alb_photo_height = null;
    var $alb_vspace = null;
    var $alb_hspace = null;
    var $general_share_photo = null;
    var $general_download = null;
    var $general_show_alb = null;
    var $facebook_api = null;
    var $thumbimg_width = null;
    var $thumbimg_height = null;
    var $photo_vspace = null;
    var $photo_hspace = null;
    var $fullimg_width = null;
    var $fullimg_height = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function Tablesettings(&$db)
    {
        parent::__construct( '#__simplephotogallery_settings', 'id', $db );
    }
}

?>
