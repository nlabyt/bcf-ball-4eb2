<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: images.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Images table for storing image details
 */
/* No direct acesss */
defined( '_JEXEC' ) or die( 'restricted access' );
class Tableimages extends JTable
{
    var $id = null;
    var $title=null;
    var $image = null;
    var $alias_name = null;
    var $description = null;
    var $album_id = null;
    var $sortorder = null;
    var $is_featured = null;
    var $published = null;
    var $album_cover = null;
    var $created_date = null;
    var $modified_date = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function Tableimages(&$db)
    {
        parent::__construct( '#__simplephotogallery_images', 'id', $db );
    }
}

?>
