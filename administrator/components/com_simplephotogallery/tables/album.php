<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: album.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Album table for storing album details
 */
/* No direct acesss */
defined( '_JEXEC' ) or die( 'restricted access' );
class Tablealbum extends JTable
{
    var $id = null;
    var $album_name=null;
    var $alias_name=null;
    var $description = null;
    var $published = null;
    var $created_date = null;
    var $modified_date = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function Tablealbum(&$db)
    {
        parent::__construct( '#__simplephotogallery_album', 'id', $db );
    }
}

?>
