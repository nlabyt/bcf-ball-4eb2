<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
jimport('joomla.html.pane');

	if(isset($this->message)){
		$this->display('message');
	}

    $rows = $this->rows;
	$page = $this->page;
    $Itemid = JRequest::getInt('Itemid');

JHTML::_('behavior.tooltip');
?>

<?php if ( $this->params->def( 'show_page_title', 1 ) ) : ?>
        <div class="clrthis">
            <div class="componentheading<?php echo $this->params->get( 'pageclass_sfx' ); ?>">
                <?php
                echo $this->curseas->name; ?>
            </div>
            <?php  if($this->curseas->logo && is_file('media/bearleague/'.$this->curseas->logo)){
                 echo "<img src='".JURI::base()."media/bearleague/".$this->curseas->logo."' class='tourn_logo' width='120' style='margin:10px 0 10px 0;'/>";
            }?>
            <div class="js_tourndescr">
                <?php echo $this->curseas->descr; ?>
            </div>
        </div>
<?php endif;

//echo '<div id="d_name">'.$this->md_title."</div>";
   /* if($this->tmpl != 'component'){
        $lnk = "window.open('".JURI::base()."index.php?&tmpl=component&option=com_joomsport&amp;task=tournament','jsmywindow','width=600,height=700');";
    }else{
        $lnk = "window.print();";
    }*/
?>
<!--<div style="float:right">
	<img onClick="<?php //echo $lnk;?>" src="components/com_joomsport/images/printButton.png" border="0" alt="Print" style="cursor:pointer;" title="Print" />
</div>-->

<!--            ||||||||||||||||||||||||||||||||||||||||||||||||||||        -->
<form action="" method="post" name="adminForm" id="adminForm">
    <div align="right"><?php //echo JText::_('BLFA_FILTERS')?> <?php //echo $lists['teams1'];?></div>
    <table class="adminlist">
        <thead>
        <tr>
            <th class="title" style="text-align: left;">
                <?php echo "#" ?>
            </th>
            <th class="title" style="text-align: left;">
                <?php echo JText::_( 'BLFA_SEAS' ); ?>
            </th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="13">
                <?php echo $page->getListFooter(); ?>
            </td>
        </tr>
        </tfoot>
        <tbody>
        <?php
        $k = 0;

  // print_r($this->s_id);
        if( count( $rows ) ) {
            for ($i=0, $n=count( $rows ); $i < $n; $i++) {
               $row 	= $rows[$i];
               $num = $i+1;
               JFilterOutput::objectHtmlSafe($row);
               $link = JRoute::_( 'index.php?option=com_joomsport&view=ltable&sid='.$row->s_id.'&Itemid='.$Itemid);
                //http://localhost/joomla5/index.php?option=com_joomsport&view=ltable&sid=1

               // $link = JRoute::_("index.php?option=com_joomsport&view=ltable?sid=".$row->s_id);
                //$checked 	= @JHTML::_('grid.checkedout',   $row, $i );
                //$published 	= JHTML::_('grid.published', $row, $i);
                ?>
            <tr class="<?php echo "row$k"; ?>">
                <td>
                    <?php
                   echo $num;
                    ?>
                </td>
                <td>
                    <?php
                   echo '<a href="'.$link.'" style="margin-left:10px;">'.$row->s_name.' </a>';
                    ?>
                </td>



            </tr>
                <?php
            }
       }
        ?>
        </tbody>
    </table>
    <input type="hidden" name="task" value="tournament" />
    <input type="hidden" name="boxchecked" value="0" />
    <!--<input type="hidden" name="sid" value="<?php //echo $this->s_id;?>" />-->
    <?php echo JHTML::_( 'form.token' ); ?>
</form>
<!--                        ||||||||||||||||||||||||||||||||||||||||||||||||||||                -->


<?php if($this->tmpl != 'component'){?>
<div align="right" style="padding-bottom:30px;"><div style="width:100px;float:right;text-align:right"><a href="javascript:void(0);" onclick="history.back(-1);"><?php echo $this->bl_lang["BL_BACK"]?></a></div></div>
<?php } ?>