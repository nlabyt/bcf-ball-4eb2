<?php
/*------------------------------------------------------------------------
# JoomSport Professional 
# ------------------------------------------------------------------------
# BearDev development company 
# Copyright (C) 2011 JoomSport.com. All Rights Reserved.
# @license - http://joomsport.com/news/license.html GNU/GPL
# Websites: http://www.JoomSport.com 
# Technical Support:  Forum - http://joomsport.com/helpdesk/
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

class bleagueViewtournament extends JView
{

	function display($tpl = null)
	{

       $mainframe = JFactory::getApplication();
       $params	=& $mainframe->getParams();


        $s_id = JRequest::getVar( 'sid', 0, '', 'int' );//id by tournament
        //$this->s_id = JRequest::getVar( 'tid', 0, '', 'int' );
        //$this->playerlist = JRequest::getVar( 'calendar', 0, '', 'int' );

        $option = 'com_joomsport';
        $limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
        $limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

        $tmpl = JRequest::getVar( 'tmpl', '', '', 'string' );
        $db			=& JFactory::getDBO();

        $query = "SELECT * FROM  #__bl_tournament as t WHERE t.id=".$s_id." AND t.published='1'";
        $db->setQuery($query);
        $curseas = $db->loadObject();
        if(!count($curseas)){
            JError::raiseError( 403, JText::_('Access Forbidden') );
            return;
        }

        $query = "SELECT COUNT(DISTINCT(s.s_id)) FROM  #__bl_seasons as s WHERE s.t_id = ".$s_id."";
        $db->setQuery($query);
        $total = $db->loadResult();

		jimport('joomla.html.pagination');
        $pageNav = new JPagination( $total, $limitstart, $limit );

        $query = "SELECT DISTINCT(s.s_id),s.* FROM  #__bl_seasons as s WHERE s.t_id=".$s_id." AND s.published = 1 ORDER BY s.s_name";
        $db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
        $rows = $db->loadObjectList();
//
        require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_joomsport'.DIRECTORY_SEPARATOR.'bl_lang.php');

        $this->assignRef('bl_lang', $bl_lang);
        $this->assignRef('tmpl', $tmpl);

        $this->assignRef('params',		$params);

        $this->assignRef('rows',		$rows);
        $this->assignRef('curseas',		$curseas);
		$this->assignRef('page', $pageNav);
       // $this->assignRef('s_id', $s_id);
		parent::display($tpl);
		//require_once(dirname(__FILE__).'/tmpl/default'.($tpl?"_".$tpl:"").'.php');
		
		
	}
	
		
}
