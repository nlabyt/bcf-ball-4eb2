<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
jimport('joomla.html.pane');

	if(isset($this->message)){
		$this->display('message');
	}

    $rows = $this->rows;
    $lists = $this->lists;
    $page = $this->page;
    $Itemid = JRequest::getInt('Itemid');

JHTML::_('behavior.tooltip');
?>

<?php if ( $this->params->def( 'show_page_title', 1 ) ) : ?>
        <div class="componentheading<?php echo $this->params->get( 'pageclass_sfx' ); ?>">
            <?php echo $this->escape($this->params->get('page_title')); ?>
        </div>
<?php endif;

//echo '<div id="d_name">'.$this->md_title."</div>";
    if($this->tmpl != 'component'){
        $lnk = "window.open('".JURI::base()."index.php?&tmpl=component&option=com_joomsport&amp;task=teamlist&amp;sid=".$this->s_id."','jsmywindow','width=600,height=700');";
    }else{
        $lnk = "window.print();";
    }
?>
<div style="float:right">
	<img onClick="<?php echo $lnk;?>" src="components/com_joomsport/images/printButton.png" border="0" alt="Print" style="cursor:pointer;" title="Print" />
</div>

<!--            ||||||||||||||||||||||||||||||||||||||||||||||||||||        -->
<form action="" method="post" name="adminForm" id="adminForm">
    
    <table class="adminlist">
        <thead>
        <tr>

            <th class="title" style="text-align: left;">
                <?php echo JText::_( 'BLFA_TEAM' ); ?>
            </th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="13">
                <?php echo $page->getListFooter(); ?>
            </td>
        </tr>
        </tfoot>
        <tbody>
        <?php
        $k = 0;
        if( count( $rows ) ) {
            for ($i=0, $n=count( $rows ); $i < $n; $i++) {
                $row 	= $rows[$i];
                JFilterOutput::objectHtmlSafe($row);
                $link = JRoute::_( 'index.php?option=com_joomsport&task=team&cid[]='. $row->id.'&sid='.$this->s_id.'&Itemid='.$Itemid );
                $checked 	= @JHTML::_('grid.checkedout',   $row, $i );
                //$published 	= JHTML::_('grid.published', $row, $i);
                ?>
            <tr class="<?php echo "row$k"; ?>">

                <td>
                    <?php
                    echo '<a href="'.$link.'" style="margin-left:10px;">'.$row->t_name.' </a>';
                    ?>
                </td>



            </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <input type="hidden" name="task" value="teamlist" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="sid" value="<?php echo $this->s_id;?>" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>
<!--                        ||||||||||||||||||||||||||||||||||||||||||||||||||||                -->


<?php if($this->tmpl != 'component'){?>
<div align="right" style="padding-bottom:30px;"><div style="width:100px;float:right;text-align:right"><a href="javascript:void(0);" onclick="history.back(-1);"><?php echo $this->bl_lang["BL_BACK"]?></a></div></div>
<?php } ?>