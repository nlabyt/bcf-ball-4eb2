<?php
/*------------------------------------------------------------------------
# JoomSport Professional 
# ------------------------------------------------------------------------
# BearDev development company 
# Copyright (C) 2011 JoomSport.com. All Rights Reserved.
# @license - http://joomsport.com/news/license.html GNU/GPL
# Websites: http://www.JoomSport.com 
# Technical Support:  Forum - http://joomsport.com/helpdesk/
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

class bleagueViewplayerlist extends JView
{
	//var $_model = null;

	function display($tpl = null)
	{

        $mainframe = JFactory::getApplication();
        $pathway  =& $mainframe->getPathway();
        $document =& JFactory::getDocument();
        $params	= &$mainframe->getParams();
        // Page Title
        $menus	= &JSite::getMenu();
        $menu	= $menus->getActive();
        $option = 'com_joomsport';
        $tmpl = JRequest::getVar( 'tmpl', '', '', 'string' );;
        $s_id		= $mainframe->getUserStateFromRequest( $option.'.sid', 'sid', 0, 'int' );
        $limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
        $limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
        $f_team		= $mainframe->getUserStateFromRequest( $option.'.filter_team', 'f_team', 0, 'int' );
        $f_pos		= $mainframe->getUserStateFromRequest( $option.'.filter_pos', 'f_pos', 0, 'int' );
        $db			=& JFactory::getDBO();
        $query = "SELECT COUNT(DISTINCT(p.id)) FROM #__bl_players as p LEFT JOIN #__bl_positions as bp ON bp.p_id=p.position_id, #__bl_teams as t, #__bl_seasons as s, #__bl_season_teams as st, #__bl_tournament as tr WHERE s.s_id=st.season_id AND st.team_id = t.id AND s.t_id = tr.id AND s.s_id=".$s_id." AND p.team_id = t.id ".($f_team?" AND t.id =".$f_team:"").($f_pos?" AND bp.p_id =".$f_pos:"")." ";
        $db->setQuery($query);
        $total = $db->loadResult();

        jimport('joomla.html.pagination');
        $pageNav = new JPagination( $total, $limitstart, $limit );

        $query = "SELECT DISTINCT(p.id),p.*,t.t_name,bp.p_name FROM #__bl_players as p LEFT JOIN #__bl_positions as bp ON bp.p_id=p.position_id, #__bl_teams as t, #__bl_seasons as s, #__bl_season_teams as st, #__bl_tournament as tr WHERE s.s_id=st.season_id AND st.team_id = t.id AND s.t_id = tr.id AND s.s_id=".$s_id." AND p.team_id = t.id ".($f_team?" AND t.id =".$f_team:"").($f_pos?" AND bp.p_id =".$f_pos:"")."  ORDER BY p.first_name,p.last_name";
        $db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
        $rows = $db->loadObjectList();

        $javascript = 'onchange = "document.adminForm.submit();"';


        $is_team = array();
	//$s_id
        $query = "SELECT t.id as id,t.t_name FROM #__bl_teams as t, #__bl_season_teams as st WHERE st.season_id = ".$s_id." AND st.team_id = t.id ORDER BY t.t_name";
        $db->setQuery($query);
        $team = $db->loadObjectList();
        $is_team[] = JHTML::_('select.option',  0, JText::_('BLFA_SELTEAM'), 'id', 't_name' );
        $teamis = array_merge($is_team,$team);
        $lists['teams1'] = JHTML::_('select.genericlist',   $teamis, 'f_team', 'class="inputbox" size="1"'.$javascript, 'id', 't_name', $f_team);
//
        $query = "SELECT CONCAT(t.name,' ',s.s_name) FROM #__bl_seasons as s, #__bl_tournament as t WHERE t.id = s.t_id AND s.s_id = ".$s_id;
        $db->setQuery($query);
        $lists['p_title'] = $db->loadResult();

        if (is_object( $menu )) {
            $menu_params = new JRegistry;// new JParameter( $menu->params );
            if (!$menu_params->get( 'page_title')) {
                $params->set('page_title',	JText::_( $lists['p_title'] ));
            }
        } else {
            $params->set('page_title',	JText::_( $lists['p_title'] ));
        }
        $document->setTitle( $params->get( 'page_title' ) );
        $pathway->addItem( JText::_( $lists['p_title'] ));

        require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_joomsport'.DIRECTORY_SEPARATOR.'bl_lang.php');

        $this->assignRef('bl_lang', $bl_lang);
        $this->assignRef('params',		$params);

        $this->assignRef('rows',		$rows);

        $this->assignRef('lists',		$lists);
        $this->assignRef('page', $pageNav);
        $this->assignRef('s_id', $s_id);
        $this->assignRef('tmpl', $tmpl);
		parent::display($tpl);
		//require_once(dirname(__FILE__).'/tmpl/default'.($tpl?"_".$tpl:"").'.php');
		
		
	}
	
		
}
