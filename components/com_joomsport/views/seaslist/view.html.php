<?php
/*------------------------------------------------------------------------
# JoomSport Professional 
# ------------------------------------------------------------------------
# BearDev development company 
# Copyright (C) 2011 JoomSport.com. All Rights Reserved.
# @license - http://joomsport.com/news/license.html GNU/GPL
# Websites: http://www.JoomSport.com 
# Technical Support:  Forum - http://joomsport.com/helpdesk/
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

class bleagueViewseaslist extends JView
{

	function display($tpl = null)
	{

        $mainframe = JFactory::getApplication();
        $pathway  =& $mainframe->getPathway();
        $document =& JFactory::getDocument();
        $params	= &$mainframe->getParams();
        $menus	= &JSite::getMenu();
        $menu	= $menus->getActive();
        $option = 'com_joomsport';
        $tmpl = JRequest::getVar( 'tmpl', '', '', 'string' );;
        $limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
        $limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

        $db			=& JFactory::getDBO();

        $query = "SELECT COUNT(DISTINCT(s.s_id)) FROM  #__bl_seasons as s,  #__bl_tournament as tr WHERE s.t_id=tr.id AND tr.published = 1 AND s.published = 1";
        $db->setQuery($query);
        $total = $db->loadResult();

        jimport('joomla.html.pagination');
        $pageNav = new JPagination( $total, $limitstart, $limit );

        $query = "SELECT DISTINCT(s.s_id),s.* FROM  #__bl_seasons as s,  #__bl_tournament as tr WHERE s.t_id=tr.id  AND tr.published = 1 AND s.published = 1 ORDER BY s.s_name";
        $db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
        $rows = $db->loadObjectList();
        //  print_r(  $this->_rows);die;
        $tm = array();
        if(count($rows)){
            for($i = 0;$i<count($rows);$i++){
                $query = "SELECT COUNT(DISTINCT(st.team_id)) FROM  #__bl_season_teams as st RIGHT JOIN #__bl_teams as t ON st.team_id = t.id WHERE st.season_id = ".$rows[$i]->s_id." AND st.team_id != '0'";
                $db->setQuery($query);
                $total_team = $db->loadResult();
                $rows[$i]->num_t = $total_team;
            }

        }

        //if (is_object( $menu )) {
        //    $menu_params = new JRegistry;// new JParameter( $menu->params );
       ///     if (!$menu_params->get( 'page_title')) {
       ///         $params->set('page_title',	JText::_( $lists['p_title'] ));
       //     }
       // } else {
       //     $params->set('page_title',	JText::_( $lists['p_title'] ));
       // }
       // $document->setTitle( $params->get( 'page_title' ) );
       // $pathway->addItem( JText::_( $lists['p_title'] ));
//
        require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_joomsport'.DIRECTORY_SEPARATOR.'bl_lang.php');

        $this->assignRef('bl_lang', $bl_lang);
        $this->assignRef('tmpl', $tmpl);

        $this->assignRef('params',		$params);

        $this->assignRef('rows',		$rows);
        //$this->assignRef('lists',		$lists);
        $this->assignRef('page', $pageNav);
       // $this->assignRef('s_id', $s_id);
		parent::display($tpl);
		//require_once(dirname(__FILE__).'/tmpl/default'.($tpl?"_".$tpl:"").'.php');
		
		
	}
	
		
}
