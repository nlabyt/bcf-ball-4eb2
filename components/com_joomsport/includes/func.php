<?php
defined('_JEXEC') or die('Restricted access');

function date_bl($date,$time){
	$db			=& JFactory::getDBO(); 
	$format = "%d-%m-%Y %H:%M";
	if($date == '' || $date == '0000-00-00'){
		return '';
	}
	//echo $date.' - '.$time."<br />";
	$query = "SELECT cfg_value FROM #__bl_config WHERE cfg_name='date_format'";
	$db->setquery($query);
	$format = $db->loadResult();
	//switch ($format){
		//case "d-m-Y H:i": $format = "%d-%m-%Y %H:%M"; break;
		//case "m-d-Y g:i A": $format = "%m-%d-%Y %I:%M %p"; break;
		//case "j F, Y H:i": $format = "%m %B, %Y %H:%M"; break;
		//case "j F, Y g:i A": $format = "%m %B, %Y %I:%H %p"; break;
		//case "d-m-Y": $format = "%d-%m-%Y"; break;
		//case "l d F, Y H:i": $format = "%A %d %B, %Y  %H:%M"; break;
	//}
	
	if(!$time){
		$time = '00:00';
	}
	$time_m = explode(':',$time);
	$date_m = explode('-',$date);
	//echo $time_m[0].','.$time_m[1].','.$date_m[1].','.$date_m[2].','.$date_m[0];
	if(function_exists('date_default_timezone_set')){
		date_default_timezone_set('GMT');
	}
	$tm = @mktime($time_m[0],$time_m[1],'0',$date_m[1],$date_m[2],$date_m[0]);
	jimport('joomla.utilities.date');
	$dt = new JDate($tm,0);
	return $dt->format($format);
	//return JHTML::_('date',@mktime(substr($time,0,2),substr($time,3,2),0,substr($date,5,2),substr($date,8,2),substr($date,0,4)),$format,0);
}
function getVer(){
	$version = new JVersion;
	$joomla = $version->getShortVersion();
	return substr($joomla,0,3);
}
function getImgPop($img){
	$max_height = 500;
	$max_width = 600;
	$link = JURI::base().'media/bearleague/'.$img;
	$fileDetails = pathinfo(JURI::base().'media/bearleague/'.$img);
	$img_types = array('png','gif','jpg','jpeg');
        $ext = strtolower($fileDetails["extension"]);
	
	if (is_file(JPATH_ROOT.'/media/bearleague/'.$img) && in_array(strtolower($ext),$img_types)){
		$size = getimagesize(JPATH_ROOT.'/media/bearleague/'.$img);
		
		if($size[0] > $max_width && $size[0] > $size[1]){
			$link = JURI::base().'components/com_joomsport/includes/imgres.php?src='.$link.'&w='.$max_width;
		}else if($size[1] > $max_height && $size[1] > $size[0]){
			$link =JURI::base().'components/com_joomsport/includes/imgres.php?src='.$link.'&h='.$max_height;
		}
	}
	
	return $link;
}

function getConfigVal($val){
		$db			=& JFactory::getDBO(); 
		$query = "SELECT cfg_value FROM #__bl_config WHERE cfg_name='".$val."'";
		$db->setQuery($query);
		return $db->loadResult();
	}

/* page 
		1-season layout
		2-team layout
		3-player layout
		4-match layout
		5-venue layout
	*/
	function getSocialButtons($page,$title='',$img='',$txt=''){
		$doc =& JFactory::getDocument();
		$db			=& JFactory::getDBO(); 
		if(!getConfigVal($page)){
			return '';
			
		}
		
		
		$socbut = '';
		if(getConfigVal('jsb_twitter')){
			$socbut .= '<div class="jsd_buttons">
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" target="_blank">Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>';
		}
		if(getConfigVal('jsb_gplus')){
			$socbut .= '<div class="jsd_buttons">
							<g:plusone size="medium"></g:plusone>

							<script type="text/javascript">
							  window.___gcfg = {lang: "en"};

							  (function() {
								var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
								po.src = "https://apis.google.com/js/plusone.js";
								var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
							  })();
							</script>
						</div>';
		}
		
		if(getConfigVal('jsb_fbshare') || getConfigVal('jsb_fblike') || getConfigVal('jsb_gplus')){
			if($title){
				$doc->addCustomTag( '<meta property="og:title" content="'.htmlspecialchars($title).'"/> ' );
			}	
			if($img){
				$doc->addCustomTag( '<meta property="og:image" content="'.$img.'"/> ' );
			}
			//if($txt){
				$doc->addCustomTag( '<meta property="og:description" content="'.htmlspecialchars($txt?$txt:$title).'"/> ' );
			//}
		}
		
		if(getConfigVal('jsb_fbshare')){

			$socbut .= '<div class="jsd_buttons"  style="margin-right:15px;">
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>';

			$socbut .= '<div class="fb-send" data-href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'" data-font="verdana"></div>';

			$socbut .= '</div>';
		}
		if(getConfigVal('jsb_fblike')){
			
			$socbut .= '<div class="jsd_buttons">	
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>

							<div class="fb-like" data-send="false" data-layout="button_count" data-width="130" data-show-faces="true" data-font="verdana"></div>
						</div>';
		}
		
		return $socbut;
		
	}

?>