<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: images.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Images model for displaying images.
 */
/* No direct acesss */
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

class SimplephotogalleryModelimages extends JModel {
    /* function for executing queries */

	function getImages($albumId,$photoId)
	{   
		 
		//print_r($_REQUEST);die;
		/* if(JRequest::getVar('order'))
		{
			$imageId = ' AND sortorder = '.$photoId;
		}
		*/
		if($photoId != '' && !JRequest::getVar('order') )
		{
			$photoId = $photoId - 1;
			$photoVal = 'LIMIT '.$photoId.',1';
		}
		//print_r($_REQUEST);die;
		$db = & JFactory::getDBO();
		if(empty($albumId))
		{
			
			$query1 = "SELECT id FROM #__simplephotogallery_album where published = 1 order by id asc limit 1";
			$db->setQuery($query1);
        	$rows = $db->loadRow();
        	
			$query = "select * from #__simplephotogallery_images WHERE published = 1 ".$imageId." AND album_id = ".$rows[0];
			
		}
		else 
		{
        	$query = "select * from #__simplephotogallery_images WHERE published = 1 ".$imageId." AND album_id = ".$albumId." ORDER BY id ASC ".$photoVal;
		}
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
	}
	
	function totalImage($albumId){
		$db = & JFactory::getDBO();
        $query = "select count(*) as total from #__simplephotogallery_images WHERE published = 1 AND album_id = ".$albumId;
        $db->setQuery($query);
        
        $rows = $db->loadRow();
        return $rows;
	}
	
	function getAlbumName($albumId)
	{
		$db = & JFactory::getDBO();
		if(empty($albumId))
		{
			$query = "SELECT album_name,description,id FROM #__simplephotogallery_album where published = 1 order by id asc limit 1";
		}
		else 
		{
        	$query = "SELECT album_name,description,id FROM #__simplephotogallery_album where id = ".$albumId;
		}
        $db->setQuery($query);
        $settings = $db->loadRow();
        return $settings;
	}
	
	function getSettings()
	{
		$db = & JFactory::getDBO();
        $query = "SELECT * FROM #__simplephotogallery_settings";
        $db->setQuery($query);
        $settings = $db->loadObjectList();
        return $settings;
	}
	
	function getAlbum()
	{
		$db = & JFactory::getDBO();
		$query = "SELECT Distinct(a.id),a.album_name,a.description,b.album_id,b.album_cover,b.image FROM #__simplephotogallery_album as a left outer join #__simplephotogallery_images as b on b.album_id = a.id and b.album_cover = 1 where a.published=1 group by a.id DESC";
		$db->setQuery($query);
        $album = $db->loadObjectList();
        return $album;
	}
	
	/* function disableModule()
	{
		$db = & JFactory::getDBO();
		echo $query = "SELECT module FROM #__modules where published = 1 and (position = 'left' or position='right')";
		$db->setQuery($query);
        $album = $db->loadObjectList();
        print_r($album);die;
        return $album;
	} */
}