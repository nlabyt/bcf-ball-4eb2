<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: simplephotogallery.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    simplephotogallery model for listing featured images and album list.
 */
/* No direct acesss */
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

class SimplephotogalleryModelsimplephotogallery extends JModel {
    /* function for executing queries */

	function getImages()
	{
		$db = & JFactory::getDBO();
        $query = "SELECT * FROM #__simplephotogallery_settings";
        $db->setQuery($query);
        $settings = $db->loadObjectList();
       	$rowCount = $settings[0]->feat_rows;
       	$columnCount = $settings[0]->feat_cols;
       	$totalCount = $rowCount * $columnCount;
       	
        
		$db = & JFactory::getDBO();
        $query = "select img.* from #__simplephotogallery_images as img inner join #__simplephotogallery_album as alb on alb.id = img.album_id WHERE img.is_featured = 1 AND img.published = 1 ORDER BY rand() LIMIT $totalCount ";
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        
        return $rows;
	}
	
	function getSettings()
	{
		$db = & JFactory::getDBO();
        $query = "SELECT * FROM #__simplephotogallery_settings";
        $db->setQuery($query);
        $settings = $db->loadObjectList();
        return $settings;
	}
	
	function getTotalAlbum()
	{
		$db = & JFactory::getDBO();
        $query = "SELECT count(*) FROM #__simplephotogallery_album where published = '1'";
        $db->setQuery($query);
        $settings = $db->loadRow();
        return $settings;
	}
	
	function getAlbum()
	{
		$db = & JFactory::getDBO();
		$query = "SELECT Distinct(a.id),a.album_name,a.description,b.album_id,b.album_cover,b.image FROM #__simplephotogallery_album as a left outer join #__simplephotogallery_images as b on b.album_id = a.id and b.album_cover = 1 where a.published=1 group by a.id DESC";
		$db->setQuery($query);
        $album = $db->loadObjectList();
        return $album;
	}
	
}