<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: controller.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Controller to list albums and images.
 */
error_reporting(E_ERROR |  E_PARSE);
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');


/**
 * details Component Controller
 */
class simplephotogalleryController extends JController {

    /**
     * Method to display the view
     *
     * @access    public
     */
    function display() {

        
        parent::display();
    }
    
    function download()
    {
    	//print_r($_REQUEST);
    	$albumid = JRequest::getVar('albumid');
    	$photoid = JRequest::getVar('photoid');
    	$db = & JFactory::getDBO();
        $query = "select image from #__simplephotogallery_images WHERE album_id = ".$albumid." AND sortorder = ".$photoid;
        $db->setQuery($query);
        $rows = $db->loadRow();
        //print_r($rows[0]);die;
    	$baseurl = JURI::base();
    	
    	
		$file = JURI::base() . 'images/photogallery/'.str_replace(' ','%20',trim($rows[0]));
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=" . basename($file));
		
		header("Content-Description: File Transfer");
		@readfile($file);
		jexit();
    }

}

?>