<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: router.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Router file used for SEO Friendly URL.
 */
function simplephotogalleryBuildRoute(&$query) {
    $segments = array();
	$db = JFactory::getDBO();
    if (isset($query['view'])) {
        $segments[] = $query['view'];
        unset($query['view']);
    }
	if(isset($query['albumid'])){
	$q="SELECT id,alias_name FROM #__simplephotogallery_album WHERE id='".$query['albumid']."'";
	$db->setQuery($q);
	$albumTitle = $db->loadRow();
	$segments[] = $albumTitle[1];
	unset( $query['albumid'] );
	}
	if(isset($query['photoid'])){
		$albumId = $albumTitle[0];
	 	$q="SELECT alias_name FROM #__simplephotogallery_images WHERE album_id=".$albumId." AND sortorder='".$query['photoid']."'";
	
	$db->setQuery($q);
	$imageTitle = $db->loadResult();
	$segments[] = $imageTitle;
	unset( $query['photoid'] );
	}
	
    //echo "test";
    //print_r($segments);
    return $segments;
}


/**
 * @param	array	A named array
 * @param	array
 *
 * Formats:

 */
function simplephotogalleryParseRoute($segments) {
	$db = JFactory::getDBO();
	//print_r($segments);
    $vars = array();
    // view is always the first element of the array
    $count = count($segments);
	//print_r($segments);
    if ($count) {
        switch ($segments[0]) {
            case 'images':
                $vars['view'] = 'images';

                if (isset($segments[1]))
                {
                    $vars['albumid'] = $segments[1];
                    $vars['albumid'] = str_replace(":", "-", $vars['albumid']);
                    $q="SELECT id FROM #__simplephotogallery_album WHERE alias_name = '". $vars['albumid']."'";
					$db->setQuery($q);
					$albumId = $db->loadResult();
                    //$albumId = explode('_',$segments[1] );
                    $vars['albumid'] = $albumId;
                }
                if (isset($segments[2]))
                {
                    $vars['photoid'] = $segments[2];
                    $vars['photoid'] = str_replace(":", "-", $vars['photoid']);
                    $q="SELECT sortorder FROM #__simplephotogallery_images WHERE album_id = ".$vars['albumid']." AND alias_name = '". $vars['photoid']."'";
					$db->setQuery($q);
					$imageId = $db->loadResult();
					
                    $vars['photoid'] = $imageId;
                }
                    //echo $vars['albumid'].'sd';
                break;
        }
    }
    //print_r($vars);
    //print_r($vars['albumid']);
    return $vars;
}

?>
