
<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Default page to display album and image list.
 */
//print_r($_REQUEST);
$session = JFactory::getSession();
//$mymessage = $session->get('backlist');

if(!JRequest::getVar('photoid'))
{
$session->set('backlist', 'albumPage');
}
$mymessage = $session->get('backlist');

$images = $this->images;
//print_r($images);
$album = $this->album;
$model = &$this->getModel();
$albumId = JRequest::getVar('albumid');
$totalPhotos = $model->totalImage($albumId);
$totalPhotos = $totalPhotos[0];
//echo count($images);
//print_r($images);
$settings = $this->settings;
//print_r($settings);
$fullImageWidth = $settings[0]->fullimg_width;
$fullImageHeight = $settings[0]->fullimg_height;
$isShare = $settings[0]->general_share_photo;
$isDownload = $settings[0]->general_download;
$isShowAlbum = $settings[0]->general_show_alb;
$api_key = $settings[0]->facebook_api;
$vspace = $settings[0]->photo_vspace;
$hspace = $settings[0]->photo_hspace;
$twidth = $settings[0]->thumbimg_width;
$theight = $settings[0]->thumbimg_height;

$featVspace = $settings[0]->feat_vspace;
$featHspace = $settings[0]->feat_hspace;
$featWidth = $settings[0]->feat_photo_width;
$featHeight = $settings[0]->feat_photo_height;
$albVspace = $settings[0]->alb_vspace;
$albHspace = $settings[0]->alb_hspace;
$albWidth = $settings[0]->alb_photo_width;
$albHeight = $settings[0]->alb_photo_height;
//$totalPhotos = 
$photoOrderNo = JRequest::getVar('photoid');
$leftArrow = JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/left-arrow.png';
$rightArrow = JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/right-arrow.png';
$document =& JFactory::getDocument();
$document->addCustomTag('<html lang="en" class="ie8 ielt9"> <html lang="en">');
?>

<?php
$document->addStyleSheet(JURI::base().'components/com_simplephotogallery/zoominout/iestyle.css');
?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/jquery-1.4.2.min.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/jquery.jcarousel.min.js';?>"></script>


<link rel="stylesheet" type="text/css" media="all" href="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/skin.css';?>" />
<html lang="en" class="ie8 ielt9"> <html lang="en">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/iestyle.css';?>" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<script type="text/javascript"> 
 
function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });
 
    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });
 
    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};
var simplePhotoGallery = jQuery.noConflict();
simplePhotoGallery(document).ready(function() {
	simplePhotoGallery('#mycarouselPhoto').jcarousel({
        auto: 3,
        wrap: 'last',
        initCallback: mycarousel_initCallback,
        itemFallbackDimension: 300,
        visible:3
    });
});
 
</script>

		
<?php
if($photoOrderNo <= 1 )	
		  		{
		  			$leftArrowId = 0;
		  		}
		  		else{
		  			$leftArrowId = $photoOrderNo - 1;
		  		}
		  		if($totalPhotos <= $photoOrderNo )
		  		{
		  			$rightArrowId = 0;
		  		}
		  		else{
		  			$rightArrowId = $photoOrderNo + 1;
		  		}
if(JRequest::getVar('photoid') != "")
{
	
	//echo JURI::current();
	//print_r($_REQUEST);
	$downloadID = JRequest::getVar('photoid');
	$downloadAlbumId = JRequest::getVar('albumid');
	$fbUrl1 = 'http://www.facebook.com/dialog/feed?app_id='.$api_key.'&link='.urlencode(JRoute::_(JURI::current().'?option=com_simplephotogallery&view=images&albumid='.JRequest::getInt("albumid").'&photoid='.JRequest::getInt("photoid"))).'&picture='.urlencode(str_replace(" ","",JURI::root().'images/photogallery/thumb_image/'.trim($images[0]->image))).'&name='.$images[0]->title.'&description='.$images[0]->description.'&redirect_uri='.urlencode(JRoute::_(JURI::current().'?option=com_simplephotogallery&view=images&albumid='.JRequest::getInt("albumid").'&photoid='.JRequest::getInt("photoid")));
	$u =& JFactory::getURI();
	$fbUrl = 'http://www.facebook.com/sharer.php?s=100&p[title]='.$images[0]->title.'&p[url]='.urlencode($u->toString()).'&p[summary]='.$images[0]->description.'&p[images][0]='.JURI::root().'images/photogallery/thumb_image/'.trim($images[0]->image);
?>
<style>
html.ielt9 #iviewer .viewer {
			display: block;
}
<!--
.fullscreecss {
border: 1px solid white;
float: left;
padding: 2px 5px;
}
.fullscreecss a {
font-size: 1.2em;
    text-decoration: none;
}
.fullscreecss span{font-family: Georgia,"Bitstream Charter",serif;}
.fullscreecss:hover {
border: 1px solid #CCC;
border-bottom-left-radius: 2px 2px;
border-bottom-right-radius: 2px 2px;
border-top-left-radius: 2px 2px;
border-top-right-radius: 2px 2px;
padding: 2px 5px;

}
.fullscreecss a:hover, .fullscreecss a:focus{background-color:#fff !important;color: black !important;}
-->

.fullImageView tr, td{border: none !important;}
</style>


<div style="float:left;margin-right:10px;padding-bottom:2px;line-height: 15px;">
			    		<div class="fullscreecss">
		        <a  id='go' style="text-decoration: none;background-color:none !important;" href="<?php echo JURI::base().'images/photogallery/full_image/'.$images[0]->image?>"> 
				<img src="<?php echo JURI::base().'components/com_simplephotogallery/images/fullscreen.png';?>">
				<span>Full Screen</span></a>
				        </div></div>
		<?php if($isDownload == '1'){?>		        
<div style="float:left;margin-right:10px;line-height: 15px;" class="fullscreecss">
<a style="text-decoration: none;" href="<?php echo JURI::base().'index.php?option=com_simplephotogallery&task=download&albumid='.$downloadAlbumId.'&photoid='.$downloadID; ?>">
<img src="<?php echo JURI::base().'components/com_simplephotogallery/images/download.png';?>">
<span><?php echo JText::_('Download') ?></span>
</a>
</div>
<?php } if($isShare == '1'){?>
<div style="float:left;line-height: 15px;"><div class="fullscreecss">
				<a  class="links" title="Facebook Share" href="javascript:void(0);" onClick='window.open("<?php echo $fbUrl; ?>","facebookShare","scrollbars=1,width=800,height=400")' style="text-decoration: none;" >
				        <img src="<?php echo JURI::base().'components/com_simplephotogallery/images/share.png';?>">
				<span> Share</span></a>
				</div>
				</div>
				<?php } ?>
				<?php
	
//echo '<a href="http://www.facebook.com/sharer.php?s=100&p[title]=BarcodeID&p[url]="'.urlencode(JRoute::_(JURI::current().'"?option=com_simplephotogallery&view=images&albumid="'.JRequest::getInt('albumid').'&photoid="'.JRequest::getInt('photoid'))).'"&p[images][0]="'.JURI::base().'images/photogallery/full_image/'.$images[0]->image.'">share</a>';
//echo '<a href="http://www.facebook.com/sharer.php?s=100&p[title]=BarcodeID">share</a>';
?>
<div id="iviewer">
		        <div class="loader"></div>
		        <div class="viewer"></div>
		        <ul class="controls">
		            <li style="list-style: none;" class="close"></li>
		            <li style="list-style: none;" class="zoomin"></li>
		            <li style="list-style: none;" class="zoomout"></li>
		        </ul>
	    	</div>
	    	<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/js/jquery.js'?>"></script>
		<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/js/jqueryui.js'?>"></script>
		<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/js/jquery.iviewer.js'?>"></script>
		<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/js/main.js'?>"></script>
		<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/zoomstyle.css'?>" /> 
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo JURI::base().'components/com_simplephotogallery/zoominout/colorbox1.css'?>" />

<div style="float: right;">
<?php 
if($mymessage == "feature")
{
	$photoListURL = JRoute::_("index.php?option=com_simplephotogallery");
}
else 
{
	$photoListURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".JRequest::getVar('albumid'));
}
?>
<a style='text-decoration: none;' href='<?php echo $photoListURL; ?>'>
<img title="Back to photo list" src="<?php echo JURI::base().'components/com_simplephotogallery/images/back.jpg'?>">
</a>
</div>
<br><br>
<div style="clear: both;"></div>
<table class='fullImageView' width="100%" style="margin-bottom: 10px;">

<tr>
<td width="45px">
<?php

if($leftArrowId){  // show left arrow 
$photoURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".JRequest::getVar('albumid')."&photoid=".($photoOrderNo-1));
	?>  	
									
									<img style="cursor: pointer;" src="<?php echo $leftArrow; ?>" onclick="showsliding('<?php echo $photoURL ?>'); " />
									 
						<?php   }  ?>
						</td>
<td class="singlePhoto" style="text-align: center;">

<img width='<?php echo $fullImageWidth;?>' height='<?php echo $fullImageHeight;?>' style="box-shadow:2px 2px 8px #888888" src="<?php echo JURI::base().'images/photogallery/full_image/'.$images[0]->image?>">
</td>
<td width="45px">

<?php 	if($rightArrowId){
	$photoURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".JRequest::getVar('albumid')."&photoid=".($photoOrderNo+1));
	// show right arrow  ?>   
							 	
							 		<img style="cursor: pointer;" src="<?php echo $rightArrow; ?>" onclick="showsliding('<?php echo $photoURL;?>')"  />
							 	 
							<?php 	} //if for photosno right arrow ?>	
</td>
</tr>
</table>


<div><span style="font-family: Georgia,Bitstream Charter,serif;font-size:16px;font-weight: bold;"><?php echo ucfirst($images[0]->title).':'; ?></span>
<span style="font-family: Georgia,Bitstream Charter,serif;font-size:15px;">
<?php
if($images[0]->description)
{
echo ucfirst($images[0]->description);
}
else
{ 
echo "No description available";
}
?>
</span></div>
<div style="height: 30px;"></div>
<?php 
}
else 
{
	//print_r($_REQUEST);
	$model = $this->getModel();
	$albumName = $model->getAlbumName(JRequest::getVar('albumid'));
	//print_r($albumName);
	if(count($images) > 0)
	{
?>
<h1>Photos of <?php echo $albumName[0]?></h1>
<?php echo $albumName[1]?>
<div class="imageList">
<?php
for($i = 0;$i < count($images); $i++)
{
	$imageURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".$albumName[2]."&photoid=".($i+1));
?>
<div style="cursor:pointer;width:<?php echo $twidth ?>px;height:<? echo $theight?>px; float: left;box-shadow:2px 2px 8px #888888;margin: <?php echo $vspace.'px '.$hspace.'px' ?>;">
<div onclick="showPhoto('<?php echo $imageURL?>')">
<img width="<?php echo $twidth?>" height="<?php echo $theight?>" src="<?php echo JURI::base().'images/photogallery/thumb_image/'.$images[$i]->image?>">


<div style="color: #888888;font-family: arial,sans-serif;font-size: 8pt;height: 1.25em;overflow: hidden;text-align: center;"><?php echo $images[$i]->title?></div>
    </div>
</div>
<?php 
}
	}
	else 
	{
		echo '<h1>Photos</h1>';
		echo "No photos.";
	}
?>


</div>

<div style="clear: both;height: 30px;"> </div>
<?php } ?>
<div style = 'clear:both;'></div>
<?php 
if(!JRequest::getVar('photoid'))
{
?>
<style>
.jcarousel-skin-tango1 .jcarousel-next-horizontal
{
	top:<?php echo ($albHeight/2)+$albVspace?>px !important;
}

.jcarousel-skin-tango1 .jcarousel-prev-horizontal
{
	top:<?php echo ($albHeight/2)+$albVspace?>px !important;
}
#wrap a:hover, #wrap a:focus{background-color:#fff !important;color: black !important;}
</style>
<h1> Albums </h1>
<?php if( count($album) <=3 )
{
?>
<style>
<!--
.jcarousel-skin-tango1 .jcarousel-next-horizontal{display: none !important;}
-->
</style>
<?php } ?>
<?php 
if(count($album) > 0)
{
?>
<div id="wrap" style="width:<?php echo (3*$albWidth)+(6*$albHspace)+10;?>px">
<ul id="mycarouselPhoto" class="jcarousel-skin-tango1" >
<?php 
for($j = 0;$j < count($album); $j++)
{
?>
<li style="background:none !important;cursor:pointer;padding:0px !important;float:left;list-style:none outside none !important;margin:<?php echo $albVspace.'px '.$albHspace?>px">
<?php $strUrl = 'index.php?option=com_simplephotogallery&view=images&albumid='.$album[$j]->id; ?>
<a class="album-thumb" href="<?php echo JRoute::_($strUrl)?>" style=" padding-top: 1px;text-decoration:none;" >
<?php 
if($album[$j]->image != '')
{
echo "<img style='box-shadow:1px 1px 2px #888888;' width='".$albWidth."px' height='".$albHeight."px' src='".JURI::base().'images/photogallery/album_image/'.$album[$j]->image."'>";
}
else
{
echo "<img width='".$albWidth."px' height='".$albHeight."px' src='".JURI::base()."components/com_simplephotogallery/images/default_star.gif'>";
}
?>
<br>
<b><?php echo $album[$j]->album_name?></b>
</a>
</li>
<?php } ?>

</ul>  </div>
<?php
}
else{
	echo "No albums.";
} 
}
?>
<div style = 'clear:both;'></div>
<script type="text/javascript">
function changepage(albumid)
{
  
    window.location.href = '<?php echo JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=")?>'+albumid;
}
    function showPhoto(imageURL)
    {
        window.location.href = imageURL;
    }

    function showsliding(photoURL)
    {
        
        window.location.href = photoURL;
    }
    
    
    </script>