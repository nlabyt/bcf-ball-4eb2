<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Image view to retrieves the data to be displayed and pushes it into the template.
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/* Album view  */

class SimplephotogalleryViewimages extends JView {

    function display() 
    {
    	$photoId = JRequest::getVar('photoid');
    	
    	$albumId = JRequest::getVar('albumid');
    	if($photoId)
    	{
    		$model = &$this->getModel('images');
    		$images = $model->getImages($albumId,$photoId);
    		
    		//$moduleDisable = $model->disableModule();
    	}
    	else
    	{
    		$model = &$this->getModel('images');
    		$images = $model->getImages($albumId,$photoId);
    	}
    	
    	//print_r($images);
    	$this->assignRef('images', $images);
    	//print_r($_REQUEST);
    	$settings = $model->getSettings();
    	$this->assignRef('settings', $settings);
    	
    	$album = $model->getAlbum();
    	$this->assignRef('album', $album);
    	//print_r($album);
    	$albumName = $model->getAlbumName($albumId);
    	$totalPhotos = $model->totalImage($albumId);
		$totalPhotos = $totalPhotos[0];
    	//print_r($albumName);
    	$mainframe = &JFactory::getApplication();
        $pathway   =& $mainframe->getPathway();
         if (JRequest::getCmd("view") == "images" && !JRequest::getVar("photoid")) {
            $pathway->addItem(JText::_("Albums"), JRoute::_("index.php?option=com_simplephotogallery") );
            $pathway->addItem(ucfirst($albumName[0]), '');
            
         }
         else if (JRequest::getCmd("view") == "images" && JRequest::getVar("photoid")) {
         	$pathway->addItem(ucfirst($albumName[0]), JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".JRequest::getVar('albumid')) );
            $pathway->addItem(ucfirst($images[0]->title), '');
            $pathway->addItem(JText::_('Photo').' '.JRequest::getVar("photoid").' of '.$totalPhotos, '');
            
            
            
         }
    	//$album = $model->getAlbum();
    	//$this->assignRef('album', $album);
    	parent::display();
    }
}