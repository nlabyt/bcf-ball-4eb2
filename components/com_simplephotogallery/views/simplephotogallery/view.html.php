<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: view.html.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Simplephotogallery view to retrieves the data to be displayed and pushes it into the template.
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/* Album view  */

class SimplephotogalleryViewsimplephotogallery extends JView {

    function display() 
    {
    	
    	$model = &$this->getModel('simplephotogallery');
    	$images = $model->getImages();
    	$this->assignRef('images', $images);
    	
    	$settings = $model->getSettings();
    	$this->assignRef('settings', $settings);
    	
    	$album = $model->getAlbum();
    	$this->assignRef('album', $album);
    	parent::display();
    }
}