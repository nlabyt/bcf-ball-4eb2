
<?php
/**
 * @name        Simple Photo Gallery
 * @version	1.0: default.php
 * @since       Joomla 1.5
 * @subpackage	com_simplephotogallery
 * @author      Apptha
 * @copyright   Copyright (C) 2011 Powered by Apptha
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @abstract    Default page to display album list and image list.
 */
defined('_JEXEC') or die('restricted access');
jimport('joomla.application.component.view');



$model = $this->getModel();
$totalAlbum = $model->getTotalAlbum();
$totalAlbum = $totalAlbum[0];
//print_r($totalAlbum);
$images = $this->images;
//print_r($images);
$settings = $this->settings;
$isShowAlbum = $settings[0]->general_show_alb;
$album = $this->album;
//print_r($album);
$featVspace = $settings[0]->feat_vspace;
$featHspace = $settings[0]->feat_hspace;
$featWidth = $settings[0]->feat_photo_width;
$featHeight = $settings[0]->feat_photo_height;
$albVspace = $settings[0]->alb_vspace;
$albHspace = $settings[0]->alb_hspace;
$albWidth = $settings[0]->alb_photo_width;
$albHeight = $settings[0]->alb_photo_height;

$session = JFactory::getSession();
$session->set('backlist', 'feature');
$mymessage = $session->get('backlist');
//echo $mymessage;
?>
<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/jquery-1.4.2.min.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/jquery.jcarousel.min.js';?>"></script>


<link rel="stylesheet" type="text/css" media="all" href="<?php echo JURI::base().'components/com_simplephotogallery/pica_Carousel_with_autoscrolling_files/skin.css';?>" />


<script type="text/javascript"> 
 
function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });
 
    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });
 
    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};
var simplePhotoGallery = jQuery.noConflict();
simplePhotoGallery(document).ready(function() {
	simplePhotoGallery('#mycarouselPhoto').jcarousel({
        auto: 3,
        wrap: 'last',
        initCallback: mycarousel_initCallback,
        itemFallbackDimension: 300,
        visible:3
    });
});
 
</script>
<h1 class="entry-title">Featured Photos</h1>
<div class="featuredPhoto">
<?php
if(count($images) > 0)
{
$a = "";
for ($i = 0; $i < count($images); $i++,$a++) 
{
					
					if($a >= $settings[0]->feat_cols)  // for showing number of photos per row
			      	{
			      		$a = 0;
			      		echo "<div style = 'clear:both;'></div>";
			      	}
$imageURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".$images[$i]->album_id."&photoid=".$images[$i]->sortorder);
			      	?>
<a href="<?php echo $imageURL;?>">
<div style="cursor:pointer;float: left;box-shadow:2px 2px 8px #888888;margin: <?php echo $featVspace.'px '.$featHspace.'px' ?>;width: <?php echo $featWidth;?>px">
	
	<img height="<?php echo $featHeight;?>" width="<?php echo $featWidth;?>" src="<?php echo JURI::base().'images/photogallery/featured_image/'.$images[$i]->image;?>">
	<br>
	<?php echo $images[$i]->album_name; ?>
	</div>
	</a>
<?php } } else {
 echo "No featured photos.";
}?>
</div>

<div style = 'clear:both;'></div>
<?php if($isShowAlbum == '1'){?>
<style>
<!--
#wrap a:hover, #wrap a:focus{background-color:#fff !important;color: black !important;}

.jcarousel-skin-tango1 .jcarousel-next-horizontal
{
	top:<?php echo ($albHeight/2)+$albVspace?>px !important;
}

.jcarousel-skin-tango1 .jcarousel-prev-horizontal
{
	top:<?php echo ($albHeight/2)+$albVspace?>px !important;
}
-->
</style>
<h1> Albums </h1>
<?php if( count($album) <=3 )
{
?>
<style>
<!--
.jcarousel-skin-tango1 .jcarousel-next-horizontal{display: none !important;}
-->

</style>
<?php } ?>
<?php
if(count($album) > 0)
{
?>
<div id="wrap" style="width:<?php echo (3*$albWidth)+(6*$albHspace)+5;?>px">
<ul id="mycarouselPhoto" class="jcarousel-skin-tango1">
<?php 
for($j = 0;$j < count($album); $j++)
{
	//$imageURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".$images[$i]->album_id."&photoid=".$images[$i]->sortorder);
	$albURL = JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=".$album[$j]->id);
?>
<li style="background:none !important;cursor:pointer;padding:0px !important;float:left;list-style:none outside none !important;margin:<?php echo $albVspace.'px '.$albHspace?>px" >
<!--   onclick="changepage(<?php echo $album[$j]->id ?>)" -->
<?php $strUrl = 'index.php?option=com_simplephotogallery&view=images&albumid='.$album[$j]->id; ?>

<a class="album-thumb" href="<?php echo JRoute::_($strUrl)?>" style=" padding-top: 1px;text-decoration:none;" >
<?php 
if($album[$j]->image != '')
{
echo "<img style='box-shadow:1px 1px 2px #888888;' width='".$albWidth."px' height='".$albHeight."px' src='".JURI::base().'images/photogallery/album_image/'.$album[$j]->image."'>";
}
else
{
echo "<img width='".$albWidth."px' height='".$albHeight."px' src='".JURI::base()."components/com_simplephotogallery/images/default_star.gif'>";
}
?>
<br>
<b><?php echo $album[$j]->album_name?></b> </a>
</li>
<?php } ?>

</ul>  </div>
<?php }  
else {
	echo "No albums.";
}
 }  ?>
<div style = 'clear:both;'></div>
<script type="text/javascript">
    function changepage(albumid)
    { alert('<?php echo JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=")?>'+albumid)
        window.location.href = '<?php echo JRoute::_("index.php?option=com_simplephotogallery&view=images&albumid=")?>'+albumid;
    }

    function showPhoto(imageURL)
    {
        window.location.href = imageURL;
    }
    
    
    </script>
  