<?php die("Access Denied"); ?>#x#a:2:{s:6:"output";s:0:"";s:6:"result";O:8:"stdClass":52:{s:2:"id";s:1:"8";s:5:"title";s:33:"Championnat Féminin Outdoor 2012";s:5:"alias";s:32:"championnat-feminin-outdoor-2012";s:5:"catid";s:2:"10";s:9:"published";s:1:"1";s:9:"introtext";s:19285:"<h1>Championnat Outdoor Féminin IDF 2012</h1>
<table width="100%" border="0">
<tbody>
<tr>
<td><img src="images/miniidffem2012.jpg" border="0" alt="" style="display: block; margin-left: auto; margin-right: auto;" /><br /><br /></td>
</tr>
<tr>
<td><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:TrackMoves/>
  <w:TrackFormatting/>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>FR</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]-->
<p class="MsoNormal">Le championnat printemps-été 2012 pour l’équipe féminine fut riche en émotions.</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal">Les matchs des filles se sont joués les dimanches sur 11 journées du 25 mars au 17 juin 2012. Le championnat se composait de cinq équipes: Le BCF, Thiais (Tenantes du titre), BAT, le PUC et Limeil-Noisy.</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal">Le 01/07, Le BCF perd en finale 5-8 contre l’équipe de Thiais.</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal">Florence repart avec le titre de meilleure frappeuse du championnat IDF 2012</p>
<!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tableau Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0cm;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;}
</style>
<![endif]--></td>
</tr>
</tbody>
</table>";s:8:"fulltext";s:0:"";s:5:"video";N;s:7:"gallery";N;s:12:"extra_fields";s:2:"[]";s:19:"extra_fields_search";s:0:"";s:7:"created";s:19:"2013-04-05 11:47:50";s:10:"created_by";s:2:"42";s:16:"created_by_alias";s:0:"";s:11:"checked_out";s:1:"0";s:16:"checked_out_time";s:19:"0000-00-00 00:00:00";s:8:"modified";s:19:"2013-07-02 22:25:14";s:11:"modified_by";s:2:"42";s:10:"publish_up";s:19:"2013-04-05 11:36:28";s:12:"publish_down";s:19:"0000-00-00 00:00:00";s:5:"trash";s:1:"0";s:6:"access";s:1:"1";s:8:"ordering";s:1:"2";s:8:"featured";s:1:"0";s:17:"featured_ordering";s:1:"0";s:13:"image_caption";s:0:"";s:13:"image_credits";s:0:"";s:13:"video_caption";s:0:"";s:13:"video_credits";s:0:"";s:4:"hits";i:0;s:6:"params";O:10:"JParameter":5:{s:7:" * _raw";s:3588:"{"enable_css":"1","jQueryHandling":"1.8remote","backendJQueryHandling":"remote","userName":"1","userImage":"1","userDescription":"1","userURL":"1","userEmail":"0","userFeedLink":"1","userFeedIcon":"1","userItemCount":"10","userItemTitle":"1","userItemTitleLinked":"1","userItemDateCreated":"1","userItemImage":"1","userItemIntroText":"1","userItemCategory":"1","userItemTags":"1","userItemCommentsAnchor":"1","userItemReadMore":"1","userItemK2Plugins":"1","tagItemCount":"10","tagItemTitle":"1","tagItemTitleLinked":"1","tagItemDateCreated":"1","tagItemImage":"1","tagItemIntroText":"1","tagItemCategory":"1","tagItemReadMore":"1","tagItemExtraFields":"0","tagOrdering":"","tagFeedLink":"1","tagFeedIcon":"1","genericItemCount":"10","genericItemTitle":"1","genericItemTitleLinked":"1","genericItemDateCreated":"1","genericItemImage":"1","genericItemIntroText":"1","genericItemCategory":"1","genericItemReadMore":"1","genericItemExtraFields":"0","genericFeedLink":"1","genericFeedIcon":"1","feedLimit":"10","feedItemImage":"1","feedImgSize":"S","feedItemIntroText":"1","feedTextWordLimit":"","feedItemFullText":"1","feedItemTags":"0","feedItemVideo":"0","feedItemGallery":"0","feedItemAttachments":"0","feedBogusEmail":"","introTextCleanup":"0","introTextCleanupExcludeTags":"","introTextCleanupTagAttr":"","fullTextCleanup":"0","fullTextCleanupExcludeTags":"","fullTextCleanupTagAttr":"","xssFiltering":"0","linkPopupWidth":"900","linkPopupHeight":"600","imagesQuality":"100","itemImageXS":"100","itemImageS":"200","itemImageM":"400","itemImageL":"600","itemImageXL":"900","itemImageGeneric":"300","catImageWidth":"100","catImageDefault":"1","userImageWidth":"100","userImageDefault":"1","commenterImgWidth":"48","onlineImageEditor":"splashup","imageTimestamp":"0","imageMemoryLimit":"","socialButtonCode":"","twitterUsername":"","facebookImage":"1","comments":"1","commentsOrdering":"DESC","commentsLimit":"10","commentsFormPosition":"below","commentsPublishing":"1","commentsReporting":"2","commentsReportRecipient":"","inlineCommentsModeration":"0","gravatar":"1","recaptcha":"0","commentsFormNotes":"1","commentsFormNotesText":"","frontendEditing":"1","showImageTab":"1","showImageGalleryTab":"1","showVideoTab":"1","showExtraFieldsTab":"1","showAttachmentsTab":"1","showK2Plugins":"1","sideBarDisplayFrontend":"0","mergeEditors":"1","sideBarDisplay":"1","attachmentsFolder":"","hideImportButton":"0","googleSearch":"0","googleSearchContainer":"k2GoogleSearchContainer","K2UserProfile":"1","K2UserGroup":"","redirect":"","adminSearch":"simple","cookieDomain":"","taggingSystem":"1","lockTags":"0","showTagFilter":"0","k2TagNorm":"0","k2TagNormCase":"lower","k2TagNormAdditionalReplacements":"","recaptcha_public_key":"","recaptcha_private_key":"","recaptcha_theme":"clean","recaptchaOnRegistration":"0","stopForumSpam":"0","stopForumSpamApiKey":"","showItemsCounterAdmin":"1","showChildCatItems":"1","disableCompactOrdering":"0","metaDescLimit":"150","enforceSEFReplacements":"0","SEFReplacements":"","k2Sef":"0","k2SefLabelCat":"content","k2SefLabelTag":"tag","k2SefLabelUser":"author","k2SefLabelSearch":"search","k2SefLabelDate":"date","k2SefLabelItem":"0","k2SefLabelItemCustomPrefix":"","k2SefInsertItemId":"1","k2SefItemIdTitleAliasSep":"dash","k2SefUseItemTitleAlias":"1","k2SefInsertCatId":"1","k2SefCatIdTitleAliasSep":"dash","k2SefUseCatTitleAlias":"1","sh404SefLabelCat":"","sh404SefLabelUser":"blog","sh404SefLabelItem":"2","sh404SefTitleAlias":"alias","sh404SefModK2ContentFeedAlias":"feed","sh404SefInsertItemId":"0","sh404SefInsertUniqueItemId":"0","cbIntegration":"0"}";s:7:" * _xml";N;s:12:" * _elements";a:0:{}s:15:" * _elementPath";a:1:{i:0;s:63:"/homez.714/bcfsoftb/www/libraries/joomla/html/parameter/element";}s:7:" * data";O:8:"stdClass":270:{s:10:"enable_css";s:1:"1";s:14:"jQueryHandling";s:9:"1.8remote";s:21:"backendJQueryHandling";s:6:"remote";s:8:"userName";s:1:"1";s:9:"userImage";s:1:"1";s:15:"userDescription";s:1:"1";s:7:"userURL";s:1:"1";s:9:"userEmail";s:1:"0";s:12:"userFeedLink";s:1:"1";s:12:"userFeedIcon";s:1:"1";s:13:"userItemCount";s:2:"10";s:13:"userItemTitle";s:1:"1";s:19:"userItemTitleLinked";s:1:"1";s:19:"userItemDateCreated";s:1:"1";s:13:"userItemImage";s:1:"1";s:17:"userItemIntroText";s:1:"1";s:16:"userItemCategory";s:1:"1";s:12:"userItemTags";s:1:"1";s:22:"userItemCommentsAnchor";s:1:"1";s:16:"userItemReadMore";s:1:"1";s:17:"userItemK2Plugins";s:1:"1";s:12:"tagItemCount";s:2:"10";s:12:"tagItemTitle";s:1:"1";s:18:"tagItemTitleLinked";s:1:"1";s:18:"tagItemDateCreated";s:1:"1";s:12:"tagItemImage";s:1:"1";s:16:"tagItemIntroText";s:1:"1";s:15:"tagItemCategory";s:1:"1";s:15:"tagItemReadMore";s:1:"1";s:18:"tagItemExtraFields";s:1:"0";s:11:"tagOrdering";s:0:"";s:11:"tagFeedLink";s:1:"1";s:11:"tagFeedIcon";s:1:"1";s:16:"genericItemCount";s:2:"10";s:16:"genericItemTitle";s:1:"1";s:22:"genericItemTitleLinked";s:1:"1";s:22:"genericItemDateCreated";s:1:"1";s:16:"genericItemImage";s:1:"1";s:20:"genericItemIntroText";s:1:"1";s:19:"genericItemCategory";s:1:"1";s:19:"genericItemReadMore";s:1:"1";s:22:"genericItemExtraFields";s:1:"0";s:15:"genericFeedLink";s:1:"1";s:15:"genericFeedIcon";s:1:"1";s:9:"feedLimit";s:2:"10";s:13:"feedItemImage";s:1:"1";s:11:"feedImgSize";s:1:"S";s:17:"feedItemIntroText";s:1:"1";s:17:"feedTextWordLimit";s:0:"";s:16:"feedItemFullText";s:1:"1";s:12:"feedItemTags";s:1:"0";s:13:"feedItemVideo";s:1:"0";s:15:"feedItemGallery";s:1:"0";s:19:"feedItemAttachments";s:1:"0";s:14:"feedBogusEmail";s:0:"";s:16:"introTextCleanup";s:1:"0";s:27:"introTextCleanupExcludeTags";s:0:"";s:23:"introTextCleanupTagAttr";s:0:"";s:15:"fullTextCleanup";s:1:"0";s:26:"fullTextCleanupExcludeTags";s:0:"";s:22:"fullTextCleanupTagAttr";s:0:"";s:12:"xssFiltering";s:1:"0";s:14:"linkPopupWidth";s:3:"900";s:15:"linkPopupHeight";s:3:"600";s:13:"imagesQuality";s:3:"100";s:11:"itemImageXS";s:3:"100";s:10:"itemImageS";s:3:"200";s:10:"itemImageM";s:3:"400";s:10:"itemImageL";s:3:"600";s:11:"itemImageXL";s:3:"900";s:16:"itemImageGeneric";s:3:"300";s:13:"catImageWidth";s:3:"100";s:15:"catImageDefault";s:1:"1";s:14:"userImageWidth";s:3:"100";s:16:"userImageDefault";s:1:"1";s:17:"commenterImgWidth";s:2:"48";s:17:"onlineImageEditor";s:8:"splashup";s:14:"imageTimestamp";s:1:"0";s:16:"imageMemoryLimit";s:0:"";s:16:"socialButtonCode";s:0:"";s:15:"twitterUsername";s:0:"";s:13:"facebookImage";s:1:"1";s:8:"comments";s:1:"1";s:16:"commentsOrdering";s:4:"DESC";s:13:"commentsLimit";s:2:"10";s:20:"commentsFormPosition";s:5:"below";s:18:"commentsPublishing";s:1:"1";s:17:"commentsReporting";s:1:"2";s:23:"commentsReportRecipient";s:0:"";s:24:"inlineCommentsModeration";s:1:"0";s:8:"gravatar";s:1:"1";s:9:"recaptcha";s:1:"0";s:17:"commentsFormNotes";s:1:"1";s:21:"commentsFormNotesText";s:0:"";s:15:"frontendEditing";s:1:"1";s:12:"showImageTab";s:1:"1";s:19:"showImageGalleryTab";s:1:"1";s:12:"showVideoTab";s:1:"1";s:18:"showExtraFieldsTab";s:1:"1";s:18:"showAttachmentsTab";s:1:"1";s:13:"showK2Plugins";s:1:"1";s:22:"sideBarDisplayFrontend";s:1:"0";s:12:"mergeEditors";s:1:"1";s:14:"sideBarDisplay";s:1:"1";s:17:"attachmentsFolder";s:0:"";s:16:"hideImportButton";s:1:"0";s:12:"googleSearch";s:1:"0";s:21:"googleSearchContainer";s:23:"k2GoogleSearchContainer";s:13:"K2UserProfile";s:1:"1";s:11:"K2UserGroup";s:0:"";s:8:"redirect";s:0:"";s:11:"adminSearch";s:6:"simple";s:12:"cookieDomain";s:0:"";s:13:"taggingSystem";s:1:"1";s:8:"lockTags";s:1:"0";s:13:"showTagFilter";s:1:"0";s:9:"k2TagNorm";s:1:"0";s:13:"k2TagNormCase";s:5:"lower";s:31:"k2TagNormAdditionalReplacements";s:0:"";s:20:"recaptcha_public_key";s:0:"";s:21:"recaptcha_private_key";s:0:"";s:15:"recaptcha_theme";s:5:"clean";s:23:"recaptchaOnRegistration";s:1:"0";s:13:"stopForumSpam";s:1:"0";s:19:"stopForumSpamApiKey";s:0:"";s:21:"showItemsCounterAdmin";s:1:"1";s:17:"showChildCatItems";s:1:"1";s:22:"disableCompactOrdering";s:1:"0";s:13:"metaDescLimit";s:3:"150";s:22:"enforceSEFReplacements";s:1:"0";s:15:"SEFReplacements";s:0:"";s:5:"k2Sef";s:1:"0";s:13:"k2SefLabelCat";s:7:"content";s:13:"k2SefLabelTag";s:3:"tag";s:14:"k2SefLabelUser";s:6:"author";s:16:"k2SefLabelSearch";s:6:"search";s:14:"k2SefLabelDate";s:4:"date";s:14:"k2SefLabelItem";s:1:"0";s:26:"k2SefLabelItemCustomPrefix";s:0:"";s:17:"k2SefInsertItemId";s:1:"1";s:24:"k2SefItemIdTitleAliasSep";s:4:"dash";s:22:"k2SefUseItemTitleAlias";s:1:"1";s:16:"k2SefInsertCatId";s:1:"1";s:23:"k2SefCatIdTitleAliasSep";s:4:"dash";s:21:"k2SefUseCatTitleAlias";s:1:"1";s:16:"sh404SefLabelCat";s:0:"";s:17:"sh404SefLabelUser";s:4:"blog";s:17:"sh404SefLabelItem";s:1:"2";s:18:"sh404SefTitleAlias";s:5:"alias";s:29:"sh404SefModK2ContentFeedAlias";s:4:"feed";s:20:"sh404SefInsertItemId";s:1:"0";s:26:"sh404SefInsertUniqueItemId";s:1:"0";s:13:"cbIntegration";s:1:"0";s:10:"categories";a:3:{i:0;s:1:"9";i:1;s:2:"10";i:2;s:2:"11";}s:14:"catCatalogMode";s:1:"0";s:5:"theme";s:7:"default";s:17:"num_leading_items";s:1:"2";s:19:"num_leading_columns";s:1:"1";s:14:"leadingImgSize";s:5:"Large";s:17:"num_primary_items";s:1:"4";s:19:"num_primary_columns";s:1:"2";s:14:"primaryImgSize";s:6:"Medium";s:19:"num_secondary_items";s:1:"4";s:21:"num_secondary_columns";s:1:"1";s:16:"secondaryImgSize";s:5:"Small";s:9:"num_links";s:1:"4";s:17:"num_links_columns";s:1:"1";s:12:"linksImgSize";s:6:"XSmall";s:16:"catFeaturedItems";s:1:"1";s:13:"catPagination";s:1:"2";s:20:"catPaginationResults";s:1:"1";s:11:"catFeedLink";s:1:"0";s:11:"catFeedIcon";s:1:"0";s:9:"menu_text";i:1;s:17:"show_page_heading";i:0;s:6:"secure";i:0;s:22:"fusion_menu_entry_type";s:6:"normal";s:18:"fusion_menu_module";s:3:"103";s:14:"fusion_columns";s:1:"1";s:19:"fusion_distribution";s:4:"even";s:21:"fusion_children_group";s:1:"0";s:20:"fusion_children_type";s:9:"menuitems";s:14:"fusion_modules";s:3:"103";s:25:"splitmenu_menu_entry_type";s:6:"normal";s:21:"splitmenu_menu_module";s:3:"103";s:11:"inheritFrom";s:1:"0";s:8:"catTitle";s:1:"0";s:19:"catTitleItemCounter";s:1:"0";s:14:"catDescription";s:1:"0";s:8:"catImage";s:1:"0";s:13:"subCategories";s:1:"0";s:13:"subCatColumns";s:1:"2";s:11:"subCatTitle";s:1:"0";s:22:"subCatTitleItemCounter";s:1:"0";s:17:"subCatDescription";s:1:"0";s:11:"subCatImage";s:1:"0";s:12:"catItemTitle";s:1:"0";s:18:"catItemTitleLinked";s:1:"1";s:21:"catItemFeaturedNotice";s:1:"0";s:13:"catItemAuthor";s:1:"0";s:18:"catItemDateCreated";s:1:"0";s:13:"catItemRating";s:1:"0";s:12:"catItemImage";s:1:"1";s:16:"catItemIntroText";s:1:"1";s:18:"catItemExtraFields";s:1:"0";s:11:"catItemHits";s:1:"0";s:15:"catItemCategory";s:1:"0";s:11:"catItemTags";s:1:"0";s:18:"catItemAttachments";s:1:"0";s:25:"catItemAttachmentsCounter";s:1:"0";s:12:"catItemVideo";s:1:"0";s:20:"catItemVideoAutoPlay";s:1:"0";s:19:"catItemImageGallery";s:1:"0";s:19:"catItemDateModified";s:1:"0";s:15:"catItemReadMore";s:1:"1";s:21:"catItemCommentsAnchor";s:1:"1";s:16:"catItemK2Plugins";s:1:"1";s:15:"itemDateCreated";s:1:"0";s:9:"itemTitle";s:1:"0";s:18:"itemFeaturedNotice";s:1:"1";s:10:"itemAuthor";s:1:"0";s:15:"itemFontResizer";s:1:"1";s:15:"itemPrintButton";s:1:"1";s:15:"itemEmailButton";s:1:"1";s:16:"itemSocialButton";s:1:"1";s:15:"itemVideoAnchor";s:1:"1";s:22:"itemImageGalleryAnchor";s:1:"1";s:18:"itemCommentsAnchor";s:1:"1";s:10:"itemRating";s:1:"0";s:9:"itemImage";s:1:"1";s:11:"itemImgSize";s:5:"Large";s:20:"itemImageMainCaption";s:1:"1";s:20:"itemImageMainCredits";s:1:"1";s:13:"itemIntroText";s:1:"1";s:12:"itemFullText";s:1:"1";s:15:"itemExtraFields";s:1:"1";s:16:"itemDateModified";s:1:"0";s:8:"itemHits";s:1:"0";s:12:"itemCategory";s:1:"0";s:8:"itemTags";s:1:"0";s:15:"itemAttachments";s:1:"1";s:22:"itemAttachmentsCounter";s:1:"1";s:9:"itemVideo";s:1:"1";s:17:"itemVideoAutoPlay";s:1:"0";s:16:"itemVideoCaption";s:1:"1";s:16:"itemVideoCredits";s:1:"1";s:16:"itemImageGallery";s:1:"1";s:14:"itemNavigation";s:1:"1";s:12:"itemComments";s:1:"1";s:17:"itemTwitterButton";s:1:"1";s:18:"itemFacebookButton";s:1:"1";s:23:"itemGooglePlusOneButton";s:1:"1";s:15:"itemAuthorBlock";s:1:"0";s:15:"itemAuthorImage";s:1:"0";s:21:"itemAuthorDescription";s:1:"0";s:13:"itemAuthorURL";s:1:"0";s:15:"itemAuthorEmail";s:1:"0";s:16:"itemAuthorLatest";s:1:"0";s:21:"itemAuthorLatestLimit";s:1:"5";s:11:"itemRelated";s:1:"1";s:16:"itemRelatedLimit";s:1:"5";s:16:"itemRelatedTitle";s:1:"1";s:19:"itemRelatedCategory";s:1:"0";s:20:"itemRelatedImageSize";s:1:"0";s:20:"itemRelatedIntrotext";s:1:"0";s:19:"itemRelatedFulltext";s:1:"0";s:17:"itemRelatedAuthor";s:1:"0";s:16:"itemRelatedMedia";s:1:"0";s:23:"itemRelatedImageGallery";s:1:"0";s:13:"itemK2Plugins";s:1:"1";}}s:8:"metadesc";s:0:"";s:8:"metadata";s:15:"robots=
author=";s:7:"metakey";s:0:"";s:7:"plugins";s:0:"";s:8:"language";s:1:"*";s:11:"lastChanged";s:19:"2013-07-02 22:25:14";s:12:"categoryname";s:20:"Actualités féminin";s:10:"categoryid";s:2:"10";s:13:"categoryalias";s:18:"actualites-feminin";s:14:"categoryparams";s:2744:"{"inheritFrom":"0","theme":"","num_leading_items":"2","num_leading_columns":"1","leadingImgSize":"Large","num_primary_items":"4","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"4","num_secondary_columns":"1","secondaryImgSize":"Small","num_links":"4","num_links_columns":"1","linksImgSize":"XSmall","catCatalogMode":"0","catFeaturedItems":"1","catOrdering":"","catPagination":"2","catPaginationResults":"1","catTitle":"0","catTitleItemCounter":"0","catDescription":"0","catImage":"0","catFeedLink":"0","catFeedIcon":"0","subCategories":"0","subCatColumns":"2","subCatOrdering":"","subCatTitle":"0","subCatTitleItemCounter":"0","subCatDescription":"0","subCatImage":"0","itemImageXS":"","itemImageS":"","itemImageM":"","itemImageL":"","itemImageXL":"","catItemTitle":"1","catItemTitleLinked":"1","catItemFeaturedNotice":"0","catItemAuthor":"0","catItemDateCreated":"1","catItemRating":"0","catItemImage":"1","catItemIntroText":"1","catItemIntroTextWordLimit":"","catItemExtraFields":"0","catItemHits":"0","catItemCategory":"0","catItemTags":"0","catItemAttachments":"0","catItemAttachmentsCounter":"0","catItemVideo":"0","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"0","catItemImageGallery":"0","catItemDateModified":"0","catItemReadMore":"1","catItemCommentsAnchor":"1","catItemK2Plugins":"1","itemDateCreated":"0","itemTitle":"1","itemFeaturedNotice":"1","itemAuthor":"0","itemFontResizer":"1","itemPrintButton":"1","itemEmailButton":"1","itemSocialButton":"1","itemVideoAnchor":"1","itemImageGalleryAnchor":"1","itemCommentsAnchor":"1","itemRating":"0","itemImage":"1","itemImgSize":"Large","itemImageMainCaption":"1","itemImageMainCredits":"1","itemIntroText":"1","itemFullText":"1","itemExtraFields":"1","itemDateModified":"0","itemHits":"0","itemCategory":"0","itemTags":"0","itemAttachments":"1","itemAttachmentsCounter":"1","itemVideo":"1","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"0","itemVideoCaption":"1","itemVideoCredits":"1","itemImageGallery":"1","itemNavigation":"1","itemComments":"1","itemTwitterButton":"1","itemFacebookButton":"1","itemGooglePlusOneButton":"1","itemAuthorBlock":"0","itemAuthorImage":"0","itemAuthorDescription":"0","itemAuthorURL":"0","itemAuthorEmail":"0","itemAuthorLatest":"0","itemAuthorLatestLimit":"5","itemRelated":"1","itemRelatedLimit":"5","itemRelatedTitle":"1","itemRelatedCategory":"0","itemRelatedImageSize":"0","itemRelatedIntrotext":"0","itemRelatedFulltext":"0","itemRelatedAuthor":"0","itemRelatedMedia":"0","itemRelatedImageGallery":"0","itemK2Plugins":"1","catMetaDesc":"","catMetaKey":"","catMetaRobots":"","catMetaAuthor":""}";s:9:"itemGroup";s:7:"leading";s:8:"category";O:15:"TableK2Category":22:{s:2:"id";s:2:"10";s:4:"name";s:20:"Actualités féminin";s:5:"alias";s:18:"actualites-feminin";s:11:"description";s:0:"";s:6:"parent";s:1:"6";s:16:"extraFieldsGroup";s:1:"0";s:9:"published";s:1:"1";s:5:"image";s:0:"";s:6:"access";s:1:"1";s:8:"ordering";s:1:"1";s:6:"params";s:2744:"{"inheritFrom":"0","theme":"","num_leading_items":"2","num_leading_columns":"1","leadingImgSize":"Large","num_primary_items":"4","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"4","num_secondary_columns":"1","secondaryImgSize":"Small","num_links":"4","num_links_columns":"1","linksImgSize":"XSmall","catCatalogMode":"0","catFeaturedItems":"1","catOrdering":"","catPagination":"2","catPaginationResults":"1","catTitle":"0","catTitleItemCounter":"0","catDescription":"0","catImage":"0","catFeedLink":"0","catFeedIcon":"0","subCategories":"0","subCatColumns":"2","subCatOrdering":"","subCatTitle":"0","subCatTitleItemCounter":"0","subCatDescription":"0","subCatImage":"0","itemImageXS":"","itemImageS":"","itemImageM":"","itemImageL":"","itemImageXL":"","catItemTitle":"1","catItemTitleLinked":"1","catItemFeaturedNotice":"0","catItemAuthor":"0","catItemDateCreated":"1","catItemRating":"0","catItemImage":"1","catItemIntroText":"1","catItemIntroTextWordLimit":"","catItemExtraFields":"0","catItemHits":"0","catItemCategory":"0","catItemTags":"0","catItemAttachments":"0","catItemAttachmentsCounter":"0","catItemVideo":"0","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"0","catItemImageGallery":"0","catItemDateModified":"0","catItemReadMore":"1","catItemCommentsAnchor":"1","catItemK2Plugins":"1","itemDateCreated":"0","itemTitle":"1","itemFeaturedNotice":"1","itemAuthor":"0","itemFontResizer":"1","itemPrintButton":"1","itemEmailButton":"1","itemSocialButton":"1","itemVideoAnchor":"1","itemImageGalleryAnchor":"1","itemCommentsAnchor":"1","itemRating":"0","itemImage":"1","itemImgSize":"Large","itemImageMainCaption":"1","itemImageMainCredits":"1","itemIntroText":"1","itemFullText":"1","itemExtraFields":"1","itemDateModified":"0","itemHits":"0","itemCategory":"0","itemTags":"0","itemAttachments":"1","itemAttachmentsCounter":"1","itemVideo":"1","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"0","itemVideoCaption":"1","itemVideoCredits":"1","itemImageGallery":"1","itemNavigation":"1","itemComments":"1","itemTwitterButton":"1","itemFacebookButton":"1","itemGooglePlusOneButton":"1","itemAuthorBlock":"0","itemAuthorImage":"0","itemAuthorDescription":"0","itemAuthorURL":"0","itemAuthorEmail":"0","itemAuthorLatest":"0","itemAuthorLatestLimit":"5","itemRelated":"1","itemRelatedLimit":"5","itemRelatedTitle":"1","itemRelatedCategory":"0","itemRelatedImageSize":"0","itemRelatedIntrotext":"0","itemRelatedFulltext":"0","itemRelatedAuthor":"0","itemRelatedMedia":"0","itemRelatedImageGallery":"0","itemK2Plugins":"1","catMetaDesc":"","catMetaKey":"","catMetaRobots":"","catMetaAuthor":""}";s:5:"trash";s:1:"0";s:7:"plugins";s:0:"";s:8:"language";s:1:"*";s:7:" * _tbl";s:16:"#__k2_categories";s:11:" * _tbl_key";s:2:"id";s:6:" * _db";O:15:"JDatabaseMySQLi":18:{s:4:"name";s:6:"mysqli";s:12:" * nameQuote";s:1:"`";s:11:" * nullDate";s:19:"0000-00-00 00:00:00";s:20:" JDatabase _database";s:12:"bcfsoftbmod1";s:13:" * connection";O:6:"mysqli":0:{}s:8:" * count";i:0;s:9:" * cursor";O:13:"mysqli_result":0:{}s:8:" * debug";b:0;s:8:" * limit";i:0;s:6:" * log";a:0:{}s:9:" * offset";i:0;s:6:" * sql";s:67:"SELECT COUNT(*) FROM #__k2_comments WHERE itemID=8 AND published=1 ";s:14:" * tablePrefix";s:4:"jom_";s:6:" * utf";b:1;s:11:" * errorNum";i:0;s:11:" * errorMsg";s:0:"";s:12:" * hasQuoted";b:0;s:9:" * quoted";a:0:{}}s:15:" * _trackAssets";b:0;s:9:" * _rules";N;s:10:" * _locked";b:0;s:10:" * _errors";a:0:{}s:4:"link";s:31:"/les-equipes/feminin/actualites";}s:4:"link";s:71:"/les-equipes/feminin/actualites/item/8-championnat-feminin-outdoor-2012";s:9:"printLink";s:98:"/les-equipes/feminin/actualites/item/8-championnat-feminin-outdoor-2012?tmpl=component&amp;print=1";s:11:"imageXSmall";s:0:"";s:10:"imageSmall";s:0:"";s:11:"imageMedium";s:0:"";s:10:"imageLarge";s:0:"";s:11:"imageXLarge";s:0:"";s:10:"cleanTitle";s:33:"Championnat Féminin Outdoor 2012";s:13:"numOfComments";s:1:"0";}}